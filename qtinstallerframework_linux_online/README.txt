Shows how to set up an online installer.

Ändra i .desktop

[Desktop Entry]
Version=1.0
Type=Application
Name=streamCapture2
Comment=Download video streams
Exec=/opt/StreamCapture2/AppRun %F
Icon=/opt/StreamCapture2/streamcapture2.png
Categories=AudioVideo;


Generate online repository with

Lubuntu 32-bit
/opt/qtinstallerframework/bin

Ändra i config.xml
32-bit
 <Url>http://bin.ceicer.com/streamcapture2/repository/linux_x86</Url>
 64-bit
  <Url>http://bin.ceicer.com/streamcapture2/repository/linux_x86_64</Url>

  repogen -p packages repository
  repogen -p packages linux_x86_64
  /opt/qtinstallerframework/bin/repogen -p packages linux_x86
  

Generate installer with

  binarycreator --online-only -c config/config.xml -p packages install-streamCapture2_X86_64-0.18.6
  Linux 32-bit
  /opt/qtinstallerframework/bin/binarycreator --online-only --resources fonts.qrc -c config/config.xml -p packages install-streamCapture2_X86-0.18.6

This should make the content of the local directory available under
http://localhost

You should be able to now launch the installer.

To deploy an update
Ändra 
<Version>2.5.2</Version> i package.xml
run:

  repogen --update-new-components -p packages repository

and launch the maintenance tool in your installation.
