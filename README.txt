2020-05-02T02:29:42
Algoritm: md5

Windows installer 64 bit
d12b02c2720b8b9bca78301f31bab696 install-streamcapture2_X86_64_MinGW8.1.0-0.18.6.exe (Compiled by MinGW GCC 8.1.0)
efa61d3a2e9d776864cd8168f0771243 install-streamcapture2_X86_64_VC++16.5-0.18.6.exe (Compiled by MSVC++ 16.5)

Windows installer 32 bit
211dda11ea81d5f6b2774cfbf8d20021 install-streamcapture2_X86_MinGW8.1.0-0.18.6.exe (Compiled by MinGW GCC 8.1.0)

Windows portable 64 bit
3647f9a3f081776860d8e255ef29d6a9 win-portable-streamcapture2_X86_64_MinGW8.1.0-0.18.6.zip (Compiled by MinGW GCC 8.1.0)
256a9bacc22d392a1d825025b5457f8f win-portable-streamcapture2_X86_64_VC++16.5-0.18.6-0.18.6.zip (Compiled by MSVC++ 16.5)

Windows portable 32 bit
2660f4c88a10e8d0a51be9cbab972b44 win-portable-streamcapture2_X86_MinGW8.1.0-0.18.6.zip (Compiled by MinGW GCC 8.1.0)



Linux 64 bit, GLIBC >= 2.23, For example, Ubuntu 16.04 or later
7c5d2a1f37ed40c2829074d3f559a5da streamcapture2-1beab68-x86_64_GCC5.4.0.AppImage (Compiled by GCC 5.4.0)

Linux 32 bit, GLIBC >= 2.23, For example, Ubuntu 16.04 or later
4d1e7c6470753aec9d0a5519dcc791e1 streamcapture2-1beab68-i386_GCC5.4.0.AppImage (Compiled by GCC 5.4.0)

Linux 64 bit, GLIBC >= 2.31, For example, Ubuntu 20.04 or later
0534caba4471e1bdcb574eb1ecf818a0 streamcapture2-710199d-x86_64_CLANG10.0.0.AppImage (Compiled by CLANG 10.0.0)
fe0d482d04a316f5ddaa06e6c2a54bae streamcapture2-710199d-x86_64_GCC9.3.0.AppImage (Compiled by GCC 9.3.0)
68c6b0f40249f59151a7376b58debb1f streamcapture2-710199d-x86_64_ICC19.10.AppImage (Compiled by Intel ICC 19.10)
