 # streamCapture2

http://ceicer.org/streamcapture2/index_eng.php<br>
**Read more and download:** [**Wiki**](https://gitlab.com/posktomten/streamcapture2/wikis/home)<br>
&copy; Coppyright Ingemar Ceicer GPL v3<br>

@posktomten

A program to save streaming video to your computer. A graphical shell for the command line program svtplay-dl
The program works with Linux, Windows and MacOS X. The program is written in C++ and uses Qt5 graphic library. There is a setup program for Windows and AppImage for Linux.
Tested on Windows 10 (64 bit), Ubuntu 18.04 (64-bit) and Lubuntu 16.04 (32-bit and 64-bit).

The program can handle three languages English, German and Swedish.

# streamCapture2 is a graphical shell for the command line program svtplay-dl.
https://svtplay-dl.se/

FFmpeg is used in turn by svtplay-dl

https://www.ffmpeg.org/


streamCapture2 is designed to work primarily on

```
aftonbladet.se bambuser.com dbtv.no di.se dn.se dplay.se dr.dk  efn.se  
expressen.se  filmarkivet.se  flowonline.tv  hbo.com  dplay.se 
nickelodeon.nl  nickelodeon.no  nickelodeon.se  nrk.no  oppetarkiv.se 
pokemon.com  ruv.is  riksdagen.se  svd.se  sverigesradio.se  svtplay.se 
viafree.dk  viafree.no  tv3play.ee  tv3play.lt  tv3play.lv  tv4.se 
tv4play.se  twitch.tv  ur.se  urplay.se  vg.no  viagame.com 
viasat4play.no  viasatsport.se

```
