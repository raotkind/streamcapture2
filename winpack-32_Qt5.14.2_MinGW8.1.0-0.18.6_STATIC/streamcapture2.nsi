  
  !include LogicLib.nsh
  # EDIT THIS
  # dynamic
  
  
  !define COMPANY_NAME "Ceicer IT"
  !define PRODUCT_PUBLISHER "Ceicer IT"
  !define PROGRAMMERS_WEBSITE "Website"
  !define APPICON "icon.ico"
  
  !define INSTALL_DIR_NAME "Streamcapture2"

  
  !define DISPLAY_NAME "streamCapture2"
  
  !define PROG_NAME "streamcapture2"
  !define VER_NAME "0.18.6"
  
  
  !define BRANDING_TEXT "Ceicer IT"
  !define BGGRADIENT "009463" #off
  
  !define LICENSE_DATA "license_windows.txt"
  
  Icon "icons\install.ico"
  UninstallIcon "icons\uninstall.ico"
  


  !include LogicLib.nsh
  !define LOCALE_SNATIVELANGNAME '0x4' ;System native language name

  Var /GLOBAL stopp




LicenseForceSelection off ;radiobuttons | checkbox

ShowInstDetails hide
ShowUnInstDetails hide
SetCompress off

LoadLanguageFile "${NSISDIR}\Contrib\Language files\English.nlf"
LoadLanguageFile "${NSISDIR}\Contrib\Language files\Swedish.nlf"





Page license
 

   


; License headline
;LicenseText "You must agree to this license before installing."
; Text file containing the license
LicenseData "${LICENSE_DATA}"


; The name of the installer
Name "${DISPLAY_NAME} ${VER_NAME}"

; The file to write
#  OutFile "install-${VER_NAME}.exe"
  OutFile "install-${PROG_NAME}_X86-${VER_NAME}.exe"




BrandingText "${BRANDING_TEXT}"
BGGradient "${BGGRADIENT}"

; The default installation directory
    # 64-bit
    # InstallDir $PROGRAMFILES64\${INSTALL_DIR_NAME}
    # 32-bit
      InstallDir $PROGRAMFILES32\${INSTALL_DIR_NAME}



; Registry key to check for directory (so if you install again, it will 
; overwrite the old one automatically)
InstallDirRegKey HKLM "Software\${DISPLAY_NAME}" "Install_Dir"

;--------------------------------

; Pages

Page components
Page directory
Page instfiles


UninstPage uninstConfirm
UninstPage instfiles

;--------------------------------



; The stuff to install
Section "Required"




  SectionIn RO
System::Call 'kernel32::GetLocaleInfoA(i 1024, i ${LOCALE_SNATIVELANGNAME}, t .r1, i ${NSIS_MAX_STRLEN}) i r0'
 ${If} $stopp == "1"
	${If} $1 == "svenska"
	messageBox MB_OK "Du mste avinstallera den nuvarande versionen$\ninnan du kan installera en ny!$\nInstallationen avbryts."
	Quit
	${Else}
	messageBox MB_OK "You must uninstall the current version $\nbefore you can install a new one!$\nThe installation is interrupted."
	Quit
	${EndIf}
  ${EndIf}
  

  
  ; Set output path to the installation directory.
  SetOutPath "$INSTDIR"

  SetShellVarContext all # i startmenyn fr "all users", default "current"
  
  
  ; Put file there


SetOutPath "$INSTDIR"
    File "${PROG_NAME}-${VER_NAME}.exe"
	File "ffmpeg.exe"
	File "icon.ico"
	File "license_windows.txt"
	File "python37.dll"
	File "svtplay-dl.exe"
	File "VCRUNTIME140.dll"
	File "Website.URL"
SetOutPath "$INSTDIR\icons"
	File "icons\uninstall.ico"
SetOutPath "$INSTDIR\lib"
	File "lib\libcrypto-1_1.dll"
	File "lib\library.zip"
	File "lib\libssl-1_1.dll"
	File "lib\pyexpat.pyd"
	File "lib\python37.dll"
	File "lib\select.pyd"
	File "lib\unicodedata.pyd"
	File "lib\VCRUNTIME140.dll"
	File "lib\_bz2.pyd"
	File "lib\_cffi_backend.cp37-win32.pyd"
	File "lib\_ctypes.pyd"
	File "lib\_decimal.pyd"
	File "lib\_elementtree.pyd"
	File "lib\_hashlib.pyd"
	File "lib\_lzma.pyd"
	File "lib\_multiprocessing.pyd"
	File "lib\_queue.pyd"
	File "lib\_socket.pyd"
	File "lib\_ssl.pyd"
	File "lib\_yaml.cp37-win32.pyd"
SetOutPath "$INSTDIR\lib\backports"
	File "lib\backports\_datetime_fromisoformat.cp37-win32.pyd"
	File "lib\backports\__init__.pyc"
SetOutPath "$INSTDIR\lib\certifi"
	File "lib\certifi\cacert.pem"
	File "lib\certifi\core.pyc"
	File "lib\certifi\__init__.pyc"
SetOutPath "$INSTDIR\lib\cffi"
	File "lib\cffi\api.pyc"
	File "lib\cffi\backend_ctypes.pyc"
	File "lib\cffi\cffi_opcode.pyc"
	File "lib\cffi\commontypes.pyc"
	File "lib\cffi\cparser.pyc"
	File "lib\cffi\error.pyc"
	File "lib\cffi\ffiplatform.pyc"
	File "lib\cffi\lock.pyc"
	File "lib\cffi\model.pyc"
	File "lib\cffi\parse_c_type.h"
	File "lib\cffi\pkgconfig.pyc"
	File "lib\cffi\recompiler.pyc"
	File "lib\cffi\setuptools_ext.pyc"
	File "lib\cffi\vengine_cpy.pyc"
	File "lib\cffi\vengine_gen.pyc"
	File "lib\cffi\verifier.pyc"
	File "lib\cffi\_cffi_errors.h"
	File "lib\cffi\_cffi_include.h"
	File "lib\cffi\_embedding.h"
	File "lib\cffi\__init__.pyc"
SetOutPath "$INSTDIR\lib\chardet"
	File "lib\chardet\big5freq.pyc"
	File "lib\chardet\big5prober.pyc"
	File "lib\chardet\chardistribution.pyc"
	File "lib\chardet\charsetgroupprober.pyc"
	File "lib\chardet\charsetprober.pyc"
	File "lib\chardet\codingstatemachine.pyc"
	File "lib\chardet\compat.pyc"
	File "lib\chardet\cp949prober.pyc"
	File "lib\chardet\enums.pyc"
	File "lib\chardet\escprober.pyc"
	File "lib\chardet\escsm.pyc"
	File "lib\chardet\eucjpprober.pyc"
	File "lib\chardet\euckrfreq.pyc"
	File "lib\chardet\euckrprober.pyc"
	File "lib\chardet\euctwfreq.pyc"
	File "lib\chardet\euctwprober.pyc"
	File "lib\chardet\gb2312freq.pyc"
	File "lib\chardet\gb2312prober.pyc"
	File "lib\chardet\hebrewprober.pyc"
	File "lib\chardet\jisfreq.pyc"
	File "lib\chardet\jpcntx.pyc"
	File "lib\chardet\langbulgarianmodel.pyc"
	File "lib\chardet\langcyrillicmodel.pyc"
	File "lib\chardet\langgreekmodel.pyc"
	File "lib\chardet\langhebrewmodel.pyc"
	File "lib\chardet\langthaimodel.pyc"
	File "lib\chardet\langturkishmodel.pyc"
	File "lib\chardet\latin1prober.pyc"
	File "lib\chardet\mbcharsetprober.pyc"
	File "lib\chardet\mbcsgroupprober.pyc"
	File "lib\chardet\mbcssm.pyc"
	File "lib\chardet\sbcharsetprober.pyc"
	File "lib\chardet\sbcsgroupprober.pyc"
	File "lib\chardet\sjisprober.pyc"
	File "lib\chardet\universaldetector.pyc"
	File "lib\chardet\utf8prober.pyc"
	File "lib\chardet\version.pyc"
	File "lib\chardet\__init__.pyc"
SetOutPath "$INSTDIR\lib\collections"
	File "lib\collections\abc.pyc"
	File "lib\collections\__init__.pyc"
SetOutPath "$INSTDIR\lib\concurrent"
	File "lib\concurrent\__init__.pyc"
SetOutPath "$INSTDIR\lib\concurrent\futures"
	File "lib\concurrent\futures\process.pyc"
	File "lib\concurrent\futures\thread.pyc"
	File "lib\concurrent\futures\_base.pyc"
	File "lib\concurrent\futures\__init__.pyc"
SetOutPath "$INSTDIR\lib\cryptography"
	File "lib\cryptography\exceptions.pyc"
	File "lib\cryptography\fernet.pyc"
	File "lib\cryptography\utils.pyc"
	File "lib\cryptography\__about__.pyc"
	File "lib\cryptography\__init__.pyc"
SetOutPath "$INSTDIR\lib\cryptography\hazmat"
	File "lib\cryptography\hazmat\_der.pyc"
	File "lib\cryptography\hazmat\_oid.pyc"
	File "lib\cryptography\hazmat\__init__.pyc"
SetOutPath "$INSTDIR\lib\cryptography\hazmat\backends"
	File "lib\cryptography\hazmat\backends\interfaces.pyc"
	File "lib\cryptography\hazmat\backends\__init__.pyc"
SetOutPath "$INSTDIR\lib\cryptography\hazmat\backends\openssl"
	File "lib\cryptography\hazmat\backends\openssl\aead.pyc"
	File "lib\cryptography\hazmat\backends\openssl\backend.pyc"
	File "lib\cryptography\hazmat\backends\openssl\ciphers.pyc"
	File "lib\cryptography\hazmat\backends\openssl\cmac.pyc"
	File "lib\cryptography\hazmat\backends\openssl\decode_asn1.pyc"
	File "lib\cryptography\hazmat\backends\openssl\dh.pyc"
	File "lib\cryptography\hazmat\backends\openssl\dsa.pyc"
	File "lib\cryptography\hazmat\backends\openssl\ec.pyc"
	File "lib\cryptography\hazmat\backends\openssl\ed25519.pyc"
	File "lib\cryptography\hazmat\backends\openssl\ed448.pyc"
	File "lib\cryptography\hazmat\backends\openssl\encode_asn1.pyc"
	File "lib\cryptography\hazmat\backends\openssl\hashes.pyc"
	File "lib\cryptography\hazmat\backends\openssl\hmac.pyc"
	File "lib\cryptography\hazmat\backends\openssl\ocsp.pyc"
	File "lib\cryptography\hazmat\backends\openssl\poly1305.pyc"
	File "lib\cryptography\hazmat\backends\openssl\rsa.pyc"
	File "lib\cryptography\hazmat\backends\openssl\utils.pyc"
	File "lib\cryptography\hazmat\backends\openssl\x25519.pyc"
	File "lib\cryptography\hazmat\backends\openssl\x448.pyc"
	File "lib\cryptography\hazmat\backends\openssl\x509.pyc"
	File "lib\cryptography\hazmat\backends\openssl\__init__.pyc"
SetOutPath "$INSTDIR\lib\cryptography\hazmat\bindings"
	File "lib\cryptography\hazmat\bindings\python37.dll"
	File "lib\cryptography\hazmat\bindings\_constant_time.cp37-win32.pyd"
	File "lib\cryptography\hazmat\bindings\_openssl.cp37-win32.pyd"
	File "lib\cryptography\hazmat\bindings\_padding.cp37-win32.pyd"
	File "lib\cryptography\hazmat\bindings\__init__.pyc"
SetOutPath "$INSTDIR\lib\cryptography\hazmat\bindings\openssl"
	File "lib\cryptography\hazmat\bindings\openssl\binding.pyc"
	File "lib\cryptography\hazmat\bindings\openssl\_conditional.pyc"
	File "lib\cryptography\hazmat\bindings\openssl\__init__.pyc"
SetOutPath "$INSTDIR\lib\cryptography\hazmat\primitives"
	File "lib\cryptography\hazmat\primitives\cmac.pyc"
	File "lib\cryptography\hazmat\primitives\constant_time.pyc"
	File "lib\cryptography\hazmat\primitives\hashes.pyc"
	File "lib\cryptography\hazmat\primitives\hmac.pyc"
	File "lib\cryptography\hazmat\primitives\keywrap.pyc"
	File "lib\cryptography\hazmat\primitives\padding.pyc"
	File "lib\cryptography\hazmat\primitives\poly1305.pyc"
	File "lib\cryptography\hazmat\primitives\__init__.pyc"
SetOutPath "$INSTDIR\lib\cryptography\hazmat\primitives\asymmetric"
	File "lib\cryptography\hazmat\primitives\asymmetric\dh.pyc"
	File "lib\cryptography\hazmat\primitives\asymmetric\dsa.pyc"
	File "lib\cryptography\hazmat\primitives\asymmetric\ec.pyc"
	File "lib\cryptography\hazmat\primitives\asymmetric\ed25519.pyc"
	File "lib\cryptography\hazmat\primitives\asymmetric\ed448.pyc"
	File "lib\cryptography\hazmat\primitives\asymmetric\padding.pyc"
	File "lib\cryptography\hazmat\primitives\asymmetric\rsa.pyc"
	File "lib\cryptography\hazmat\primitives\asymmetric\utils.pyc"
	File "lib\cryptography\hazmat\primitives\asymmetric\x25519.pyc"
	File "lib\cryptography\hazmat\primitives\asymmetric\x448.pyc"
	File "lib\cryptography\hazmat\primitives\asymmetric\__init__.pyc"
SetOutPath "$INSTDIR\lib\cryptography\hazmat\primitives\ciphers"
	File "lib\cryptography\hazmat\primitives\ciphers\aead.pyc"
	File "lib\cryptography\hazmat\primitives\ciphers\algorithms.pyc"
	File "lib\cryptography\hazmat\primitives\ciphers\base.pyc"
	File "lib\cryptography\hazmat\primitives\ciphers\modes.pyc"
	File "lib\cryptography\hazmat\primitives\ciphers\__init__.pyc"
SetOutPath "$INSTDIR\lib\cryptography\hazmat\primitives\kdf"
	File "lib\cryptography\hazmat\primitives\kdf\concatkdf.pyc"
	File "lib\cryptography\hazmat\primitives\kdf\hkdf.pyc"
	File "lib\cryptography\hazmat\primitives\kdf\kbkdf.pyc"
	File "lib\cryptography\hazmat\primitives\kdf\pbkdf2.pyc"
	File "lib\cryptography\hazmat\primitives\kdf\scrypt.pyc"
	File "lib\cryptography\hazmat\primitives\kdf\x963kdf.pyc"
	File "lib\cryptography\hazmat\primitives\kdf\__init__.pyc"
SetOutPath "$INSTDIR\lib\cryptography\hazmat\primitives\serialization"
	File "lib\cryptography\hazmat\primitives\serialization\base.pyc"
	File "lib\cryptography\hazmat\primitives\serialization\pkcs12.pyc"
	File "lib\cryptography\hazmat\primitives\serialization\ssh.pyc"
	File "lib\cryptography\hazmat\primitives\serialization\__init__.pyc"
SetOutPath "$INSTDIR\lib\cryptography\hazmat\primitives\twofactor"
	File "lib\cryptography\hazmat\primitives\twofactor\hotp.pyc"
	File "lib\cryptography\hazmat\primitives\twofactor\totp.pyc"
	File "lib\cryptography\hazmat\primitives\twofactor\utils.pyc"
	File "lib\cryptography\hazmat\primitives\twofactor\__init__.pyc"
SetOutPath "$INSTDIR\lib\cryptography\x509"
	File "lib\cryptography\x509\base.pyc"
	File "lib\cryptography\x509\certificate_transparency.pyc"
	File "lib\cryptography\x509\extensions.pyc"
	File "lib\cryptography\x509\general_name.pyc"
	File "lib\cryptography\x509\name.pyc"
	File "lib\cryptography\x509\ocsp.pyc"
	File "lib\cryptography\x509\oid.pyc"
	File "lib\cryptography\x509\__init__.pyc"
SetOutPath "$INSTDIR\lib\ctypes"
	File "lib\ctypes\util.pyc"
	File "lib\ctypes\_aix.pyc"
	File "lib\ctypes\_endian.pyc"
	File "lib\ctypes\__init__.pyc"
SetOutPath "$INSTDIR\lib\ctypes\macholib"
	File "lib\ctypes\macholib\dyld.pyc"
	File "lib\ctypes\macholib\dylib.pyc"
	File "lib\ctypes\macholib\fetch_macholib"
	File "lib\ctypes\macholib\fetch_macholib.bat"
	File "lib\ctypes\macholib\framework.pyc"
	File "lib\ctypes\macholib\README.ctypes"
	File "lib\ctypes\macholib\__init__.pyc"
SetOutPath "$INSTDIR\lib\distutils"
	File "lib\distutils\archive_util.pyc"
	File "lib\distutils\ccompiler.pyc"
	File "lib\distutils\cmd.pyc"
	File "lib\distutils\config.pyc"
	File "lib\distutils\core.pyc"
	File "lib\distutils\debug.pyc"
	File "lib\distutils\dep_util.pyc"
	File "lib\distutils\dir_util.pyc"
	File "lib\distutils\dist.pyc"
	File "lib\distutils\errors.pyc"
	File "lib\distutils\extension.pyc"
	File "lib\distutils\fancy_getopt.pyc"
	File "lib\distutils\filelist.pyc"
	File "lib\distutils\file_util.pyc"
	File "lib\distutils\log.pyc"
	File "lib\distutils\msvc9compiler.pyc"
	File "lib\distutils\README"
	File "lib\distutils\spawn.pyc"
	File "lib\distutils\sysconfig.pyc"
	File "lib\distutils\text_file.pyc"
	File "lib\distutils\util.pyc"
	File "lib\distutils\version.pyc"
	File "lib\distutils\versionpredicate.pyc"
	File "lib\distutils\_msvccompiler.pyc"
	File "lib\distutils\__init__.pyc"
SetOutPath "$INSTDIR\lib\distutils\command"
	File "lib\distutils\command\bdist.pyc"
	File "lib\distutils\command\build_ext.pyc"
	File "lib\distutils\command\build_py.pyc"
	File "lib\distutils\command\build_scripts.pyc"
	File "lib\distutils\command\command_template"
	File "lib\distutils\command\install.pyc"
	File "lib\distutils\command\install_scripts.pyc"
	File "lib\distutils\command\sdist.pyc"
	File "lib\distutils\command\wininst-10.0-amd64.exe"
	File "lib\distutils\command\wininst-10.0.exe"
	File "lib\distutils\command\wininst-14.0-amd64.exe"
	File "lib\distutils\command\wininst-14.0.exe"
	File "lib\distutils\command\wininst-6.0.exe"
	File "lib\distutils\command\wininst-7.1.exe"
	File "lib\distutils\command\wininst-8.0.exe"
	File "lib\distutils\command\wininst-9.0-amd64.exe"
	File "lib\distutils\command\wininst-9.0.exe"
	File "lib\distutils\command\__init__.pyc"
SetOutPath "$INSTDIR\lib\distutils\tests"
	File "lib\distutils\tests\includetest.rst"
	File "lib\distutils\tests\Setup.sample"
SetOutPath "$INSTDIR\lib\email"
	File "lib\email\architecture.rst"
	File "lib\email\base64mime.pyc"
	File "lib\email\charset.pyc"
	File "lib\email\contentmanager.pyc"
	File "lib\email\encoders.pyc"
	File "lib\email\errors.pyc"
	File "lib\email\feedparser.pyc"
	File "lib\email\generator.pyc"
	File "lib\email\header.pyc"
	File "lib\email\headerregistry.pyc"
	File "lib\email\iterators.pyc"
	File "lib\email\message.pyc"
	File "lib\email\parser.pyc"
	File "lib\email\policy.pyc"
	File "lib\email\quoprimime.pyc"
	File "lib\email\utils.pyc"
	File "lib\email\_encoded_words.pyc"
	File "lib\email\_header_value_parser.pyc"
	File "lib\email\_parseaddr.pyc"
	File "lib\email\_policybase.pyc"
	File "lib\email\__init__.pyc"
SetOutPath "$INSTDIR\lib\encodings"
	File "lib\encodings\aliases.pyc"
	File "lib\encodings\ascii.pyc"
	File "lib\encodings\base64_codec.pyc"
	File "lib\encodings\big5.pyc"
	File "lib\encodings\big5hkscs.pyc"
	File "lib\encodings\bz2_codec.pyc"
	File "lib\encodings\charmap.pyc"
	File "lib\encodings\cp037.pyc"
	File "lib\encodings\cp1006.pyc"
	File "lib\encodings\cp1026.pyc"
	File "lib\encodings\cp1125.pyc"
	File "lib\encodings\cp1140.pyc"
	File "lib\encodings\cp1250.pyc"
	File "lib\encodings\cp1251.pyc"
	File "lib\encodings\cp1252.pyc"
	File "lib\encodings\cp1253.pyc"
	File "lib\encodings\cp1254.pyc"
	File "lib\encodings\cp1255.pyc"
	File "lib\encodings\cp1256.pyc"
	File "lib\encodings\cp1257.pyc"
	File "lib\encodings\cp1258.pyc"
	File "lib\encodings\cp273.pyc"
	File "lib\encodings\cp424.pyc"
	File "lib\encodings\cp437.pyc"
	File "lib\encodings\cp500.pyc"
	File "lib\encodings\cp65001.pyc"
	File "lib\encodings\cp720.pyc"
	File "lib\encodings\cp737.pyc"
	File "lib\encodings\cp775.pyc"
	File "lib\encodings\cp850.pyc"
	File "lib\encodings\cp852.pyc"
	File "lib\encodings\cp855.pyc"
	File "lib\encodings\cp856.pyc"
	File "lib\encodings\cp857.pyc"
	File "lib\encodings\cp858.pyc"
	File "lib\encodings\cp860.pyc"
	File "lib\encodings\cp861.pyc"
	File "lib\encodings\cp862.pyc"
	File "lib\encodings\cp863.pyc"
	File "lib\encodings\cp864.pyc"
	File "lib\encodings\cp865.pyc"
	File "lib\encodings\cp866.pyc"
	File "lib\encodings\cp869.pyc"
	File "lib\encodings\cp874.pyc"
	File "lib\encodings\cp875.pyc"
	File "lib\encodings\cp932.pyc"
	File "lib\encodings\cp949.pyc"
	File "lib\encodings\cp950.pyc"
	File "lib\encodings\euc_jisx0213.pyc"
	File "lib\encodings\euc_jis_2004.pyc"
	File "lib\encodings\euc_jp.pyc"
	File "lib\encodings\euc_kr.pyc"
	File "lib\encodings\gb18030.pyc"
	File "lib\encodings\gb2312.pyc"
	File "lib\encodings\gbk.pyc"
	File "lib\encodings\hex_codec.pyc"
	File "lib\encodings\hp_roman8.pyc"
	File "lib\encodings\hz.pyc"
	File "lib\encodings\idna.pyc"
	File "lib\encodings\iso2022_jp.pyc"
	File "lib\encodings\iso2022_jp_1.pyc"
	File "lib\encodings\iso2022_jp_2.pyc"
	File "lib\encodings\iso2022_jp_2004.pyc"
	File "lib\encodings\iso2022_jp_3.pyc"
	File "lib\encodings\iso2022_jp_ext.pyc"
	File "lib\encodings\iso2022_kr.pyc"
	File "lib\encodings\iso8859_1.pyc"
	File "lib\encodings\iso8859_10.pyc"
	File "lib\encodings\iso8859_11.pyc"
	File "lib\encodings\iso8859_13.pyc"
	File "lib\encodings\iso8859_14.pyc"
	File "lib\encodings\iso8859_15.pyc"
	File "lib\encodings\iso8859_16.pyc"
	File "lib\encodings\iso8859_2.pyc"
	File "lib\encodings\iso8859_3.pyc"
	File "lib\encodings\iso8859_4.pyc"
	File "lib\encodings\iso8859_5.pyc"
	File "lib\encodings\iso8859_6.pyc"
	File "lib\encodings\iso8859_7.pyc"
	File "lib\encodings\iso8859_8.pyc"
	File "lib\encodings\iso8859_9.pyc"
	File "lib\encodings\johab.pyc"
	File "lib\encodings\koi8_r.pyc"
	File "lib\encodings\koi8_t.pyc"
	File "lib\encodings\koi8_u.pyc"
	File "lib\encodings\kz1048.pyc"
	File "lib\encodings\latin_1.pyc"
	File "lib\encodings\mac_arabic.pyc"
	File "lib\encodings\mac_centeuro.pyc"
	File "lib\encodings\mac_croatian.pyc"
	File "lib\encodings\mac_cyrillic.pyc"
	File "lib\encodings\mac_farsi.pyc"
	File "lib\encodings\mac_greek.pyc"
	File "lib\encodings\mac_iceland.pyc"
	File "lib\encodings\mac_latin2.pyc"
	File "lib\encodings\mac_roman.pyc"
	File "lib\encodings\mac_romanian.pyc"
	File "lib\encodings\mac_turkish.pyc"
	File "lib\encodings\mbcs.pyc"
	File "lib\encodings\oem.pyc"
	File "lib\encodings\palmos.pyc"
	File "lib\encodings\ptcp154.pyc"
	File "lib\encodings\punycode.pyc"
	File "lib\encodings\quopri_codec.pyc"
	File "lib\encodings\raw_unicode_escape.pyc"
	File "lib\encodings\rot_13.pyc"
	File "lib\encodings\shift_jis.pyc"
	File "lib\encodings\shift_jisx0213.pyc"
	File "lib\encodings\shift_jis_2004.pyc"
	File "lib\encodings\tis_620.pyc"
	File "lib\encodings\undefined.pyc"
	File "lib\encodings\unicode_escape.pyc"
	File "lib\encodings\unicode_internal.pyc"
	File "lib\encodings\utf_16.pyc"
	File "lib\encodings\utf_16_be.pyc"
	File "lib\encodings\utf_16_le.pyc"
	File "lib\encodings\utf_32.pyc"
	File "lib\encodings\utf_32_be.pyc"
	File "lib\encodings\utf_32_le.pyc"
	File "lib\encodings\utf_7.pyc"
	File "lib\encodings\utf_8.pyc"
	File "lib\encodings\utf_8_sig.pyc"
	File "lib\encodings\uu_codec.pyc"
	File "lib\encodings\zlib_codec.pyc"
	File "lib\encodings\__init__.pyc"
SetOutPath "$INSTDIR\lib\html"
	File "lib\html\entities.pyc"
	File "lib\html\__init__.pyc"
SetOutPath "$INSTDIR\lib\http"
	File "lib\http\client.pyc"
	File "lib\http\cookiejar.pyc"
	File "lib\http\cookies.pyc"
	File "lib\http\server.pyc"
	File "lib\http\__init__.pyc"
SetOutPath "$INSTDIR\lib\idna"
	File "lib\idna\codec.pyc"
	File "lib\idna\compat.pyc"
	File "lib\idna\core.pyc"
	File "lib\idna\idnadata.pyc"
	File "lib\idna\intranges.pyc"
	File "lib\idna\package_data.pyc"
	File "lib\idna\uts46data.pyc"
	File "lib\idna\__init__.pyc"
SetOutPath "$INSTDIR\lib\importlib"
	File "lib\importlib\abc.pyc"
	File "lib\importlib\machinery.pyc"
	File "lib\importlib\util.pyc"
	File "lib\importlib\_bootstrap.pyc"
	File "lib\importlib\_bootstrap_external.pyc"
	File "lib\importlib\__init__.pyc"
SetOutPath "$INSTDIR\lib\json"
	File "lib\json\decoder.pyc"
	File "lib\json\encoder.pyc"
	File "lib\json\scanner.pyc"
	File "lib\json\__init__.pyc"
SetOutPath "$INSTDIR\lib\lib2to3"
	File "lib\lib2to3\btm_matcher.pyc"
	File "lib\lib2to3\btm_utils.pyc"
	File "lib\lib2to3\fixer_util.pyc"
	File "lib\lib2to3\Grammar.txt"
	File "lib\lib2to3\Grammar3.7.5.final.0.pickle"
	File "lib\lib2to3\patcomp.pyc"
	File "lib\lib2to3\PatternGrammar.txt"
	File "lib\lib2to3\PatternGrammar3.7.5.final.0.pickle"
	File "lib\lib2to3\pygram.pyc"
	File "lib\lib2to3\pytree.pyc"
	File "lib\lib2to3\refactor.pyc"
	File "lib\lib2to3\__init__.pyc"
SetOutPath "$INSTDIR\lib\lib2to3\pgen2"
	File "lib\lib2to3\pgen2\driver.pyc"
	File "lib\lib2to3\pgen2\grammar.pyc"
	File "lib\lib2to3\pgen2\literals.pyc"
	File "lib\lib2to3\pgen2\parse.pyc"
	File "lib\lib2to3\pgen2\pgen.pyc"
	File "lib\lib2to3\pgen2\token.pyc"
	File "lib\lib2to3\pgen2\tokenize.pyc"
	File "lib\lib2to3\pgen2\__init__.pyc"
SetOutPath "$INSTDIR\lib\lib2to3\tests\data"
	File "lib\lib2to3\tests\data\README"
SetOutPath "$INSTDIR\lib\logging"
	File "lib\logging\__init__.pyc"
SetOutPath "$INSTDIR\lib\multiprocessing"
	File "lib\multiprocessing\connection.pyc"
	File "lib\multiprocessing\context.pyc"
	File "lib\multiprocessing\forkserver.pyc"
	File "lib\multiprocessing\heap.pyc"
	File "lib\multiprocessing\managers.pyc"
	File "lib\multiprocessing\pool.pyc"
	File "lib\multiprocessing\popen_fork.pyc"
	File "lib\multiprocessing\popen_forkserver.pyc"
	File "lib\multiprocessing\popen_spawn_posix.pyc"
	File "lib\multiprocessing\popen_spawn_win32.pyc"
	File "lib\multiprocessing\process.pyc"
	File "lib\multiprocessing\queues.pyc"
	File "lib\multiprocessing\reduction.pyc"
	File "lib\multiprocessing\resource_sharer.pyc"
	File "lib\multiprocessing\semaphore_tracker.pyc"
	File "lib\multiprocessing\sharedctypes.pyc"
	File "lib\multiprocessing\spawn.pyc"
	File "lib\multiprocessing\synchronize.pyc"
	File "lib\multiprocessing\util.pyc"
	File "lib\multiprocessing\__init__.pyc"
SetOutPath "$INSTDIR\lib\multiprocessing\dummy"
	File "lib\multiprocessing\dummy\connection.pyc"
	File "lib\multiprocessing\dummy\__init__.pyc"
SetOutPath "$INSTDIR\lib\pkg_resources"
	File "lib\pkg_resources\py2_warn.pyc"
	File "lib\pkg_resources\py31compat.pyc"
	File "lib\pkg_resources\__init__.pyc"
SetOutPath "$INSTDIR\lib\pkg_resources\extern"
	File "lib\pkg_resources\extern\__init__.pyc"
SetOutPath "$INSTDIR\lib\pkg_resources\_vendor"
	File "lib\pkg_resources\_vendor\appdirs.pyc"
	File "lib\pkg_resources\_vendor\pyparsing.pyc"
	File "lib\pkg_resources\_vendor\six.pyc"
	File "lib\pkg_resources\_vendor\__init__.pyc"
SetOutPath "$INSTDIR\lib\pkg_resources\_vendor\packaging"
	File "lib\pkg_resources\_vendor\packaging\markers.pyc"
	File "lib\pkg_resources\_vendor\packaging\requirements.pyc"
	File "lib\pkg_resources\_vendor\packaging\specifiers.pyc"
	File "lib\pkg_resources\_vendor\packaging\utils.pyc"
	File "lib\pkg_resources\_vendor\packaging\version.pyc"
	File "lib\pkg_resources\_vendor\packaging\_compat.pyc"
	File "lib\pkg_resources\_vendor\packaging\_structures.pyc"
	File "lib\pkg_resources\_vendor\packaging\__about__.pyc"
	File "lib\pkg_resources\_vendor\packaging\__init__.pyc"
SetOutPath "$INSTDIR\lib\pycparser"
	File "lib\pycparser\ast_transforms.pyc"
	File "lib\pycparser\c_ast.pyc"
	File "lib\pycparser\c_lexer.pyc"
	File "lib\pycparser\c_parser.pyc"
	File "lib\pycparser\lextab.pyc"
	File "lib\pycparser\plyparser.pyc"
	File "lib\pycparser\yacctab.pyc"
	File "lib\pycparser\_c_ast.cfg"
	File "lib\pycparser\__init__.pyc"
SetOutPath "$INSTDIR\lib\pycparser\ply"
	File "lib\pycparser\ply\lex.pyc"
	File "lib\pycparser\ply\yacc.pyc"
	File "lib\pycparser\ply\__init__.pyc"
SetOutPath "$INSTDIR\lib\pydoc_data"
	File "lib\pydoc_data\topics.pyc"
	File "lib\pydoc_data\_pydoc.css"
	File "lib\pydoc_data\__init__.pyc"
SetOutPath "$INSTDIR\lib\requests"
	File "lib\requests\adapters.pyc"
	File "lib\requests\api.pyc"
	File "lib\requests\auth.pyc"
	File "lib\requests\certs.pyc"
	File "lib\requests\compat.pyc"
	File "lib\requests\cookies.pyc"
	File "lib\requests\exceptions.pyc"
	File "lib\requests\hooks.pyc"
	File "lib\requests\models.pyc"
	File "lib\requests\packages.pyc"
	File "lib\requests\sessions.pyc"
	File "lib\requests\status_codes.pyc"
	File "lib\requests\structures.pyc"
	File "lib\requests\utils.pyc"
	File "lib\requests\_internal_utils.pyc"
	File "lib\requests\__init__.pyc"
	File "lib\requests\__version__.pyc"
SetOutPath "$INSTDIR\lib\requests\packages\urllib3"
	File "lib\requests\packages\urllib3\connection.pyc"
	File "lib\requests\packages\urllib3\connectionpool.pyc"
	File "lib\requests\packages\urllib3\exceptions.pyc"
	File "lib\requests\packages\urllib3\fields.pyc"
	File "lib\requests\packages\urllib3\filepost.pyc"
	File "lib\requests\packages\urllib3\poolmanager.pyc"
	File "lib\requests\packages\urllib3\request.pyc"
	File "lib\requests\packages\urllib3\response.pyc"
	File "lib\requests\packages\urllib3\_collections.pyc"
	File "lib\requests\packages\urllib3\__init__.pyc"
SetOutPath "$INSTDIR\lib\requests\packages\urllib3\contrib"
	File "lib\requests\packages\urllib3\contrib\_appengine_environ.pyc"
	File "lib\requests\packages\urllib3\contrib\__init__.pyc"
SetOutPath "$INSTDIR\lib\requests\packages\urllib3\packages"
	File "lib\requests\packages\urllib3\packages\six.pyc"
	File "lib\requests\packages\urllib3\packages\__init__.pyc"
SetOutPath "$INSTDIR\lib\requests\packages\urllib3\packages\ssl_match_hostname"
	File "lib\requests\packages\urllib3\packages\ssl_match_hostname\_implementation.pyc"
	File "lib\requests\packages\urllib3\packages\ssl_match_hostname\__init__.pyc"
SetOutPath "$INSTDIR\lib\requests\packages\urllib3\util"
	File "lib\requests\packages\urllib3\util\connection.pyc"
	File "lib\requests\packages\urllib3\util\queue.pyc"
	File "lib\requests\packages\urllib3\util\request.pyc"
	File "lib\requests\packages\urllib3\util\response.pyc"
	File "lib\requests\packages\urllib3\util\retry.pyc"
	File "lib\requests\packages\urllib3\util\ssl_.pyc"
	File "lib\requests\packages\urllib3\util\timeout.pyc"
	File "lib\requests\packages\urllib3\util\url.pyc"
	File "lib\requests\packages\urllib3\util\wait.pyc"
	File "lib\requests\packages\urllib3\util\__init__.pyc"
SetOutPath "$INSTDIR\lib\setuptools"
	File "lib\setuptools\archive_util.pyc"
	File "lib\setuptools\cli-32.exe"
	File "lib\setuptools\cli-64.exe"
	File "lib\setuptools\cli.exe"
	File "lib\setuptools\config.pyc"
	File "lib\setuptools\depends.pyc"
	File "lib\setuptools\dist.pyc"
	File "lib\setuptools\extension.pyc"
	File "lib\setuptools\glob.pyc"
	File "lib\setuptools\gui-32.exe"
	File "lib\setuptools\gui-64.exe"
	File "lib\setuptools\gui.exe"
	File "lib\setuptools\installer.pyc"
	File "lib\setuptools\lib2to3_ex.pyc"
	File "lib\setuptools\monkey.pyc"
	File "lib\setuptools\package_index.pyc"
	File "lib\setuptools\py27compat.pyc"
	File "lib\setuptools\py31compat.pyc"
	File "lib\setuptools\py33compat.pyc"
	File "lib\setuptools\py34compat.pyc"
	File "lib\setuptools\sandbox.pyc"
	File "lib\setuptools\script (dev).tmpl"
	File "lib\setuptools\script.tmpl"
	File "lib\setuptools\ssl_support.pyc"
	File "lib\setuptools\unicode_utils.pyc"
	File "lib\setuptools\version.pyc"
	File "lib\setuptools\wheel.pyc"
	File "lib\setuptools\windows_support.pyc"
	File "lib\setuptools\_deprecation_warning.pyc"
	File "lib\setuptools\_imp.pyc"
	File "lib\setuptools\__init__.pyc"
SetOutPath "$INSTDIR\lib\setuptools\command"
	File "lib\setuptools\command\bdist_egg.pyc"
	File "lib\setuptools\command\build_ext.pyc"
	File "lib\setuptools\command\build_py.pyc"
	File "lib\setuptools\command\easy_install.pyc"
	File "lib\setuptools\command\egg_info.pyc"
	File "lib\setuptools\command\install_scripts.pyc"
	File "lib\setuptools\command\launcher manifest.xml"
	File "lib\setuptools\command\py36compat.pyc"
	File "lib\setuptools\command\sdist.pyc"
	File "lib\setuptools\command\setopt.pyc"
	File "lib\setuptools\command\__init__.pyc"
SetOutPath "$INSTDIR\lib\setuptools\extern"
	File "lib\setuptools\extern\__init__.pyc"
SetOutPath "$INSTDIR\lib\svtplay_dl"
	File "lib\svtplay_dl\error.pyc"
	File "lib\svtplay_dl\__init__.pyc"
	File "lib\svtplay_dl\__version__.pyc"
SetOutPath "$INSTDIR\lib\svtplay_dl\fetcher"
	File "lib\svtplay_dl\fetcher\dash.pyc"
	File "lib\svtplay_dl\fetcher\hds.pyc"
	File "lib\svtplay_dl\fetcher\hls.pyc"
	File "lib\svtplay_dl\fetcher\http.pyc"
	File "lib\svtplay_dl\fetcher\__init__.pyc"
SetOutPath "$INSTDIR\lib\svtplay_dl\postprocess"
	File "lib\svtplay_dl\postprocess\__init__.pyc"
SetOutPath "$INSTDIR\lib\svtplay_dl\service"
	File "lib\svtplay_dl\service\aftonbladet.pyc"
	File "lib\svtplay_dl\service\barnkanalen.pyc"
	File "lib\svtplay_dl\service\bigbrother.pyc"
	File "lib\svtplay_dl\service\cmore.pyc"
	File "lib\svtplay_dl\service\disney.pyc"
	File "lib\svtplay_dl\service\dplay.pyc"
	File "lib\svtplay_dl\service\dr.pyc"
	File "lib\svtplay_dl\service\efn.pyc"
	File "lib\svtplay_dl\service\eurosport.pyc"
	File "lib\svtplay_dl\service\expressen.pyc"
	File "lib\svtplay_dl\service\facebook.pyc"
	File "lib\svtplay_dl\service\filmarkivet.pyc"
	File "lib\svtplay_dl\service\flowonline.pyc"
	File "lib\svtplay_dl\service\koket.pyc"
	File "lib\svtplay_dl\service\lemonwhale.pyc"
	File "lib\svtplay_dl\service\mtvnn.pyc"
	File "lib\svtplay_dl\service\mtvservices.pyc"
	File "lib\svtplay_dl\service\nhl.pyc"
	File "lib\svtplay_dl\service\nrk.pyc"
	File "lib\svtplay_dl\service\oppetarkiv.pyc"
	File "lib\svtplay_dl\service\picsearch.pyc"
	File "lib\svtplay_dl\service\pokemon.pyc"
	File "lib\svtplay_dl\service\radioplay.pyc"
	File "lib\svtplay_dl\service\raw.pyc"
	File "lib\svtplay_dl\service\riksdagen.pyc"
	File "lib\svtplay_dl\service\ruv.pyc"
	File "lib\svtplay_dl\service\services.pyc"
	File "lib\svtplay_dl\service\solidtango.pyc"
	File "lib\svtplay_dl\service\sportlib.pyc"
	File "lib\svtplay_dl\service\sr.pyc"
	File "lib\svtplay_dl\service\svt.pyc"
	File "lib\svtplay_dl\service\svtplay.pyc"
	File "lib\svtplay_dl\service\tv4play.pyc"
	File "lib\svtplay_dl\service\twitch.pyc"
	File "lib\svtplay_dl\service\urplay.pyc"
	File "lib\svtplay_dl\service\vg.pyc"
	File "lib\svtplay_dl\service\viaplay.pyc"
	File "lib\svtplay_dl\service\viasatsport.pyc"
	File "lib\svtplay_dl\service\vimeo.pyc"
	File "lib\svtplay_dl\service\youplay.pyc"
	File "lib\svtplay_dl\service\__init__.pyc"
SetOutPath "$INSTDIR\lib\svtplay_dl\subtitle"
	File "lib\svtplay_dl\subtitle\__init__.pyc"
SetOutPath "$INSTDIR\lib\svtplay_dl\tests\dash-manifests"
	File "lib\svtplay_dl\tests\dash-manifests\bbb.mpd"
	File "lib\svtplay_dl\tests\dash-manifests\cmore.mpd"
	File "lib\svtplay_dl\tests\dash-manifests\content_type_adaptationset.mpd"
	File "lib\svtplay_dl\tests\dash-manifests\dash.mpd"
	File "lib\svtplay_dl\tests\dash-manifests\fff.mpd"
	File "lib\svtplay_dl\tests\dash-manifests\hbbbtv.mpd"
	File "lib\svtplay_dl\tests\dash-manifests\mediaPresentationDuration.mpd"
	File "lib\svtplay_dl\tests\dash-manifests\nyaord.svt.mpd"
	File "lib\svtplay_dl\tests\dash-manifests\nyheterclipp.mpd"
	File "lib\svtplay_dl\tests\dash-manifests\svt-klipp-crash.mpd"
	File "lib\svtplay_dl\tests\dash-manifests\svtplay-live.mpd"
	File "lib\svtplay_dl\tests\dash-manifests\svtplay-live2.mpd"
	File "lib\svtplay_dl\tests\dash-manifests\svtvod.mpd"
SetOutPath "$INSTDIR\lib\svtplay_dl\tests\postprocess"
	File "lib\svtplay_dl\tests\postprocess\textfile-service.srt"
	File "lib\svtplay_dl\tests\postprocess\textfile-servicegrej.srt"
	File "lib\svtplay_dl\tests\postprocess\textfile-servicehej.srt"
	File "lib\svtplay_dl\tests\postprocess\textfile-serviceoversattning-lulesamiska.srt"
SetOutPath "$INSTDIR\lib\svtplay_dl\utils"
	File "lib\svtplay_dl\utils\getmedia.pyc"
	File "lib\svtplay_dl\utils\http.pyc"
	File "lib\svtplay_dl\utils\nfo.pyc"
	File "lib\svtplay_dl\utils\output.pyc"
	File "lib\svtplay_dl\utils\parser.pyc"
	File "lib\svtplay_dl\utils\proc.pyc"
	File "lib\svtplay_dl\utils\stream.pyc"
	File "lib\svtplay_dl\utils\terminal.pyc"
	File "lib\svtplay_dl\utils\text.pyc"
	File "lib\svtplay_dl\utils\__init__.pyc"
SetOutPath "$INSTDIR\lib\unittest"
	File "lib\unittest\case.pyc"
	File "lib\unittest\loader.pyc"
	File "lib\unittest\main.pyc"
	File "lib\unittest\result.pyc"
	File "lib\unittest\runner.pyc"
	File "lib\unittest\signals.pyc"
	File "lib\unittest\suite.pyc"
	File "lib\unittest\util.pyc"
	File "lib\unittest\__init__.pyc"
SetOutPath "$INSTDIR\lib\urllib"
	File "lib\urllib\error.pyc"
	File "lib\urllib\parse.pyc"
	File "lib\urllib\request.pyc"
	File "lib\urllib\response.pyc"
	File "lib\urllib\__init__.pyc"
SetOutPath "$INSTDIR\lib\urllib3"
	File "lib\urllib3\connection.pyc"
	File "lib\urllib3\connectionpool.pyc"
	File "lib\urllib3\exceptions.pyc"
	File "lib\urllib3\fields.pyc"
	File "lib\urllib3\filepost.pyc"
	File "lib\urllib3\poolmanager.pyc"
	File "lib\urllib3\request.pyc"
	File "lib\urllib3\response.pyc"
	File "lib\urllib3\_collections.pyc"
	File "lib\urllib3\__init__.pyc"
SetOutPath "$INSTDIR\lib\urllib3\contrib"
	File "lib\urllib3\contrib\pyopenssl.pyc"
	File "lib\urllib3\contrib\socks.pyc"
	File "lib\urllib3\contrib\_appengine_environ.pyc"
	File "lib\urllib3\contrib\__init__.pyc"
SetOutPath "$INSTDIR\lib\urllib3\packages"
	File "lib\urllib3\packages\six.pyc"
	File "lib\urllib3\packages\__init__.pyc"
SetOutPath "$INSTDIR\lib\urllib3\packages\backports"
	File "lib\urllib3\packages\backports\makefile.pyc"
	File "lib\urllib3\packages\backports\__init__.pyc"
SetOutPath "$INSTDIR\lib\urllib3\packages\ssl_match_hostname"
	File "lib\urllib3\packages\ssl_match_hostname\_implementation.pyc"
	File "lib\urllib3\packages\ssl_match_hostname\__init__.pyc"
SetOutPath "$INSTDIR\lib\urllib3\util"
	File "lib\urllib3\util\connection.pyc"
	File "lib\urllib3\util\queue.pyc"
	File "lib\urllib3\util\request.pyc"
	File "lib\urllib3\util\response.pyc"
	File "lib\urllib3\util\retry.pyc"
	File "lib\urllib3\util\ssl_.pyc"
	File "lib\urllib3\util\timeout.pyc"
	File "lib\urllib3\util\url.pyc"
	File "lib\urllib3\util\wait.pyc"
	File "lib\urllib3\util\__init__.pyc"
SetOutPath "$INSTDIR\lib\xml"
	File "lib\xml\__init__.pyc"
SetOutPath "$INSTDIR\lib\xml\etree"
	File "lib\xml\etree\ElementPath.pyc"
	File "lib\xml\etree\ElementTree.pyc"
	File "lib\xml\etree\__init__.pyc"
SetOutPath "$INSTDIR\lib\xml\parsers"
	File "lib\xml\parsers\expat.pyc"
	File "lib\xml\parsers\__init__.pyc"
SetOutPath "$INSTDIR\lib\xmlrpc"
	File "lib\xmlrpc\client.pyc"
	File "lib\xmlrpc\__init__.pyc"
SetOutPath "$INSTDIR\lib\yaml"
	File "lib\yaml\composer.pyc"
	File "lib\yaml\constructor.pyc"
	File "lib\yaml\cyaml.pyc"
	File "lib\yaml\dumper.pyc"
	File "lib\yaml\emitter.pyc"
	File "lib\yaml\error.pyc"
	File "lib\yaml\events.pyc"
	File "lib\yaml\loader.pyc"
	File "lib\yaml\nodes.pyc"
	File "lib\yaml\parser.pyc"
	File "lib\yaml\reader.pyc"
	File "lib\yaml\representer.pyc"
	File "lib\yaml\resolver.pyc"
	File "lib\yaml\scanner.pyc"
	File "lib\yaml\serializer.pyc"
	File "lib\yaml\tokens.pyc"
	File "lib\yaml\__init__.pyc"


; END files

	SetOutPath $INSTDIR
    
   
  ; Write the installation path into the registry
  WriteRegStr HKLM SOFTWARE\${INSTALL_DIR_NAME} "Install_Dir" "$INSTDIR"
  
  ; Write the uninstall keys for Windows
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${DISPLAY_NAME}" "DisplayName" "${DISPLAY_NAME}"
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${DISPLAY_NAME}" "DisplayVersion" "${VER_NAME}"
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${DISPLAY_NAME}" "Publisher" "${COMPANY_NAME}"
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${DISPLAY_NAME}" "UninstallString" '"$INSTDIR\uninstall.exe"'
  WriteRegDWORD HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${DISPLAY_NAME}" "NoModify" 1
  WriteRegDWORD HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${DISPLAY_NAME}" "NoRepair" 1
  WriteUninstaller "uninstall.exe"

  
SectionEnd

; Optional section (can be disabled by the user)
Section "Start Menu Shortcuts"
SetShellVarContext all

   CreateDirectory "$SMPROGRAMS\${DISPLAY_NAME}"
   CreateShortCut "$SMPROGRAMS\${DISPLAY_NAME}\${DISPLAY_NAME} ${VER_NAME}.lnk" "$INSTDIR\${PROG_NAME}-${VER_NAME}.exe" "" "$INSTDIR\${PROG_NAME}-${VER_NAME}.exe" 0
   # CreateShortCut "$SMPROGRAMS\${DISPLAY_NAME}\${DISPLAY_NAME}\Uninstall.lnk" "$INSTDIR\uninstall.exe" "" "$INSTDIR\icons\uninstall.ico" 0
   CreateShortCut "$SMPROGRAMS\${DISPLAY_NAME}\${PROGRAMMERS_WEBSITE}.lnk" "$INSTDIR\${PROGRAMMERS_WEBSITE}.URL" "" "$INSTDIR\${APPICON}" 0
   
SectionEnd

Section "Desktop Shortcut" 
SetShellVarContext all

    SetOutPath "$INSTDIR"
    CreateShortcut "$DESKTOP\${DISPLAY_NAME}-${VER_NAME}.lnk" "$INSTDIR\${PROG_NAME}-${VER_NAME}.exe" "" "$INSTDIR\${APPICON}" 
SectionEnd

;--------------------------------

; Uninstaller

Section "Uninstall"

SetShellVarContext all
  ; Remove shortcuts, if any
  Delete "$SMPROGRAMS\${DISPLAY_NAME}\*.*"
  Delete "$DESKTOP\${DISPLAY_NAME}-${VER_NAME}.lnk"
  Delete "$PROFILE\.${INSTALL_DIR_NAME}\*.*"



  ; Remove registry keys
  DeleteRegKey HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${DISPLAY_NAME}"
  DeleteRegKey HKLM SOFTWARE\${INSTALL_DIR_NAME}

  ; Remove files and uninstaller
  #Delete $INSTDIR\${PROG_NAME}-*.exe


  RMDir /r /REBOOTOK $INSTDIR
  #RMDir $INSTDIR

  #Delete $INSTDIR\*.*





  ; Remove directories used


  RMDir "$PROFILE\.${INSTALL_DIR_NAME}"
  

SectionEnd







Function .onInit


System::Call 'kernel32::GetLocaleInfoA(i 1024, i ${LOCALE_SNATIVELANGNAME}, t .r1, i ${NSIS_MAX_STRLEN}) i r0'





  ReadRegStr $R0 HKLM \
  "Software\Microsoft\Windows\CurrentVersion\Uninstall\${DISPLAY_NAME}" \
  "UninstallString"
  StrCmp $R0 "" done
 
  ${If} $1 == "svenska"
  MessageBox MB_OKCANCEL|MB_ICONINFORMATION \
  " En tidigare version av ${DISPLAY_NAME} r redan installerad. $\n$\nKlicka $\"OK$\" fr att ta bort \
  den tidigare versionen.$\nKlicka $\"Avbryt$\" fr att avbryta uppgraderingen." \
  IDOK uninst
  Abort
  ${Else}
  MessageBox MB_OKCANCEL|MB_ICONINFORMATION \
  " A previous version of ${DISPLAY_NAME} is already installed. $\n$\nClick $\"OK$\" to remove the \
  previous version or $\"Cancel$\" to cancel this upgrade." \
  IDOK uninst
  Abort
  ${EndIf}
  
 
;Run the uninstaller
uninst:

    ClearErrors
     ;ExecWait '$R0 _?=$INSTDIR' ;Do not copy the uninstaller to a temp file
	ExecWait '$R0 _?=$INSTDIR' $stopp
    ;Exec $INSTDIR\uninst.exe ; instead of the ExecWait line

  IfErrors done
 
  done:
 
FunctionEnd


#Function .onInstSuccess
#SetOutPath $INSTDIR

#System::Call 'kernel32::GetLocaleInfoA(i 1024, i ${LOCALE_SNATIVELANGNAME}, t .r1, i ${NSIS_MAX_STRLEN}) i r0'

#${If} $1 == "svenska"
#	  MessageBox MB_YESNO "Vill du lsa om frbttringarna$\nsedan frra versionen?" IDNO NoReadme2 
#	  Exec '"$WINDIR\notepad.exe" readme_sv_SE.txt'
#    NoReadme2:
#${Else}
#	  MessageBox MB_YESNO "Want to read about improvements$\nsince the previous version?" IDNO NoReadme
#	  Exec '"$WINDIR\notepad.exe" readme.txt'
#    NoReadme:
#${EndIf}
 
# FunctionEnd
 
 
