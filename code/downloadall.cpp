/*
    streamCapture2
    Copyright (C) 2016 - 2020 Ingemar Ceicer
    http://ceicer.org/ingemar/
    programmering1 (at) ceicer (dot) org

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
*/
#include "newprg.h"
#include "ui_newprg.h"
#include "info.h"
#include "checkforupdates.h"
#include <QMessageBox>
void Newprg::downloadAll()
{
    avbrutet = false;
    ui->teOut->setTextColor(QColor("black"));
    QString save_all_path;
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME, EXECUTABLE_NAME);
    settings.setIniCodec("UTF-8");
    settings.beginGroup("Path");
    QString defaultdownloadlocation = settings.value("defaultdownloadlocation").toString();
    QString copypath = settings.value("copypath").toString();
    settings.endGroup();

    if(ui->actionDownloadToDefaultLocation->isChecked()) {
        /* */
        const QFileInfo downloadlocation(QDir::toNativeSeparators(defaultdownloadlocation));

        if(!downloadlocation.exists()) {
            QMessageBox::critical(this, DISPLAY_NAME " " VERSION, tr("The default folder for downloading video streams cannot be found.\nDownload is interrupted."));
            ui->teOut->clear();
            return;
        }

        if(!downloadlocation.isWritable()) {
            QMessageBox::critical(this, DISPLAY_NAME " " VERSION, tr("You do not have the right to save to the folder.\nDownload is interrupted."));
            ui->teOut->clear();
            return;
        }

        /* */
        save_all_path = defaultdownloadlocation;
    } else {
        save_all_path = save();
    }

    QString kopierastill;

    if(ui->actionCopyToDefaultLocation->isChecked()) {
        const QFileInfo copypathlocation(QDir::toNativeSeparators(copypath));

        if(!copypathlocation.exists()) {
            QMessageBox::critical(this, DISPLAY_NAME " " VERSION, tr("The default folder for copying video streams cannot be found.\nDownload is interrupted."));
            ui->teOut->clear();
            return;
        } else if(!copypathlocation.isWritable()) {
            QMessageBox::critical(this, DISPLAY_NAME " " VERSION, tr("You do not have the right to save to the folder.\nDownload is interrupted."));
            ui->teOut->clear();
            return;
        } else
            kopierastill = tr("The video files are copied to ") + "\"" + QDir::toNativeSeparators(copypath + "\"");
    }

    if(save_all_path != "nothing") {
        QString verbose;

        if(ui->actionShowMore->isChecked()) {
            verbose = " -v ";
        } else {
            verbose = "";
        }

        ui->teOut->setReadOnly(true);
        ui->actionSaveDownloadList->setEnabled(false);
        QStringList temp;

        foreach(QString listitems, downloadList) {
            int hittat = listitems.indexOf(',', 0);
            temp.append(listitems.left(hittat));
        }

        bool folders = false;

        if(temp.removeDuplicates() > 0) {
            folders = true;
        }

        if(ui->actionCreateFolder->isChecked() || folders) {
            ui->teOut->clear();
        } else {
            ui->teOut->setText(tr("The video streams are saved in ") + "\"" + QDir::toNativeSeparators(save_all_path) + "\"");
        }

        downloadList.removeDuplicates();
        // int i = 1;
        QString preferred, quality, provider, sub, folderaddress;
        //filnummer = 0;
        antalnedladdade = downloadList.size();

        for(int antal = 0; antal < downloadList.size(); antal++) {
            int hitta1, hitta2, hitta3, hitta4;
            address = downloadList.at(antal);
            quality = downloadList.at(antal);
            preferred = downloadList.at(antal);
            provider = downloadList.at(antal);
            sub = downloadList.at(antal);
            hitta1 = address.indexOf(',', 0);
            address = address.left(hitta1);
            folderaddress = address;
            address = "\"" + address + "\"";
            hitta1 = quality.indexOf(',', hitta1);
            hitta2 = quality.indexOf(',', hitta1 + 1);
            quality = quality.mid(hitta1 + 1, hitta2 - hitta1 - 1);
            quality = quality.simplified();
            hitta3 = preferred.indexOf(',', hitta2 + 1);
            preferred = preferred.mid(hitta2 + 1, hitta3 - hitta2 - 1);
            preferred = preferred.simplified();
            preferred = preferred.toLower();
            hitta4 = sub.indexOf(',', hitta3 + 1);
            sub = sub.mid(hitta4 + 1, 4);

            if(provider.lastIndexOf("nopassword") > 0) {
                provider = "-";
            } else {
                provider = provider.mid(hitta3 + 1, hitta4 - hitta3 - 1);
            }

            QString username, password;

            if(provider != "-") {
                QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME, EXECUTABLE_NAME);
                settings.setIniCodec("UTF-8");
                settings.beginGroup("Provider");
                username = settings.value(provider + "/username").toString();
                password = settings.value(provider + "/password").toString();
                {
                    if(password == "") {
                        bool ok;
                        QString newpassword = QInputDialog::getText(nullptr, provider + " " + tr("Enter your password"),
                                              tr("Spaces are not allowed. Use only the characters your streaming provider approves.\nThe Password will not be saved."), QLineEdit::Password,
                                              "", &ok);

                        if(newpassword.indexOf(' ') >= 0)
                            return;

                        if(ok) {
                            secretpassword = newpassword;
                            password = newpassword;
                        } else {
                            return;
                        }
                    }
                }
            }

            // Windows
#ifdef Q_OS_WIN // QT5
            const QString EXECUTE = QDir::toNativeSeparators("\"" + QCoreApplication::applicationDirPath() + "/svtplay-dl.exe\" ");
#endif
            // Linux
#ifdef Q_OS_LINUX
            const QString EXECUTE = QDir::toNativeSeparators("\"" + QCoreApplication::applicationDirPath() + "/svtplay-dl\" ");
#endif
            CommProcess2 = new QProcess(this);
            // SPARA VÄRDET PÅ "PATH" och konkatenera med det nya värdet
            QProcessEnvironment env = QProcessEnvironment::systemEnvironment();
            const QString path = QDir::toNativeSeparators(QCoreApplication::applicationDirPath());
            const QString oldpath = env.value("PATH");
#ifdef Q_OS_LINUX
            env.insert("PATH", path + ":" + oldpath); // Add an environment variable
#endif
#ifdef Q_OS_WIN // QT5
            env.insert("PATH", path + ";" + oldpath); // Add an environment variable
#endif
            CommProcess2->setProcessEnvironment(env);
            CommProcess2->setProcessChannelMode(QProcess::MergedChannels);
            QString u;
            /* MAKE FOLDER */
            QString save_all_path2;
            int hittat = folderaddress.lastIndexOf('/');
            folderaddress = folderaddress.mid(hittat);

            if(ui->actionCreateFolder->isChecked() || folders) {
                QDir dir(save_all_path + folderaddress + "/" + preferred + "_" + quality);

                if(!dir.exists())
                    dir.mkpath(".");

                save_all_path2 = dir.path();
                ui->teOut->append(tr("The video streams are saved in ") + QDir::toNativeSeparators(save_all_path2) + "\"");
            } else {
                save_all_path2 = save_all_path;
            }

            if(ui->actionCopyToDefaultLocation->isChecked())
                ui->teOut->append(kopierastill);

            CommProcess2->setWorkingDirectory(QDir::toNativeSeparators(save_all_path2));
            /* END MAKE FOLDER */

            if(provider != "-") {
                if(sub == "ysub") { // Subtitles
                    if(preferred == "unknown" || quality == "unknown") {
                        u = EXECUTE + verbose + "--username=" + username + " --password=" + password + " -o \"" + folderaddress.mid(1) + "\" " + address;
                    } else {
                        u = EXECUTE + verbose + "--username=" + username + " --password=" + password + " -S --preferred=\"" + preferred + "\" --quality=\"" + quality + "\" -Q 300 --remux -o \"" + folderaddress.mid(1) + "\" " + address;
                    }
                } else {
                    if(preferred == "unknown" || quality == "unknown") {
                        u = EXECUTE + verbose + "--username=" + username + " --password=" + password + " -o \"" + folderaddress.mid(1) + "\" " + address;
                    } else {
                        u = EXECUTE + verbose + "--username=" + username + " --password=" + password + " --preferred=\"" + preferred + "\" --quality=\"" + quality + "\" -Q 300 --remux -o \"" + folderaddress.mid(1) + "\" " + address;
                    }
                }
            } else {
                if(sub == "ysub") {
                    if(preferred == "unknown" || quality == "unknown") {
                        u = EXECUTE + verbose + " -o \"" + folderaddress.mid(1) + "\" " + address;
                    } else {
                        u = EXECUTE + verbose + " -S --preferred=\"" + preferred + "\" --quality=\"" + quality + "\" -Q 300 --remux -o \"" + folderaddress.mid(1) + "\" " + address;
                    }
                } else {
                    if(preferred == "unknown" || quality == "unknown") {
                        u = EXECUTE + verbose + " -o \"" + folderaddress.mid(1) + "\" " + address;
                    } else {
                        u = EXECUTE + verbose + " --preferred=\"" + preferred + "\" --quality=\"" + quality + "\" -Q 300 --remux -o \"" + folderaddress.mid(1) + "\" " + address;
                    }
                }
            }

            ui->teOut->append(tr("Preparing to download") + ": " + address);
            connect(CommProcess2, QOverload<int, QProcess::ExitStatus>::of(&QProcess::finished),
            [ = ](int exitCode, QProcess::ExitStatus exitStatus) {
                statusExit(exitStatus, exitCode);
                QString fil = save_all_path2 + folderaddress + ".mp4";
                QString filnamn = folderaddress.mid(1);

                if(!avbrutet) {
                    if(QFile::exists(QDir::toNativeSeparators(fil))) {
                        ui->teOut->setTextColor(QColor("darkBlue"));
                        ui->teOut->append(tr("Download ") +  tr(" succeeded (") + filnamn + ".mp4)");
                        int langd = fil.size();
                        QString path_undertextfilen = fil.replace(langd - 3, 3, "srt");

                        if(QFile::exists(path_undertextfilen)) {
                            ui->teOut->append(tr("Download ") +  tr(" succeeded (") + filnamn + ".srt)");
                        }

                        ui->teOut->setTextColor(QColor("black"));

                        if(ui->actionCopyToDefaultLocation->isChecked()) {
                            if(ui->actionCreateFolder->isChecked() || folders) {
                                QString tmp = preferred + "_" + quality;
                                copyToDefaultLocation(filnamn + ".mp4", &folderaddress, &tmp);
                            } else {
                                copyToDefaultLocation(filnamn + ".mp4", &folderaddress);
                            }
                        }
                    } else {
                        ui->teOut->setTextColor(QColor("red"));
                        ui->teOut->append(tr("Download ")  + tr(" failed (") + filnamn + ".mp4)");
                        ui->teOut->setTextColor(QColor("black"));
                        avbrutet = false;
                    }
                }

                antalnedladdade--;

                if(antalnedladdade == 0) {
                    if(ui->actionCopyToDefaultLocation->isChecked()) {
                        ui->teOut->append(kopierastill);
                    }

                    ui->teOut->append(tr("Download completed"));
                    ui->teOut->setTextColor(QColor("black"));
                    processpid = 0;
                }

                /* ... */
            });
            connect(CommProcess2, SIGNAL(started()), this, SLOT(onCommProcessStart()));
            CommProcess2->start(u);
            processpid = CommProcess2->processId();
            connect(CommProcess2, &QProcess::readyReadStandardOutput, [this]() {
                QString result(CommProcess2->readAllStandardOutput());

                if(result.contains(".mp4")) {
                }

                if(ui->actionShowMore->isChecked())  {
                    ui->teOut->append(result.trimmed());
                }
            });
        }
    } else {
        ui->teOut->setText(tr("No folder is selected"));
    }
}
