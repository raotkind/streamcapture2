/*
    streamCapture2
    Copyright (C) 2016 - 2020 Ingemar Ceicer
    http://ceicer.org/ingemar/
    programmering1 (at) ceicer (dot) org

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
*/
#include "newprg.h"
#include "ui_newprg.h"
#include "info.h"
#include "checkforupdates.h"
#include <QMessageBox>
/* DOWNLOAD */
void Newprg::download()
{
    ui->teOut->setTextColor(QColor("black"));
    avbrutet = false;
    QString verbose;

    if(ui->actionShowMore->isChecked()) {
        verbose = " -v ";
    } else {
        verbose = "";
    }

    ui->teOut->setReadOnly(true);
    ui->actionSaveDownloadList->setEnabled(false);
    ui->teOut->setText(tr("The request is processed...\nPreparing to download..."));
    /*  */
    ui->actionSaveDownloadList->setEnabled(false);
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME, EXECUTABLE_NAME);
    settings.setIniCodec("UTF-8");
    settings.beginGroup("Path");
    QString defaultdownloadlocation = settings.value("defaultdownloadlocation").toString();
    QString copypath = settings.value("copypath").toString();
    settings.endGroup();
    QString save_path;

    if(ui->actionDownloadToDefaultLocation->isChecked()) {
        save_path = defaultdownloadlocation;
    } else {
        save_path = save();
    }

    if(save_path != "nothing") {
        QString kopierastill;
        QString preferred = ui->leMethod->text();
        preferred = preferred.toLower();
        QString quality = ui->leQuality->text();
        /* CREATE FOLDER */
        QString folderaddress = address;

        // ipac Bugg V0.15.0
        if(folderaddress.right(1) == '/') {
            int storlek = folderaddress.size();
            folderaddress.remove(storlek - 1, 1);
        }

        int hittat = folderaddress.lastIndexOf('/');
        int hittat2 = folderaddress.indexOf('?');
        folderaddress = folderaddress.mid(hittat, hittat2 - hittat);
        QString save_path2 = "";

        if(ui->actionCreateFolder->isChecked()) {
            QDir dir(save_path + folderaddress + "/" + preferred + "_" + quality);

            if(!dir.exists())
                dir.mkpath(".");

            save_path2 = dir.path();
            ui->teOut->setText(tr("The video stream is saved in ") + "\"" + QDir::toNativeSeparators(save_path2 + "\""));
        } else {
            save_path2 = save_path;
            ui->teOut->setText(tr("The video stream is saved in ") + "\"" + QDir::toNativeSeparators(save_path + "\""));
        }

        if(ui->actionDownloadToDefaultLocation->isChecked()) {
            const QFileInfo downloadlocation(QDir::toNativeSeparators(defaultdownloadlocation));

            if(!downloadlocation.exists()) {
                QMessageBox::critical(this, DISPLAY_NAME " " VERSION, tr("The default folder for downloading video streams cannot be found.\nDownload is interrupted."));
                ui->teOut->clear();
                return;
            }

            if(!downloadlocation.isWritable()) {
                QMessageBox::critical(this, DISPLAY_NAME " " VERSION, tr("You do not have the right to save to the folder.\nDownload is interrupted."));
                ui->teOut->clear();
                return;
            }

            kopierastill = tr("The video file is copied to ") + "\"" + QDir::toNativeSeparators(copypath + "\"");
        }

        /* END CREATE FOLDER */
        // Windows ipac
#ifdef Q_OS_WIN // QT5
        const QString EXECUTE = QDir::toNativeSeparators("\"" + QCoreApplication::applicationDirPath() + "/svtplay-dl.exe\" ");
#endif
        // Linux
#ifdef Q_OS_LINUX
        const QString EXECUTE = QDir::toNativeSeparators("\"" + QCoreApplication::applicationDirPath() + "/svtplay-dl\" ");
#endif
        CommProcess2 = new QProcess(nullptr);
        // ipac
        // Linux
//#ifdef Q_OS_LINUX
        // SPARA VÄRDET PÅ "PATH" och konkatenera med det nya värdet
        QProcessEnvironment env = QProcessEnvironment::systemEnvironment();
        const QString path = QDir::toNativeSeparators(QCoreApplication::applicationDirPath());
        const QString oldpath = env.value("PATH");
#ifdef Q_OS_LINUX
        env.insert("PATH", path + ":" + oldpath); // Add an environment variable
#endif
#ifdef Q_OS_WIN // QT5
        env.insert("PATH", path + ";" + oldpath); // Add an environment variable
#endif
        CommProcess2->setProcessEnvironment(env);
        CommProcess2->setProcessChannelMode(QProcess::MergedChannels);
        CommProcess2->setWorkingDirectory(QDir::toNativeSeparators(save_path2));
        QString u;

        if(ui->comboPayTV->currentIndex() == 0) {  // No Password
            if(ui->chbSubtitle->isChecked()) {
                u = EXECUTE + verbose + " -S --preferred=\"" + preferred + "\" --quality=\"" + quality + "\" -Q 300 --remux "  + address;
            } else {
                u = EXECUTE + verbose + " --preferred=\"" + preferred + "\" --quality=\"" + quality + "\" -Q 300 --remux " + address;
            }
        } else { // Password
            /* */
            QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME, EXECUTABLE_NAME);
            settings.setIniCodec("UTF-8");
            QString provider = ui->comboPayTV->currentText();
            settings.beginGroup("Provider");
            QString username = settings.value(provider + "/username").toString();
            QString password = settings.value(provider + "/password").toString();

            if(password == "") {
                password = secretpassword;

                if(password == "") {
                    bool ok;
                    QString newpassword = QInputDialog::getText(nullptr, provider + " " + tr("Enter your password"),
                                          tr("Spaces are not allowed. Use only the characters your streaming provider approves.\nThe Password will not be saved."), QLineEdit::Password,
                                          "", &ok);

                    if(newpassword.indexOf(' ') >= 0)
                        return;

                    if(ok) {
                        secretpassword = newpassword;
                    }
                }
            }

            /* */
            if(ui->chbSubtitle->isChecked()) {
                u = EXECUTE + verbose + " --username=" + username + " " + "--password=" + password +  " -S --preferred=\"" + preferred + "\" --quality=\"" + quality + "\" -Q 300 --remux "  + address;
            } else {
                u = EXECUTE + verbose + "--username=" + username + " " + "--password=" + password + " --preferred=\"" + preferred + "\" --quality=\"" + quality + "\" -Q 300 --remux " + address;
            }
        }

        /* */
        auto *filnamn = new QString;
        ui->teOut->append(tr("Starts downloading: ") + address);
        connect(CommProcess2, SIGNAL(started()), this, SLOT(onCommProcessStart()));
        connect(CommProcess2, &QProcess::readyReadStandardOutput, [this, filnamn]() {
            QString result(CommProcess2->readAllStandardOutput());

            //    if((ui->actionShowMore->isChecked()) || (ui->actionCopyToDefaultLocation->isChecked())) {
            if(result.contains(".mp4")) {
                int stopp = result.indexOf(".mp4") + 4;
                int start = result.lastIndexOf(" ", stopp) + 1;
                int langd = result.size();
                int storlek = langd - start - stopp;
                QString tmp = result.mid(start, storlek);
                tmp = tmp.trimmed();
                *filnamn = tmp;
            }

            //    }

            if(ui->actionShowMore->isChecked()) {
                ui->teOut->append(result.trimmed());
            }
        });
        //  }
        connect(CommProcess2, QOverload<int, QProcess::ExitStatus>::of(&QProcess::finished),
        [ = ](int exitCode, QProcess::ExitStatus exitStatus) {
            if(!avbrutet) {
                ui->teOut->setTextColor(QColor("darkBlue"));
                ui->teOut->append(tr("Download succeeded ") + "(" + *filnamn + ")");
                QString undertexten = *filnamn;
                int langd = undertexten.size();
                undertexten = undertexten.replace(langd - 3, 3, "srt");

                if(QFile::exists(QDir::toNativeSeparators(save_path2 + "/" + undertexten))) {
                    ui->teOut->append(tr("Download succeeded ") + "(" + undertexten + ")");
                }

                ui->teOut->setTextColor(QColor("black"));

                if(ui->actionCopyToDefaultLocation->isChecked()) {
                    copyToDefaultLocation(*filnamn, &folderaddress);
                }

                ui->teOut->append(tr("Download completed"));
                ui->teOut->setTextColor(QColor("black"));

                if(ui->actionCopyToDefaultLocation->isChecked()) {
                    ui->teOut->append(kopierastill);
                }
            } else {
                ui->teOut->setTextColor(QColor("red"));
                ui->teOut->append(tr("Download failed ") + "(" + *filnamn + ")");
                ui->teOut->setTextColor(QColor("black"));
                avbrutet = false;
            }

            statusExit(exitStatus, exitCode);
            /* ... */
        });
        CommProcess2->start(u);
        processpid = CommProcess2->processId();
    } else
        ui->teOut->setText(tr("No folder is selected"));
}

/* END DOWNLOAD */
void Newprg::initSok()
{
    auto *prg = new QString("svtplay-dl");

    if(!prgExists(prg))
        return;

    ui->teOut->setTextColor("black");
    ui->teOut->setReadOnly(true);
    ui->actionSaveDownloadList->setEnabled(false);
    ui->comboBox->clear();
    ui->leQuality->setText(tr("Searching..."));
    ui->leMethod->setText(tr("Searching..."));
    ui->teOut->setText(tr("The request is processed...\nStarting search..."));
}
