#include "newprg.h"
#include "ui_newprg.h"
#include "info.h"
#include "checkforupdates.h"
void Newprg::english()
{
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME, EXECUTABLE_NAME);
    settings.setIniCodec("UTF-8");
    settings.beginGroup("Language");
    QString sp = settings.value("language", "none").toString();
    settings.endGroup();

    if(sp != "en_US") {
        switch(QMessageBox::information(this, DISPLAY_NAME " " VERSION,
                                        tr("The program must be restarted for the new language settings to take effect."),
                                        tr("Restart Now"),
                                        tr("Cancel"), nullptr, 2)) {
            case 0:
                settings.beginGroup("Language");
                settings.setValue("language", "en_US");
                settings.endGroup();
                const QString EXECUTE = QDir::toNativeSeparators("\"" + QCoreApplication::applicationDirPath() + "/" + QFileInfo(QCoreApplication::applicationFilePath()).fileName() + "\"");
                //setEndConfig();
                QProcess::startDetached(EXECUTE);
                close();
        }
    }
}

void Newprg::swedish()
{
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME, EXECUTABLE_NAME);
    settings.setIniCodec("UTF-8");
    settings.beginGroup("Language");
    QString sp = settings.value("language", "none").toString();
    settings.endGroup();

    if(sp != "sv_SE") {
        switch(QMessageBox::information(this, DISPLAY_NAME " " VERSION,
                                        tr("The program must be restarted for the new language settings to take effect."),
                                        tr("Restart Now"),
                                        tr("Cancel"), nullptr, 2)) {
            case 0:
                settings.beginGroup("Language");
                settings.setValue("language", "sv_SE");
                settings.endGroup();
                const QString EXECUTE = QDir::toNativeSeparators("\"" + QCoreApplication::applicationDirPath() + "/" + QFileInfo(QCoreApplication::applicationFilePath()).fileName() + "\"");
                //setEndConfig();
                QProcess::startDetached(EXECUTE);
                close();
        }
    }
}

void Newprg::german()
{
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME, EXECUTABLE_NAME);
    settings.setIniCodec("UTF-8");
    settings.beginGroup("Language");
    QString sp = settings.value("language", "none").toString();
    settings.endGroup();

    if(sp != "de_DE") {
        switch(QMessageBox::information(this, DISPLAY_NAME " " VERSION,
                                        tr("The program must be restarted for the new language settings to take effect."),
                                        tr("Restart Now"),
                                        tr("Cancel"), nullptr, 2)) {
            case 0:
                settings.beginGroup("Language");
                settings.setValue("language", "de_DE");
                settings.endGroup();
                const QString EXECUTE = QDir::toNativeSeparators("\"" + QCoreApplication::applicationDirPath() + "/" + QFileInfo(QCoreApplication::applicationFilePath()).fileName() + "\"");
                //setEndConfig();
                QProcess::startDetached(EXECUTE);
                close();
        }
    }
}
