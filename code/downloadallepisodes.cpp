/*
    streamCapture2
    Copyright (C) 2016 - 2020 Ingemar Ceicer
    http://ceicer.org/ingemar/
    programmering1 (at) ceicer (dot) org

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
*/
#include "newprg.h"
#include "ui_newprg.h"
#include "info.h"
void Newprg::downloadAllEpisodes()
{
    if(ui->actionCopyToDefaultLocation->isChecked()) {
        QMessageBox::warning(this, DISPLAY_NAME " " VERSION, tr("You have chosen to copy to \"Default folder\".\nUnfortunately, this does not work when you select \"Download all episodes\"."));
    }

    if(ui->actionCreateFolder->isChecked()) {
        QMessageBox::StandardButton reply = QMessageBox::information(this, DISPLAY_NAME " " VERSION, tr("Because you cannot select \"Method\" and \"Quality\" when you select \"Download all episodes\", no folders will be created.\n\nDo you want to start the download?"),
                                            QMessageBox::Yes | QMessageBox::No, QMessageBox::No);

        if(reply == QMessageBox::No)
            return;
    }

#ifdef Q_OS_LINUX
    QMessageBox::StandardButton reply = QMessageBox::information(this, DISPLAY_NAME " " VERSION, tr("Once svtplay-dl has started downloading all episodes, streamCapture2 no longer has control. "
                                        "If you want to cancel, you may need to log out or restart your computer.\n\nDo you want to start the download?"),
                                        QMessageBox::Yes | QMessageBox::No, QMessageBox::No);

    if(reply == QMessageBox::No)
        return;

#endif
    ui->teOut->setTextColor(QColor("black"));
    QString verbose;
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME, EXECUTABLE_NAME);
    settings.setIniCodec("UTF-8");
    settings.beginGroup("Path");
    QString defaultdownloadlocation = settings.value("defaultdownloadlocation").toString();
    settings.endGroup();

    if(ui->actionShowMore->isChecked()) {
        verbose = " -v ";
    } else {
        verbose = "";
    }

    ui->teOut->setReadOnly(true);
    ui->actionSaveDownloadList->setEnabled(false);
    ui->teOut->setText(tr("The request is processed...\nPreparing to download..."));
    /*  */
    ui->actionSaveDownloadList->setEnabled(false);
    QString save_path;

    if(ui->actionDownloadToDefaultLocation->isChecked()) {
        /* */
        const QFileInfo downloadlocation(QDir::toNativeSeparators(defaultdownloadlocation));

        if(!downloadlocation.exists()) {
            QMessageBox::critical(this, DISPLAY_NAME " " VERSION, tr("The default folder for downloading video streams cannot be found.\nDownload is interrupted."));
            ui->teOut->clear();
            return;
        }

        if(!downloadlocation.isWritable()) {
            QMessageBox::critical(this, DISPLAY_NAME " " VERSION, tr("You do not have the right to save to the folder.\nDownload is interrupted."));
            ui->teOut->clear();
            return;
        }

        /* */
        save_path = defaultdownloadlocation;
    } else {
        save_path = save();
    }

    if(save_path != "nothing") {
        /* END CREATE FOLDER */
        // Windows ipac
#ifdef Q_OS_WIN // QT5
        const QString EXECUTE = QDir::toNativeSeparators("\"" + QCoreApplication::applicationDirPath() + "/svtplay-dl.exe\" ");
#endif
        // Linux
#ifdef Q_OS_LINUX
        const QString EXECUTE = QDir::toNativeSeparators("\"" + QCoreApplication::applicationDirPath() + "/svtplay-dl\" ");
#endif
        CommProcess2 = new QProcess(nullptr);
        // ipac
        // Linux
//#ifdef Q_OS_LINUX
        // SPARA VÄRDET PÅ "PATH" och konkatenera med det nya värdet
        QProcessEnvironment env = QProcessEnvironment::systemEnvironment();
        const QString path = QDir::toNativeSeparators(QCoreApplication::applicationDirPath());
        const QString oldpath = env.value("PATH");
#ifdef Q_OS_LINUX
        env.insert("PATH", path + ":" + oldpath); // Add an environment variable
#endif
#ifdef Q_OS_WIN // QT5
        env.insert("PATH", path + ";" + oldpath); // Add an environment variable
#endif
        CommProcess2->setProcessEnvironment(env);
        //
//#endif
        CommProcess2->setProcessChannelMode(QProcess::MergedChannels);
        //  CommProcess2->setWorkingDirectory(QCoreApplication::applicationDirPath());
        CommProcess2->setWorkingDirectory(save_path);
        QString u;

        if(ui->comboPayTV->currentIndex() == 0) {  // No Password
            if(ui->chbSubtitle->isChecked()) {
                u = EXECUTE + verbose + " -A -S --remux " + address;
            } else {
                u = EXECUTE + verbose + " -A  --remux " + address;
            }
        } else { // Password
            /* */
            QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME, EXECUTABLE_NAME);
            settings.setIniCodec("UTF-8");
            QString provider = ui->comboPayTV->currentText();
            settings.beginGroup("Provider");
            QString username = settings.value(provider + "/username").toString();
            QString password = settings.value(provider + "/password").toString();

            if(password == "") {
                password = secretpassword;

                if(password == "") {
                    bool ok;
                    QString newpassword = QInputDialog::getText(nullptr, provider + " " + tr("Enter your password"),
                                          tr("Spaces are not allowed. Use only the characters your streaming provider approves.\nThe Password will not be saved."), QLineEdit::Password,
                                          "", &ok);

                    if(newpassword.indexOf(' ') >= 0)
                        return;

                    if(ok) {
                        secretpassword = newpassword;
                    }
                }
            }

            /* */
            if(ui->chbSubtitle->isChecked()) {
                u = EXECUTE + verbose + " --username=" + username + " " + "--password=" + password +  " -A -S --remux  " + address;
            } else {
                u = EXECUTE + verbose + "--username=" + username + " " + "--password=" + password + " -A  --remux " + address;
            }
        }

        /* */
        ui->teOut->append(tr("Starts downloading all episodes: ") + address);
        connect(CommProcess2, SIGNAL(started()), this, SLOT(onCommProcessStart()));
        connect(CommProcess2, &QProcess::readyReadStandardOutput, [this]() {
            if(ui->actionShowMore->isChecked()) {
                QString result(CommProcess2->readAllStandardOutput());
                ui->teOut->append(result.trimmed());
            }
        });
        connect(CommProcess2, QOverload<int, QProcess::ExitStatus>::of(&QProcess::finished),
        [ = ](int exitCode, QProcess::ExitStatus exitStatus) {
            statusExit(exitStatus, exitCode);

            if(!avbrutet) {
                ui->teOut->setTextColor(QColor("darkBlue"));
                ui->teOut->append(tr("The media files (and if you have selected the subtitles) have been downloaded."));
                ui->teOut->setTextColor(QColor("black"));
                ui->teOut->setTextColor(QColor("black"));
            } else {
                ui->teOut->setTextColor(QColor("red"));
                ui->teOut->append(tr("Download failed"));
                ui->teOut->setTextColor(QColor("black"));
                avbrutet = false;
            }

            /* ... */
        });
        CommProcess2->start(u);
        processpid = CommProcess2->processId();
    } else
        ui->teOut->setText(tr("No folder is selected"));
}
