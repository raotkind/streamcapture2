/*
    streamCapture2
    Copyright (C) 2016 - 2020 Ingemar Ceicer
    http://ceicer.org/ingemar/
    programmering1 (at) ceicer (dot) org

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
*/
#include "newprg.h"
#include "ui_newprg.h"
#include "info.h"
#include "checkforupdates.h"
#include <QMessageBox>

/* SOK */
void Newprg::sok()
{
    ui->teOut->setTextColor("black");
    ui->pbSok->setEnabled(false);
    ui->actionSearch->setEnabled(false);
    ui->actionListAllEpisodes->setEnabled(false);
    QTimer::singleShot(2000, this, [this] {
        ui->pbSok->setEnabled(true);
        ui->actionSearch->setEnabled(true);
        ui->actionListAllEpisodes->setEnabled(true);
    });
    ui->teOut->setReadOnly(true);
    ui->actionSaveDownloadList->setEnabled(false);
    QString sok = ui->leSok->text();
    sok = sok.simplified();

    if(sok == "") {
        ui->teOut->setText(tr("The search field is empty!"));
        ui->leQuality->setText("");
        ui->leMethod->setText("");
    } else if((sok.left(7) != "http://") && (sok.left(8) != "https://")) {
        ui->teOut->setText(tr("Incorrect URL"));
        ui->leQuality->setText("");
        ui->leMethod->setText("");
    } else {
        address = ui->leSok->text().trimmed();
        int questionmark = address.indexOf('?');
        address = address.left(questionmark);
        // Hit
        QString EXECUTE;

        if(ui->comboPayTV->currentIndex() == 0) { // No Password
            QString test = doTestSearch(address);

            if(test.trimmed() != "") {
                ui->teOut->setText(test);
                return;
            }

            if(test != "") {
                ui->teOut->setText(test);
                //   return;
            }

            // Windows
#ifdef Q_OS_WIN // Windows 32- and 64-bit
            EXECUTE = QDir::toNativeSeparators("\"" + QCoreApplication::applicationDirPath() + "/svtplay-dl.exe\" ")  + address;
#endif
#ifdef Q_OS_LINUX // Linux
            EXECUTE = QDir::toNativeSeparators("\"" + QCoreApplication::applicationDirPath() + "/svtplay-dl\" ")  + address;
#endif
        } else {  // Password
            QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME, EXECUTABLE_NAME);
            settings.setIniCodec("UTF-8");
            QString provider = ui->comboPayTV->currentText();
            settings.beginGroup("Provider");
            QString username = settings.value(provider + "/username").toString();
            QString password = settings.value(provider + "/password").toString();

            if(password == "") {
                password = secretpassword;

                if(password == "") {
                    bool ok;
                    QString newpassword = QInputDialog::getText(nullptr, provider + " " + tr("Enter your password"),
                                          tr("Spaces are not allowed. Use only the characters your streaming provider approves.\nThe Password will not be saved."), QLineEdit::Password,
                                          "", &ok);

                    if(newpassword.indexOf(' ') >= 0)
                        return;

                    if(ok) {
                        secretpassword = newpassword;
                        password = newpassword;
                    }
                }
            }

            QString test = doTestSearch(address, username, password);


            if(test.trimmed() != "") {
                ui->teOut->setText(test);
                return;
            }

            // Windows
#ifdef Q_OS_WIN // Windows 32- and 64-bit
            EXECUTE = QDir::toNativeSeparators("\"" + QCoreApplication::applicationDirPath() + "/svtplay-dl.exe\" ") +  " --username=" + username + " " + "--password=" + password + " " + address;
#endif
#ifdef Q_OS_LINUX // Linux
            EXECUTE = QDir::toNativeSeparators("\"" + QCoreApplication::applicationDirPath() + "/svtplay-dl\" ") + " --username=" + username + " " + "--password=" + password + " " + address;
#endif
        }

        CommProcess2 = new QProcess(nullptr);
        CommProcess2->setProcessChannelMode(QProcess::MergedChannels);
        CommProcess2->start(EXECUTE + " --list-quality");
        processpid = CommProcess2->processId();
        connect(CommProcess2, SIGNAL(finished(int, QProcess::ExitStatus)), this, SLOT(onCommProcessExit_sok(int, QProcess::ExitStatus)));
        connect(CommProcess2, &QProcess::readyReadStandardOutput, [this]() {
            QString result(CommProcess2->readAllStandardOutput());
#ifdef Q_OS_LINUX // Linux
            const QString os = "svtplay-dl";
#endif
#ifdef Q_OS_WIN // Windows 32- and 64-bit
            const QString os = "svtplay-dl.exe";
#endif

            if(result.mid(0, 25) == "ERROR: svtplay-dl crashed") {
                ui->teOut->setTextColor(QColor("red"));
                ui->teOut->append(os + tr(" crashed."));
                ui->teOut->append(tr("Can not find any video streams, please check the address."));
                ui->teOut->setTextColor(QColor("black"));
                return;
            }

            if(result.mid(0, 6) != "ERROR:") {
                ui->pbAdd->setEnabled(true);
                ui->actionAdd->setEnabled(true);
                ui->pbDownload->setEnabled(true);
                result = result.simplified();
                result.replace(" ", "");
                QStringList list = result.split("INFO:", QString::SkipEmptyParts);
                list.removeDuplicates();

                foreach(QString temp, list) {
                    //#ifdef Q_OS_LINUX // Linux
                    if(temp.size() > 13) {
                        continue;
                    }

                    //#endif
                    if(temp == "QualityMethod") {
                        temp = "Quality   Method";
                        ui->teOut->append(temp);
                    } else {
                        int h = temp.indexOf(QRegExp("[^0-9.]"), 0);

                        if(temp.size() < 12) {
                            if(h == 3)
                                temp.insert(h, "        ");
                            else
                                temp.insert(h, "      ");
                        }

                        ui->comboBox->addItem(temp);
                        ui->teOut->append(temp);
                    }
                }
            } else {
                ui->teOut->append(result);
            }
        });
    }
}
/* END SOK */
