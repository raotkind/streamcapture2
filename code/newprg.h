/*
    streamCapture2
    Copyright (C) 2016 - 2020 Ingemar Ceicer
    http://ceicer.org/ingemar/
    programmering1 (at) ceicer (dot) org

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
*/
#ifndef NEWPRG_H
#define NEWPRG_H

#include <QMainWindow>
#include <QProcess>
#include <QDesktopWidget>
#include <QProcess>
#include <QClipboard>
#include <QFileDialog>
#include <QInputDialog>
//#include <QSignalMapper>
#include <QTextStream>
#include <QRegExp>
#include <QSettings>
#include <QFontDatabase>
#include <QScreen>
#include <QEvent>
#include <QMouseEvent>
#include <QContextMenuEvent>
#include <QPoint>
#include <QDebug>
#include <QDateEdit>
#include <QTimer>
#include <QMessageBox>
#include <QDesktopServices>

#include "selectfont.h"
namespace Ui
{
class newprg;
}

class Newprg : public QMainWindow
{
    Q_OBJECT

public:
    explicit Newprg(QWidget *parent = nullptr);
    static QString   getSvtplaydlVersion();
    void  setEndConfig();

private slots:

    void about();
    void aboutSvtplayDl();
    void aboutFfmpeg();
    void english();
    void swedish();
    void german();
    void initSok();
    void sok();
    void download();
    void license();
    void licenseFfmpeg();
    void licenseSvtplayDl();
    void versionHistory();
    void slotRecent();
    void downloadAll();
    void onCommProcessStart();
    void onCommProcessExit_sok(int, QProcess::ExitStatus);
    void comboBoxChanged();
    // Pay TV
    void newSupplier();
    void editOrDelete(const QString &);
    void listAllEpisodes();


private:
    Ui::newprg *ui;
    void closeEvent(QCloseEvent *event) override;

    void setStartConfig();

    QStringList recentFiles;
    QStringList downloadList;
    int MAX_RECENT{};
    QString address{};
    QString save();
    void statusExit(QProcess::ExitStatus, int);
    // Pay TV
    bool testinputprovider(QString &, bool &);
    bool testinputusername(const QString &);
    bool testinputpassword(const QString &);
    bool testinputusername_edit(const QString &, const QString &);
    bool testinputpassword_edit(const QString &, const QString &);
    void editprovider(QString);
    void deleteProvider(const QString &);
    QString secretpassword;
    QString doTestSearch(const QString &);
    QString doTestSearch(const QString &, const QString &, const QString &);
    void downloadAllEpisodes();
    void deleteAllSettings();

    QProcess *CommProcess2{};
    int antalnedladdade{};
    qint64 processpid{};
    bool avbrutet{};
    bool deleteSettings{};

    void copyToDefaultLocation(QString, const QString *);
    void copyToDefaultLocation(QString, const QString *, const QString *);

    bool prgExists(QString *);

public slots:
    void setValue(const QFont &font);

};

#endif // NEWPRG_H
