/*
    streamCapture2
    Copyright (C) 2016 - 2020 Ingemar Ceicer
    http://ceicer.org/ingemar/
    programmering1 (at) ceicer (dot) org

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
*/
#include "newprg.h"
#include "info.h"
QString Newprg::save()
{
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME, EXECUTABLE_NAME);
    settings.setIniCodec("UTF-8");
    settings.beginGroup("Path");
    QString savepath = settings.value("savepath").toString();
    settings.endGroup();

    if(savepath == "")
        savepath = QDir::homePath();

    QString dir = QFileDialog::getExistingDirectory(this, tr("Save streaming media to directory"),
                  savepath,
                  QFileDialog::ShowDirsOnly
                  | QFileDialog::DontResolveSymlinks);

    if(dir != "") {
        settings.beginGroup("Path");
        settings.setValue("savepath", dir);
        settings.endGroup();
    } else {
        return "nothing";
    }

    return dir;
}
