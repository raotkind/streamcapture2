/*
    streamCapture2
    Copyright (C) 2016 - 2020 Ingemar Ceicer
    http://ceicer.org/ingemar/
    programmering1 (at) ceicer (dot) org

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
*/
#include "newprg.h"
#include "ui_newprg.h"

QString Newprg::doTestSearch(const QString &address)
{
    const QString goingon = " --silent-semi ";
    //const QString goingon = " --verbose ";
    //const QString goingon = "";
    auto *result = new QString;
#ifdef Q_OS_LINUX // Linux
    QString EXECUTE = QDir::toNativeSeparators("\"" + QCoreApplication::applicationDirPath() + "/svtplay-dl\" ") + goingon + address;
#endif
#ifdef Q_OS_WIN // Windows 32- and 64-bit
    QString EXECUTE = QDir::toNativeSeparators("\"" + QCoreApplication::applicationDirPath() + "/svtplay-dl.exe\" ") + goingon + address;
#endif
    auto *process = new QProcess(this);
    process->waitForFinished(-1);
    process->setProcessChannelMode(QProcess::MergedChannels);
    process->start(EXECUTE + " --list-quality");
    connect(process, &QProcess::readyReadStandardOutput, [process, result ]() {
        *result = *process->readAllStandardOutput();
    });
    process->kill();
    delete process;
    return *result;
}

QString Newprg::doTestSearch(const QString &address, const QString &username, const QString &password)
{
    const QString goingon = " --silent-semi ";
    // const QString goingon = " --verbuse ";
    auto *result = new QString;
#ifdef Q_OS_LINUX // Linux
    QString EXECUTE = QDir::toNativeSeparators("\"" + QCoreApplication::applicationDirPath() + "/svtplay-dl\" ") + goingon + " --username=" + username + " " + "--password=" + password + " " + address;
#endif
#ifdef Q_OS_WIN // Windows 32- and 64-bit
    QString EXECUTE = QDir::toNativeSeparators("\"" + QCoreApplication::applicationDirPath() + "/svtplay-dl.exe\" ") + goingon + " --username=" + username + " " + "--password=" + password + " " + address;
#endif
    auto *process = new QProcess;
    process->waitForFinished(-1);
    process->setProcessChannelMode(QProcess::MergedChannels);
    process->start(EXECUTE + " --list-quality");
    connect(process, &QProcess::readyReadStandardOutput, [process, result ]() {
        *result = *process->readAllStandardOutput();
    });
    process->kill();
    delete process;
    return *result;
}
