/********************************************************************************
** Form generated from reading UI file 'newprg.ui'
**
** Created by: Qt User Interface Compiler version 5.14.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_NEWPRG_H
#define UI_NEWPRG_H

#include <QtCore/QVariant>
#include <QtGui/QIcon>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_newprg
{
public:
    QAction *actionEnglish;
    QAction *actionSwedish;
    QAction *actionCheckOnStart;
    QAction *actionAbout;
    QAction *actionCheckForUpdates;
    QAction *actionExit;
    QAction *actionAboutSvtplayDl;
    QAction *actionAboutFfmpeg;
    QAction *actionSearch;
    QAction *actionPast;
    QAction *actionDownload;
    QAction *actionLicense;
    QAction *actionLicenseSvtplayDl;
    QAction *actionLicenseFfmpeg;
    QAction *actionRecentFiles;
    QAction *actionHelp;
    QAction *actionViewDownloadList;
    QAction *actionDeleteDownloadList;
    QAction *actionDeleteDownloadList_menu;
    QAction *actionGerman;
    QAction *actionVersionHistory;
    QAction *actionCreateNew;
    QAction *actionPasswordX;
    QAction *actionSubtitle;
    QAction *actionVerboseOutput;
    QAction *actionAdd;
    QAction *actionDownloadAll;
    QAction *actionCreateFolder;
    QAction *actionEditDownloadList;
    QAction *actionSaveDownloadList;
    QAction *actionShowMore;
    QAction *actionPassword;
    QAction *actionUninstall_streamCapture;
    QAction *actionDownloadAllEpisodes;
    QAction *actionDownloadAfterDate;
    QAction *actionStopAllDownloads;
    QAction *actionListAllEpisodes;
    QAction *actionDeleteAllSettings2;
    QAction *actionCopyToDefaultLocation;
    QAction *actionSetDefaultCopyLocation;
    QAction *actionSetDefaultDownloadLocation;
    QAction *actionDownloadToDefaultLocation;
    QAction *actionAddAllEpisodesToDownloadList;
    QAction *actionSelectFont;
    QAction *actionSvtplayDlForum;
    QAction *actionMaintenanceTool;
    QAction *actionDeleteAllSettings;
    QWidget *centralWidget;
    QVBoxLayout *verticalLayout_3;
    QVBoxLayout *verticalLayout_4;
    QHBoxLayout *horizontalLayout_2;
    QPushButton *pbPast;
    QLineEdit *leSok;
    QHBoxLayout *horizontalLayout_3;
    QPushButton *pbSok;
    QPushButton *pbAdd;
    QSpacerItem *horizontalSpacer;
    QTextEdit *teOut;
    QHBoxLayout *horizontalLayout;
    QVBoxLayout *verticalLayout;
    QLabel *lblQuality;
    QLabel *leQuality;
    QVBoxLayout *verticalLayout_2;
    QLabel *lblMethod;
    QLabel *leMethod;
    QSpacerItem *horizontalSpacer_2;
    QCheckBox *chbSubtitle;
    QPushButton *pbDownloadAll;
    QPushButton *pbDownload;
    QLabel *lblQualityBitrate;
    QHBoxLayout *horizontalLayout_4;
    QComboBox *comboBox;
    QComboBox *comboPayTV;
    QPushButton *pbPassword;
    QSpacerItem *horizontalSpacer_3;
    QMenuBar *menuBar;
    QMenu *menuFile;
    QMenu *menuLanguage;
    QMenu *menuTools;
    QMenu *menuHelp;
    QMenu *menuRecent;
    QMenu *menuView;
    QMenu *menuPayTV;
    QMenu *menuDownload_all_Episodes;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *newprg)
    {
        if (newprg->objectName().isEmpty())
            newprg->setObjectName(QString::fromUtf8("newprg"));
        newprg->resize(1067, 585);
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(newprg->sizePolicy().hasHeightForWidth());
        newprg->setSizePolicy(sizePolicy);
        newprg->setMinimumSize(QSize(0, 0));
        newprg->setMaximumSize(QSize(16777215, 16777215));
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/images/icon.ico"), QSize(), QIcon::Normal, QIcon::Off);
        newprg->setWindowIcon(icon);
        actionEnglish = new QAction(newprg);
        actionEnglish->setObjectName(QString::fromUtf8("actionEnglish"));
        QIcon icon1;
        icon1.addFile(QString::fromUtf8(":/images/english"), QSize(), QIcon::Normal, QIcon::Off);
        actionEnglish->setIcon(icon1);
        actionSwedish = new QAction(newprg);
        actionSwedish->setObjectName(QString::fromUtf8("actionSwedish"));
        QIcon icon2;
        icon2.addFile(QString::fromUtf8(":/images/swedish"), QSize(), QIcon::Normal, QIcon::Off);
        actionSwedish->setIcon(icon2);
        actionCheckOnStart = new QAction(newprg);
        actionCheckOnStart->setObjectName(QString::fromUtf8("actionCheckOnStart"));
        actionCheckOnStart->setCheckable(true);
        actionAbout = new QAction(newprg);
        actionAbout->setObjectName(QString::fromUtf8("actionAbout"));
        QIcon icon3;
        icon3.addFile(QString::fromUtf8(":/images/about"), QSize(), QIcon::Normal, QIcon::Off);
        actionAbout->setIcon(icon3);
        actionCheckForUpdates = new QAction(newprg);
        actionCheckForUpdates->setObjectName(QString::fromUtf8("actionCheckForUpdates"));
        QIcon icon4;
        icon4.addFile(QString::fromUtf8(":/images/update"), QSize(), QIcon::Normal, QIcon::Off);
        actionCheckForUpdates->setIcon(icon4);
        actionExit = new QAction(newprg);
        actionExit->setObjectName(QString::fromUtf8("actionExit"));
        QIcon icon5;
        icon5.addFile(QString::fromUtf8(":/images/exit"), QSize(), QIcon::Normal, QIcon::Off);
        actionExit->setIcon(icon5);
        actionAboutSvtplayDl = new QAction(newprg);
        actionAboutSvtplayDl->setObjectName(QString::fromUtf8("actionAboutSvtplayDl"));
        QIcon icon6;
        icon6.addFile(QString::fromUtf8(":/images/svtplay-dl.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionAboutSvtplayDl->setIcon(icon6);
        actionAboutFfmpeg = new QAction(newprg);
        actionAboutFfmpeg->setObjectName(QString::fromUtf8("actionAboutFfmpeg"));
        QIcon icon7;
        icon7.addFile(QString::fromUtf8(":/images/FFmpeg"), QSize(), QIcon::Normal, QIcon::Off);
        actionAboutFfmpeg->setIcon(icon7);
        actionSearch = new QAction(newprg);
        actionSearch->setObjectName(QString::fromUtf8("actionSearch"));
        QIcon icon8;
        icon8.addFile(QString::fromUtf8(":/images/search"), QSize(), QIcon::Normal, QIcon::Off);
        actionSearch->setIcon(icon8);
        actionPast = new QAction(newprg);
        actionPast->setObjectName(QString::fromUtf8("actionPast"));
        QIcon icon9;
        icon9.addFile(QString::fromUtf8(":/images/past"), QSize(), QIcon::Normal, QIcon::Off);
        actionPast->setIcon(icon9);
        actionDownload = new QAction(newprg);
        actionDownload->setObjectName(QString::fromUtf8("actionDownload"));
        actionDownload->setEnabled(false);
        QIcon icon10;
        icon10.addFile(QString::fromUtf8(":/images/download"), QSize(), QIcon::Normal, QIcon::Off);
        actionDownload->setIcon(icon10);
        actionLicense = new QAction(newprg);
        actionLicense->setObjectName(QString::fromUtf8("actionLicense"));
        QIcon icon11;
        icon11.addFile(QString::fromUtf8(":/images/gpl.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionLicense->setIcon(icon11);
        actionLicenseSvtplayDl = new QAction(newprg);
        actionLicenseSvtplayDl->setObjectName(QString::fromUtf8("actionLicenseSvtplayDl"));
        QIcon icon12;
        icon12.addFile(QString::fromUtf8(":/images/mit.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionLicenseSvtplayDl->setIcon(icon12);
        actionLicenseFfmpeg = new QAction(newprg);
        actionLicenseFfmpeg->setObjectName(QString::fromUtf8("actionLicenseFfmpeg"));
        QIcon icon13;
        icon13.addFile(QString::fromUtf8(":/images/lgpl.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionLicenseFfmpeg->setIcon(icon13);
        actionRecentFiles = new QAction(newprg);
        actionRecentFiles->setObjectName(QString::fromUtf8("actionRecentFiles"));
        actionHelp = new QAction(newprg);
        actionHelp->setObjectName(QString::fromUtf8("actionHelp"));
        QIcon icon14;
        icon14.addFile(QString::fromUtf8(":/images/help"), QSize(), QIcon::Normal, QIcon::Off);
        actionHelp->setIcon(icon14);
        actionViewDownloadList = new QAction(newprg);
        actionViewDownloadList->setObjectName(QString::fromUtf8("actionViewDownloadList"));
        QIcon icon15;
        icon15.addFile(QString::fromUtf8(":/images/list.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionViewDownloadList->setIcon(icon15);
        actionDeleteDownloadList = new QAction(newprg);
        actionDeleteDownloadList->setObjectName(QString::fromUtf8("actionDeleteDownloadList"));
        actionDeleteDownloadList->setIcon(icon15);
        actionDeleteDownloadList_menu = new QAction(newprg);
        actionDeleteDownloadList_menu->setObjectName(QString::fromUtf8("actionDeleteDownloadList_menu"));
        actionGerman = new QAction(newprg);
        actionGerman->setObjectName(QString::fromUtf8("actionGerman"));
        QIcon icon16;
        icon16.addFile(QString::fromUtf8(":/images/german"), QSize(), QIcon::Normal, QIcon::Off);
        actionGerman->setIcon(icon16);
        actionVersionHistory = new QAction(newprg);
        actionVersionHistory->setObjectName(QString::fromUtf8("actionVersionHistory"));
        QIcon icon17;
        icon17.addFile(QString::fromUtf8(":/images/versionhistory"), QSize(), QIcon::Normal, QIcon::Off);
        actionVersionHistory->setIcon(icon17);
        actionCreateNew = new QAction(newprg);
        actionCreateNew->setObjectName(QString::fromUtf8("actionCreateNew"));
        QIcon icon18;
        icon18.addFile(QString::fromUtf8(":/images/new.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionCreateNew->setIcon(icon18);
        actionPasswordX = new QAction(newprg);
        actionPasswordX->setObjectName(QString::fromUtf8("actionPasswordX"));
        QIcon icon19;
        icon19.addFile(QString::fromUtf8(":/images/password.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionPasswordX->setIcon(icon19);
        actionSubtitle = new QAction(newprg);
        actionSubtitle->setObjectName(QString::fromUtf8("actionSubtitle"));
        actionSubtitle->setCheckable(true);
        QIcon icon20;
        icon20.addFile(QString::fromUtf8(":/images/subtitle.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionSubtitle->setIcon(icon20);
        actionVerboseOutput = new QAction(newprg);
        actionVerboseOutput->setObjectName(QString::fromUtf8("actionVerboseOutput"));
        actionVerboseOutput->setCheckable(true);
        actionAdd = new QAction(newprg);
        actionAdd->setObjectName(QString::fromUtf8("actionAdd"));
        actionAdd->setEnabled(false);
        actionAdd->setIcon(icon15);
        actionDownloadAll = new QAction(newprg);
        actionDownloadAll->setObjectName(QString::fromUtf8("actionDownloadAll"));
        actionDownloadAll->setEnabled(false);
        actionDownloadAll->setIcon(icon15);
        actionCreateFolder = new QAction(newprg);
        actionCreateFolder->setObjectName(QString::fromUtf8("actionCreateFolder"));
        actionCreateFolder->setCheckable(true);
        actionEditDownloadList = new QAction(newprg);
        actionEditDownloadList->setObjectName(QString::fromUtf8("actionEditDownloadList"));
        actionEditDownloadList->setIcon(icon15);
        actionSaveDownloadList = new QAction(newprg);
        actionSaveDownloadList->setObjectName(QString::fromUtf8("actionSaveDownloadList"));
        actionSaveDownloadList->setEnabled(false);
        actionSaveDownloadList->setIcon(icon15);
        actionShowMore = new QAction(newprg);
        actionShowMore->setObjectName(QString::fromUtf8("actionShowMore"));
        actionShowMore->setCheckable(true);
        actionShowMore->setShortcutContext(Qt::WidgetShortcut);
        actionShowMore->setIconVisibleInMenu(true);
        actionPassword = new QAction(newprg);
        actionPassword->setObjectName(QString::fromUtf8("actionPassword"));
        actionUninstall_streamCapture = new QAction(newprg);
        actionUninstall_streamCapture->setObjectName(QString::fromUtf8("actionUninstall_streamCapture"));
        actionDownloadAllEpisodes = new QAction(newprg);
        actionDownloadAllEpisodes->setObjectName(QString::fromUtf8("actionDownloadAllEpisodes"));
        actionDownloadAllEpisodes->setEnabled(false);
        actionDownloadAfterDate = new QAction(newprg);
        actionDownloadAfterDate->setObjectName(QString::fromUtf8("actionDownloadAfterDate"));
        actionStopAllDownloads = new QAction(newprg);
        actionStopAllDownloads->setObjectName(QString::fromUtf8("actionStopAllDownloads"));
        QIcon icon21;
        icon21.addFile(QString::fromUtf8(":/images/kill.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionStopAllDownloads->setIcon(icon21);
        actionListAllEpisodes = new QAction(newprg);
        actionListAllEpisodes->setObjectName(QString::fromUtf8("actionListAllEpisodes"));
        actionDeleteAllSettings2 = new QAction(newprg);
        actionDeleteAllSettings2->setObjectName(QString::fromUtf8("actionDeleteAllSettings2"));
        actionCopyToDefaultLocation = new QAction(newprg);
        actionCopyToDefaultLocation->setObjectName(QString::fromUtf8("actionCopyToDefaultLocation"));
        actionCopyToDefaultLocation->setCheckable(true);
        actionSetDefaultCopyLocation = new QAction(newprg);
        actionSetDefaultCopyLocation->setObjectName(QString::fromUtf8("actionSetDefaultCopyLocation"));
        actionSetDefaultDownloadLocation = new QAction(newprg);
        actionSetDefaultDownloadLocation->setObjectName(QString::fromUtf8("actionSetDefaultDownloadLocation"));
        actionDownloadToDefaultLocation = new QAction(newprg);
        actionDownloadToDefaultLocation->setObjectName(QString::fromUtf8("actionDownloadToDefaultLocation"));
        actionDownloadToDefaultLocation->setCheckable(true);
        actionAddAllEpisodesToDownloadList = new QAction(newprg);
        actionAddAllEpisodesToDownloadList->setObjectName(QString::fromUtf8("actionAddAllEpisodesToDownloadList"));
        actionAddAllEpisodesToDownloadList->setEnabled(false);
        actionSelectFont = new QAction(newprg);
        actionSelectFont->setObjectName(QString::fromUtf8("actionSelectFont"));
        actionSvtplayDlForum = new QAction(newprg);
        actionSvtplayDlForum->setObjectName(QString::fromUtf8("actionSvtplayDlForum"));
        QIcon icon22;
        icon22.addFile(QString::fromUtf8(":/images/forum.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionSvtplayDlForum->setIcon(icon22);
        actionMaintenanceTool = new QAction(newprg);
        actionMaintenanceTool->setObjectName(QString::fromUtf8("actionMaintenanceTool"));
        actionDeleteAllSettings = new QAction(newprg);
        actionDeleteAllSettings->setObjectName(QString::fromUtf8("actionDeleteAllSettings"));
        centralWidget = new QWidget(newprg);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        verticalLayout_3 = new QVBoxLayout(centralWidget);
        verticalLayout_3->setSpacing(6);
        verticalLayout_3->setContentsMargins(11, 11, 11, 11);
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        verticalLayout_4 = new QVBoxLayout();
        verticalLayout_4->setSpacing(6);
        verticalLayout_4->setObjectName(QString::fromUtf8("verticalLayout_4"));
        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        pbPast = new QPushButton(centralWidget);
        pbPast->setObjectName(QString::fromUtf8("pbPast"));
        pbPast->setMinimumSize(QSize(150, 30));
        pbPast->setMaximumSize(QSize(150, 30));
        pbPast->setIcon(icon9);

        horizontalLayout_2->addWidget(pbPast);

        leSok = new QLineEdit(centralWidget);
        leSok->setObjectName(QString::fromUtf8("leSok"));
        leSok->setMinimumSize(QSize(0, 30));
        leSok->setMaximumSize(QSize(16777215, 30));

        horizontalLayout_2->addWidget(leSok);


        verticalLayout_4->addLayout(horizontalLayout_2);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setSpacing(6);
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        pbSok = new QPushButton(centralWidget);
        pbSok->setObjectName(QString::fromUtf8("pbSok"));
        pbSok->setMinimumSize(QSize(150, 30));
        pbSok->setMaximumSize(QSize(150, 30));
        pbSok->setIcon(icon8);

        horizontalLayout_3->addWidget(pbSok);

        pbAdd = new QPushButton(centralWidget);
        pbAdd->setObjectName(QString::fromUtf8("pbAdd"));
        pbAdd->setEnabled(false);
        pbAdd->setMinimumSize(QSize(300, 30));
        pbAdd->setMaximumSize(QSize(300, 30));
        QIcon icon23;
        icon23.addFile(QString::fromUtf8(":/images/list"), QSize(), QIcon::Normal, QIcon::Off);
        pbAdd->setIcon(icon23);

        horizontalLayout_3->addWidget(pbAdd);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer);


        verticalLayout_4->addLayout(horizontalLayout_3);


        verticalLayout_3->addLayout(verticalLayout_4);

        teOut = new QTextEdit(centralWidget);
        teOut->setObjectName(QString::fromUtf8("teOut"));
        QSizePolicy sizePolicy1(QSizePolicy::Preferred, QSizePolicy::Expanding);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(teOut->sizePolicy().hasHeightForWidth());
        teOut->setSizePolicy(sizePolicy1);
        teOut->setMaximumSize(QSize(16777215, 16777215));
        teOut->setBaseSize(QSize(0, 500));
        teOut->setAcceptDrops(false);
        teOut->setStyleSheet(QString::fromUtf8(""));
        teOut->setInputMethodHints(Qt::ImhNone);
        teOut->setUndoRedoEnabled(false);
        teOut->setLineWrapMode(QTextEdit::FixedColumnWidth);
        teOut->setLineWrapColumnOrWidth(200);
        teOut->setReadOnly(true);

        verticalLayout_3->addWidget(teOut);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(6);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        verticalLayout = new QVBoxLayout();
        verticalLayout->setSpacing(0);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        lblQuality = new QLabel(centralWidget);
        lblQuality->setObjectName(QString::fromUtf8("lblQuality"));
        lblQuality->setFrameShape(QFrame::Box);
        lblQuality->setLineWidth(0);
        lblQuality->setWordWrap(false);
        lblQuality->setIndent(0);

        verticalLayout->addWidget(lblQuality);

        leQuality = new QLabel(centralWidget);
        leQuality->setObjectName(QString::fromUtf8("leQuality"));
        leQuality->setInputMethodHints(Qt::ImhPreferNumbers);
        leQuality->setFrameShape(QFrame::NoFrame);
        leQuality->setFrameShadow(QFrame::Plain);
        leQuality->setLineWidth(0);
        leQuality->setTextFormat(Qt::RichText);
        leQuality->setScaledContents(false);
        leQuality->setMargin(0);
        leQuality->setIndent(0);

        verticalLayout->addWidget(leQuality, 0, Qt::AlignVCenter);


        horizontalLayout->addLayout(verticalLayout);

        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setSpacing(0);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        verticalLayout_2->setSizeConstraint(QLayout::SetDefaultConstraint);
        lblMethod = new QLabel(centralWidget);
        lblMethod->setObjectName(QString::fromUtf8("lblMethod"));
        lblMethod->setLayoutDirection(Qt::LeftToRight);
        lblMethod->setAutoFillBackground(false);
        lblMethod->setFrameShape(QFrame::Box);
        lblMethod->setLineWidth(0);
        lblMethod->setIndent(0);

        verticalLayout_2->addWidget(lblMethod);

        leMethod = new QLabel(centralWidget);
        leMethod->setObjectName(QString::fromUtf8("leMethod"));
        leMethod->setLineWidth(0);
        leMethod->setMargin(0);
        leMethod->setIndent(0);

        verticalLayout_2->addWidget(leMethod);


        horizontalLayout->addLayout(verticalLayout_2);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_2);

        chbSubtitle = new QCheckBox(centralWidget);
        chbSubtitle->setObjectName(QString::fromUtf8("chbSubtitle"));
        sizePolicy.setHeightForWidth(chbSubtitle->sizePolicy().hasHeightForWidth());
        chbSubtitle->setSizePolicy(sizePolicy);
        chbSubtitle->setBaseSize(QSize(0, 0));
        chbSubtitle->setIcon(icon20);
        chbSubtitle->setChecked(false);

        horizontalLayout->addWidget(chbSubtitle);

        pbDownloadAll = new QPushButton(centralWidget);
        pbDownloadAll->setObjectName(QString::fromUtf8("pbDownloadAll"));
        pbDownloadAll->setEnabled(false);
        QSizePolicy sizePolicy2(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(pbDownloadAll->sizePolicy().hasHeightForWidth());
        pbDownloadAll->setSizePolicy(sizePolicy2);
        pbDownloadAll->setMinimumSize(QSize(150, 30));
        pbDownloadAll->setMaximumSize(QSize(16777215, 30));
        pbDownloadAll->setIcon(icon23);
        pbDownloadAll->setAutoRepeatDelay(282);

        horizontalLayout->addWidget(pbDownloadAll);

        pbDownload = new QPushButton(centralWidget);
        pbDownload->setObjectName(QString::fromUtf8("pbDownload"));
        pbDownload->setEnabled(false);
        sizePolicy2.setHeightForWidth(pbDownload->sizePolicy().hasHeightForWidth());
        pbDownload->setSizePolicy(sizePolicy2);
        pbDownload->setMinimumSize(QSize(150, 30));
        pbDownload->setMaximumSize(QSize(150, 30));
        pbDownload->setIcon(icon10);

        horizontalLayout->addWidget(pbDownload);


        verticalLayout_3->addLayout(horizontalLayout);

        lblQualityBitrate = new QLabel(centralWidget);
        lblQualityBitrate->setObjectName(QString::fromUtf8("lblQualityBitrate"));
        lblQualityBitrate->setFrameShape(QFrame::Box);
        lblQualityBitrate->setLineWidth(0);

        verticalLayout_3->addWidget(lblQualityBitrate);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setSpacing(6);
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        comboBox = new QComboBox(centralWidget);
        comboBox->setObjectName(QString::fromUtf8("comboBox"));
        sizePolicy.setHeightForWidth(comboBox->sizePolicy().hasHeightForWidth());
        comboBox->setSizePolicy(sizePolicy);
        comboBox->setMinimumSize(QSize(180, 30));
        comboBox->setMaximumSize(QSize(304, 16777215));

        horizontalLayout_4->addWidget(comboBox);

        comboPayTV = new QComboBox(centralWidget);
        comboPayTV->setObjectName(QString::fromUtf8("comboPayTV"));
        sizePolicy.setHeightForWidth(comboPayTV->sizePolicy().hasHeightForWidth());
        comboPayTV->setSizePolicy(sizePolicy);
        comboPayTV->setMinimumSize(QSize(180, 30));
        comboPayTV->setMaximumSize(QSize(304, 16777215));

        horizontalLayout_4->addWidget(comboPayTV);

        pbPassword = new QPushButton(centralWidget);
        pbPassword->setObjectName(QString::fromUtf8("pbPassword"));
        pbPassword->setMinimumSize(QSize(150, 30));
        pbPassword->setIcon(icon19);
        pbPassword->setFlat(false);

        horizontalLayout_4->addWidget(pbPassword);

        horizontalSpacer_3 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_4->addItem(horizontalSpacer_3);


        verticalLayout_3->addLayout(horizontalLayout_4);

        newprg->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(newprg);
        menuBar->setObjectName(QString::fromUtf8("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 1067, 22));
        menuFile = new QMenu(menuBar);
        menuFile->setObjectName(QString::fromUtf8("menuFile"));
        menuLanguage = new QMenu(menuBar);
        menuLanguage->setObjectName(QString::fromUtf8("menuLanguage"));
        menuTools = new QMenu(menuBar);
        menuTools->setObjectName(QString::fromUtf8("menuTools"));
        menuHelp = new QMenu(menuBar);
        menuHelp->setObjectName(QString::fromUtf8("menuHelp"));
        menuRecent = new QMenu(menuBar);
        menuRecent->setObjectName(QString::fromUtf8("menuRecent"));
        menuView = new QMenu(menuBar);
        menuView->setObjectName(QString::fromUtf8("menuView"));
        menuPayTV = new QMenu(menuBar);
        menuPayTV->setObjectName(QString::fromUtf8("menuPayTV"));
        menuDownload_all_Episodes = new QMenu(menuBar);
        menuDownload_all_Episodes->setObjectName(QString::fromUtf8("menuDownload_all_Episodes"));
        newprg->setMenuBar(menuBar);
        statusBar = new QStatusBar(newprg);
        statusBar->setObjectName(QString::fromUtf8("statusBar"));
        newprg->setStatusBar(statusBar);
#if QT_CONFIG(shortcut)
        lblQualityBitrate->setBuddy(comboBox);
#endif // QT_CONFIG(shortcut)
        QWidget::setTabOrder(pbPast, pbSok);
        QWidget::setTabOrder(pbSok, leSok);
        QWidget::setTabOrder(leSok, pbDownload);
        QWidget::setTabOrder(pbDownload, teOut);
        QWidget::setTabOrder(teOut, comboBox);
        QWidget::setTabOrder(comboBox, chbSubtitle);
        QWidget::setTabOrder(chbSubtitle, pbDownloadAll);
        QWidget::setTabOrder(pbDownloadAll, pbAdd);

        menuBar->addAction(menuFile->menuAction());
        menuBar->addAction(menuRecent->menuAction());
        menuBar->addAction(menuView->menuAction());
        menuBar->addAction(menuLanguage->menuAction());
        menuBar->addAction(menuTools->menuAction());
        menuBar->addAction(menuPayTV->menuAction());
        menuBar->addAction(menuDownload_all_Episodes->menuAction());
        menuBar->addAction(menuHelp->menuAction());
        menuFile->addAction(actionPast);
        menuFile->addAction(actionSearch);
        menuFile->addAction(actionDownload);
        menuFile->addAction(actionSubtitle);
        menuFile->addSeparator();
        menuFile->addAction(actionStopAllDownloads);
        menuFile->addAction(actionExit);
        menuLanguage->addAction(actionEnglish);
        menuLanguage->addAction(actionGerman);
        menuLanguage->addAction(actionSwedish);
        menuTools->addAction(actionCheckOnStart);
        menuTools->addAction(actionCreateFolder);
        menuTools->addAction(actionShowMore);
        menuTools->addSeparator();
        menuTools->addAction(actionSetDefaultDownloadLocation);
        menuTools->addAction(actionDownloadToDefaultLocation);
        menuTools->addSeparator();
        menuTools->addAction(actionSetDefaultCopyLocation);
        menuTools->addAction(actionCopyToDefaultLocation);
        menuTools->addAction(actionSelectFont);
        menuTools->addAction(actionMaintenanceTool);
        menuTools->addAction(actionDeleteAllSettings);
        menuHelp->addAction(actionHelp);
        menuHelp->addAction(actionCheckForUpdates);
        menuHelp->addAction(actionVersionHistory);
        menuHelp->addAction(actionAbout);
        menuHelp->addAction(actionAboutSvtplayDl);
        menuHelp->addAction(actionSvtplayDlForum);
        menuHelp->addAction(actionAboutFfmpeg);
        menuHelp->addAction(actionLicense);
        menuHelp->addAction(actionLicenseSvtplayDl);
        menuHelp->addAction(actionLicenseFfmpeg);
        menuHelp->addSeparator();
        menuView->addAction(actionViewDownloadList);
        menuView->addAction(actionAdd);
        menuView->addSeparator();
        menuView->addAction(actionDownloadAll);
        menuView->addSeparator();
        menuView->addAction(actionEditDownloadList);
        menuView->addAction(actionSaveDownloadList);
        menuView->addAction(actionDeleteDownloadList);
        menuPayTV->addAction(actionPassword);
        menuPayTV->addAction(actionCreateNew);
        menuDownload_all_Episodes->addAction(actionListAllEpisodes);
        menuDownload_all_Episodes->addAction(actionAddAllEpisodesToDownloadList);
        menuDownload_all_Episodes->addSeparator();
        menuDownload_all_Episodes->addAction(actionDownloadAllEpisodes);

        retranslateUi(newprg);

        pbPassword->setDefault(false);


        QMetaObject::connectSlotsByName(newprg);
    } // setupUi

    void retranslateUi(QMainWindow *newprg)
    {
        newprg->setWindowTitle(QCoreApplication::translate("newprg", "TEST", nullptr));
        actionEnglish->setText(QCoreApplication::translate("newprg", "English", nullptr));
        actionSwedish->setText(QCoreApplication::translate("newprg", "Swedish", nullptr));
        actionCheckOnStart->setText(QCoreApplication::translate("newprg", "Check for updates at program start", nullptr));
#if QT_CONFIG(statustip)
        actionCheckOnStart->setStatusTip(QCoreApplication::translate("newprg", "Check for software updates each time the program starts.", nullptr));
#endif // QT_CONFIG(statustip)
        actionAbout->setText(QCoreApplication::translate("newprg", "About", nullptr));
        actionCheckForUpdates->setText(QCoreApplication::translate("newprg", "Check for updates", nullptr));
        actionExit->setText(QCoreApplication::translate("newprg", "Exit", nullptr));
#if QT_CONFIG(statustip)
        actionExit->setStatusTip(QCoreApplication::translate("newprg", "Exits the program.", nullptr));
#endif // QT_CONFIG(statustip)
#if QT_CONFIG(shortcut)
        actionExit->setShortcut(QCoreApplication::translate("newprg", "F4", nullptr));
#endif // QT_CONFIG(shortcut)
        actionAboutSvtplayDl->setText(QCoreApplication::translate("newprg", "About svtplay-dl", nullptr));
        actionAboutFfmpeg->setText(QCoreApplication::translate("newprg", "About FFmpeg", nullptr));
        actionSearch->setText(QCoreApplication::translate("newprg", "Search", nullptr));
#if QT_CONFIG(statustip)
        actionSearch->setStatusTip(QCoreApplication::translate("newprg", "Search for video streams.", nullptr));
#endif // QT_CONFIG(statustip)
        actionPast->setText(QCoreApplication::translate("newprg", "Paste", nullptr));
#if QT_CONFIG(statustip)
        actionPast->setStatusTip(QCoreApplication::translate("newprg", "Paste the link to the page where the video is displayed.", nullptr));
#endif // QT_CONFIG(statustip)
        actionDownload->setText(QCoreApplication::translate("newprg", "Download", nullptr));
#if QT_CONFIG(statustip)
        actionDownload->setStatusTip(QCoreApplication::translate("newprg", "Download the stream you just searched for.", nullptr));
#endif // QT_CONFIG(statustip)
        actionLicense->setText(QCoreApplication::translate("newprg", "License", nullptr));
        actionLicenseSvtplayDl->setText(QCoreApplication::translate("newprg", "License svtplay-dl", nullptr));
        actionLicenseFfmpeg->setText(QCoreApplication::translate("newprg", "License FFmpeg", nullptr));
        actionRecentFiles->setText(QCoreApplication::translate("newprg", "Recent files", nullptr));
        actionHelp->setText(QCoreApplication::translate("newprg", "Help", nullptr));
        actionViewDownloadList->setText(QCoreApplication::translate("newprg", "View Download list", nullptr));
#if QT_CONFIG(statustip)
        actionViewDownloadList->setStatusTip(QCoreApplication::translate("newprg", "Look at the list of all the streams to download.", nullptr));
#endif // QT_CONFIG(statustip)
        actionDeleteDownloadList->setText(QCoreApplication::translate("newprg", "Delete download list", nullptr));
#if QT_CONFIG(statustip)
        actionDeleteDownloadList->setStatusTip(QCoreApplication::translate("newprg", "All saved streams in the download list are deleted.", nullptr));
#endif // QT_CONFIG(statustip)
        actionDeleteDownloadList_menu->setText(QCoreApplication::translate("newprg", "Delete download list", nullptr));
        actionGerman->setText(QCoreApplication::translate("newprg", "German", nullptr));
        actionVersionHistory->setText(QCoreApplication::translate("newprg", "Version history", nullptr));
        actionCreateNew->setText(QCoreApplication::translate("newprg", "Create New", nullptr));
#if QT_CONFIG(statustip)
        actionCreateNew->setStatusTip(QCoreApplication::translate("newprg", "Save the name of a video stream provider, your username and, if you want, your password.", nullptr));
#endif // QT_CONFIG(statustip)
        actionPasswordX->setText(QCoreApplication::translate("newprg", "Password", nullptr));
#if QT_CONFIG(statustip)
        actionPasswordX->setStatusTip(QCoreApplication::translate("newprg", "If no saved password is found, click here.", nullptr));
#endif // QT_CONFIG(statustip)
        actionSubtitle->setText(QCoreApplication::translate("newprg", "Include Subtitle", nullptr));
#if QT_CONFIG(statustip)
        actionSubtitle->setStatusTip(QCoreApplication::translate("newprg", "Searching for and downloading subtitles.", nullptr));
#endif // QT_CONFIG(statustip)
        actionVerboseOutput->setText(QCoreApplication::translate("newprg", "Explain what is going on", nullptr));
        actionAdd->setText(QCoreApplication::translate("newprg", "Add to Download list", nullptr));
#if QT_CONFIG(statustip)
        actionAdd->setStatusTip(QCoreApplication::translate("newprg", "Add current video to the list of streams that will be downloaded.", nullptr));
#endif // QT_CONFIG(statustip)
        actionDownloadAll->setText(QCoreApplication::translate("newprg", "Download all", nullptr));
#if QT_CONFIG(statustip)
        actionDownloadAll->setStatusTip(QCoreApplication::translate("newprg", "Download all the streams in the list. If it is the same video stream in different qualities folders are automatically created for each video stream.", nullptr));
#endif // QT_CONFIG(statustip)
        actionCreateFolder->setText(QCoreApplication::translate("newprg", "Create folder \"method_quality\"", nullptr));
#if QT_CONFIG(statustip)
        actionCreateFolder->setStatusTip(QCoreApplication::translate("newprg", "Automatically create a folder for each downloaded video stream.", nullptr));
#endif // QT_CONFIG(statustip)
        actionEditDownloadList->setText(QCoreApplication::translate("newprg", "Edit Download list (Advanced)", nullptr));
#if QT_CONFIG(statustip)
        actionEditDownloadList->setStatusTip(QCoreApplication::translate("newprg", "Change method or quality. Remove a download from the list. NOTE! If you change incorrectly, it will not work.", nullptr));
#endif // QT_CONFIG(statustip)
        actionSaveDownloadList->setText(QCoreApplication::translate("newprg", "Save Download list (Advanced)", nullptr));
#if QT_CONFIG(statustip)
        actionSaveDownloadList->setStatusTip(QCoreApplication::translate("newprg", "Your changes to the download list are saved.", nullptr));
#endif // QT_CONFIG(statustip)
        actionShowMore->setText(QCoreApplication::translate("newprg", "Show more", nullptr));
#if QT_CONFIG(statustip)
        actionShowMore->setStatusTip(QCoreApplication::translate("newprg", "View more information from svtplay-dl.", nullptr));
#endif // QT_CONFIG(statustip)
        actionPassword->setText(QCoreApplication::translate("newprg", "Password", nullptr));
#if QT_CONFIG(statustip)
        actionPassword->setStatusTip(QCoreApplication::translate("newprg", "If no saved password is found, click here.", nullptr));
#endif // QT_CONFIG(statustip)
        actionUninstall_streamCapture->setText(QCoreApplication::translate("newprg", "Uninstall streamCapture", nullptr));
#if QT_CONFIG(statustip)
        actionUninstall_streamCapture->setStatusTip(QCoreApplication::translate("newprg", "Uninstall and remove all components", nullptr));
#endif // QT_CONFIG(statustip)
        actionDownloadAllEpisodes->setText(QCoreApplication::translate("newprg", "Direct Download of all Video Streams in current serie (Not from the Download List)", nullptr));
#if QT_CONFIG(statustip)
        actionDownloadAllEpisodes->setStatusTip(QCoreApplication::translate("newprg", "Searches for all Video Streams in the current series and tries to download them directly.", nullptr));
#endif // QT_CONFIG(statustip)
        actionDownloadAfterDate->setText(QCoreApplication::translate("newprg", "Download after Date...", nullptr));
        actionStopAllDownloads->setText(QCoreApplication::translate("newprg", "Stop all downloads", nullptr));
#if QT_CONFIG(statustip)
        actionStopAllDownloads->setStatusTip(QCoreApplication::translate("newprg", "Trying to stop svtplay-dl.", nullptr));
#endif // QT_CONFIG(statustip)
        actionListAllEpisodes->setText(QCoreApplication::translate("newprg", "List all Video Streams", nullptr));
#if QT_CONFIG(statustip)
        actionListAllEpisodes->setStatusTip(QCoreApplication::translate("newprg", "Looking for Video Streams in the current series.", nullptr));
#endif // QT_CONFIG(statustip)
        actionDeleteAllSettings2->setText(QCoreApplication::translate("newprg", "Delete all settings and Exit", nullptr));
#if QT_CONFIG(statustip)
        actionDeleteAllSettings2->setStatusTip(QCoreApplication::translate("newprg", "All saved searches and the list of streams to be downloaded will be deleted.", nullptr));
#endif // QT_CONFIG(statustip)
        actionCopyToDefaultLocation->setText(QCoreApplication::translate("newprg", "Copy", nullptr));
#if QT_CONFIG(statustip)
        actionCopyToDefaultLocation->setStatusTip(QCoreApplication::translate("newprg", "The completed video file will be copied to the selected location.", nullptr));
#endif // QT_CONFIG(statustip)
#if QT_CONFIG(whatsthis)
        actionCopyToDefaultLocation->setWhatsThis(QString());
#endif // QT_CONFIG(whatsthis)
        actionSetDefaultCopyLocation->setText(QCoreApplication::translate("newprg", "Set Copy Location...", nullptr));
#if QT_CONFIG(statustip)
        actionSetDefaultCopyLocation->setStatusTip(QCoreApplication::translate("newprg", "Save the location where the finished video file is copied.", nullptr));
#endif // QT_CONFIG(statustip)
        actionSetDefaultDownloadLocation->setText(QCoreApplication::translate("newprg", "Set Default Download Location...", nullptr));
#if QT_CONFIG(statustip)
        actionSetDefaultDownloadLocation->setStatusTip(QCoreApplication::translate("newprg", "Save the location for direct download.", nullptr));
#endif // QT_CONFIG(statustip)
        actionDownloadToDefaultLocation->setText(QCoreApplication::translate("newprg", "Download to Default Location", nullptr));
#if QT_CONFIG(statustip)
        actionDownloadToDefaultLocation->setStatusTip(QCoreApplication::translate("newprg", "Direct download to the default location.", nullptr));
#endif // QT_CONFIG(statustip)
        actionAddAllEpisodesToDownloadList->setText(QCoreApplication::translate("newprg", "Add all Video Streams to Download List", nullptr));
#if QT_CONFIG(statustip)
        actionAddAllEpisodesToDownloadList->setStatusTip(QCoreApplication::translate("newprg", "Searches for all Video Streams in the current series and adds them to the download list.", nullptr));
#endif // QT_CONFIG(statustip)
        actionSelectFont->setText(QCoreApplication::translate("newprg", "Select font", nullptr));
        actionSvtplayDlForum->setText(QCoreApplication::translate("newprg", "Visit svtplay-dl forum for issues", nullptr));
        actionMaintenanceTool->setText(QCoreApplication::translate("newprg", "Maintenance Tool", nullptr));
#if QT_CONFIG(statustip)
        actionMaintenanceTool->setStatusTip(QCoreApplication::translate("newprg", "Starts the Maintenance Tool. To update or uninstall.", nullptr));
#endif // QT_CONFIG(statustip)
        actionDeleteAllSettings->setText(QCoreApplication::translate("newprg", "Delete all settings and Exit", nullptr));
#if QT_CONFIG(statustip)
        actionDeleteAllSettings->setStatusTip(QCoreApplication::translate("newprg", "All saved searches, download list and settings are deleted.", nullptr));
#endif // QT_CONFIG(statustip)
#if QT_CONFIG(tooltip)
        pbPast->setToolTip(QString());
#endif // QT_CONFIG(tooltip)
#if QT_CONFIG(statustip)
        pbPast->setStatusTip(QCoreApplication::translate("newprg", "Paste the link to the page where the video is displayed", nullptr));
#endif // QT_CONFIG(statustip)
        pbPast->setText(QCoreApplication::translate("newprg", "Paste", nullptr));
#if QT_CONFIG(tooltip)
        pbSok->setToolTip(QString());
#endif // QT_CONFIG(tooltip)
#if QT_CONFIG(statustip)
        pbSok->setStatusTip(QCoreApplication::translate("newprg", "Search for video streams.", nullptr));
#endif // QT_CONFIG(statustip)
        pbSok->setText(QCoreApplication::translate("newprg", "Search", nullptr));
#if QT_CONFIG(tooltip)
        pbAdd->setToolTip(QString());
#endif // QT_CONFIG(tooltip)
#if QT_CONFIG(statustip)
        pbAdd->setStatusTip(QCoreApplication::translate("newprg", "Add current video to the list of files that will be downloaded.", nullptr));
#endif // QT_CONFIG(statustip)
        pbAdd->setText(QCoreApplication::translate("newprg", "Add to Download list", nullptr));
#if QT_CONFIG(statustip)
        lblQuality->setStatusTip(QCoreApplication::translate("newprg", "The number of bits that are conveyed or processed per unit of time. Higher numbers give better quality and larger file..", nullptr));
#endif // QT_CONFIG(statustip)
        lblQuality->setText(QCoreApplication::translate("newprg", "Quality (Bitrate)", nullptr));
        leQuality->setText(QString());
#if QT_CONFIG(statustip)
        lblMethod->setStatusTip(QCoreApplication::translate("newprg", "Media streaming communications protocol.", nullptr));
#endif // QT_CONFIG(statustip)
        lblMethod->setText(QCoreApplication::translate("newprg", "Method", nullptr));
        leMethod->setText(QString());
#if QT_CONFIG(statustip)
        chbSubtitle->setStatusTip(QCoreApplication::translate("newprg", "Searches for the subtitle and downloads it at the same time as the video stream.", nullptr));
#endif // QT_CONFIG(statustip)
        chbSubtitle->setText(QCoreApplication::translate("newprg", "Include Subtitle", nullptr));
#if QT_CONFIG(statustip)
        pbDownloadAll->setStatusTip(QCoreApplication::translate("newprg", "Download all files you added to the list.", nullptr));
#endif // QT_CONFIG(statustip)
        pbDownloadAll->setText(QCoreApplication::translate("newprg", "Download all", nullptr));
#if QT_CONFIG(tooltip)
        pbDownload->setToolTip(QString());
#endif // QT_CONFIG(tooltip)
#if QT_CONFIG(statustip)
        pbDownload->setStatusTip(QCoreApplication::translate("newprg", "Download the file you just searched for", nullptr));
#endif // QT_CONFIG(statustip)
        pbDownload->setText(QCoreApplication::translate("newprg", "Download", nullptr));
#if QT_CONFIG(statustip)
        lblQualityBitrate->setStatusTip(QCoreApplication::translate("newprg", "Select quality on the video you download", nullptr));
#endif // QT_CONFIG(statustip)
        lblQualityBitrate->setText(QCoreApplication::translate("newprg", "Quality (bitrate) and method. Higher bitrate gives better quality and larger file.", nullptr));
#if QT_CONFIG(tooltip)
        comboBox->setToolTip(QCoreApplication::translate("newprg", "Select quality on the video you download.", nullptr));
#endif // QT_CONFIG(tooltip)
        comboBox->setCurrentText(QString());
#if QT_CONFIG(tooltip)
        comboPayTV->setToolTip(QCoreApplication::translate("newprg", "Select provider. If yoy need a password.", nullptr));
#endif // QT_CONFIG(tooltip)
#if QT_CONFIG(statustip)
        pbPassword->setStatusTip(QCoreApplication::translate("newprg", "If no saved password is found, click here.", nullptr));
#endif // QT_CONFIG(statustip)
        pbPassword->setText(QCoreApplication::translate("newprg", "Password", nullptr));
        menuFile->setTitle(QCoreApplication::translate("newprg", "&File", nullptr));
        menuLanguage->setTitle(QCoreApplication::translate("newprg", "&Language", nullptr));
        menuTools->setTitle(QCoreApplication::translate("newprg", "&Tools", nullptr));
        menuHelp->setTitle(QCoreApplication::translate("newprg", "&Help", nullptr));
        menuRecent->setTitle(QCoreApplication::translate("newprg", "&Recent", nullptr));
        menuView->setTitle(QCoreApplication::translate("newprg", "&Download list", nullptr));
        menuPayTV->setTitle(QCoreApplication::translate("newprg", "&Pay TV", nullptr));
        menuDownload_all_Episodes->setTitle(QCoreApplication::translate("newprg", "&Several episodes", nullptr));
    } // retranslateUi

};

namespace Ui {
    class newprg: public Ui_newprg {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_NEWPRG_H
