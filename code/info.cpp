/*
    streamCapture2
    Copyright (C) 2016 - 2020 Ingemar Ceicer
    http://ceicer.org/ingemar/
    programmering1 (at) ceicer (dot) org

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
*/
#include "checkforupdates.h"
#include "newprg.h"
#include "info.h"
void Info::getSystem()
{
    // const QString VERSION_SVTPLAY = Newprg::getSvtplaydlVersion();
    QIcon icon(":/images/icon.ico");
    this->setWindowIcon(icon);
    const QString ffmpeg_homepage = FFMPEG_HOMEPAGE
                                    const QString svtplay_dl_homepage = SVTPLAYDL_HOMEPAGE
                                            const QString license = DISPLAY_NAME + tr(" is free software, license ") + "<br>GPL version " + LICENCE_VERSION + ".";
    //const QString syfte = tr("A graphical shell for ") + "<a style=\"text-decoration:none;\" href='" + svtplay_dl_homepage + "'>svtplay-dl</a>" + tr(" version ") + VERSION_SVTPLAY + tr(" and ") + "<a style=\"text-decoration:none;\" href='" + ffmpeg_homepage + "'>FFmpeg</a>.<br>" + tr(" streamCapture2 handles downloads of video streams.<br>");
    const QString syfte = tr("A graphical shell for ") + "<a style=\"text-decoration:none;\" href='" + svtplay_dl_homepage + "'>svtplay-dl</a>"  + tr(" and ") + "<a style=\"text-decoration:none;\" href='" + ffmpeg_homepage + "'>FFmpeg</a>.<br>" + tr(" streamCapture2 handles downloads of video streams.<br>");
    const QString translator = tr("Many thanks to ") + " Benjamin Weis" + tr(" for the German translation.");
    /*  */
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME, EXECUTABLE_NAME);
    settings.setIniCodec("UTF-8");
    settings.beginGroup("Language");
    QString sp = settings.value("language", "").toString();
    settings.endGroup();
    QString applicationHomepage;

    if(sp == "sv_SE") {
        applicationHomepage = APPLICATION_HOMEPAGE;
    } else {
        applicationHomepage = APPLICATION_HOMEPAGE_ENG;
    }

    /* */
    // STOP EDIT
    QString v = "";
    // v += tr("This program uses Qt version ") + QT_VERSION_STR + tr(" running on ");
    v += QSysInfo::kernelType() + ' ' + QSysInfo::prettyProductName() + " (" + QSysInfo::currentCpuArchitecture() + ").\n";
    v += DISPLAY_NAME " " VERSION + tr(" was created ") + BUILD_DATE_TIME "\n" + tr("by a computer with") + " " + QSysInfo::buildAbi() + ".";
    // v += "\n";
    // write GCC version
    QString temp;
#if defined _MSC_VER ||  __GNUC__ || defined __clang__ || defined __ICC || defined __clang__
#if defined   __GNUC__ || defined __clang__ || defined __ICC || defined __clang__
#if defined   __GNUC__ && !defined __clang__
#if defined __GNUC__ && !defined __MINGW32__
#define COMPILER "GCC"
#endif
#ifdef __MINGW32__
#define COMPILER "MinGW GCC"
#endif
    temp = (QString(tr(" Compiled by") + " %1 %2.%3.%4%5").arg(COMPILER).arg(__GNUC__).arg(__GNUC_MINOR__).arg(__GNUC_PATCHLEVEL__).arg("."));
#endif
#ifdef __ICC
#define INTEL "Intel icl"
    temp = (QString(tr(" Compiled by") + " %1 %2 Build date: %3 Update: %4%5").arg(INTEL).arg(__INTEL_COMPILER).arg(__INTEL_COMPILER_BUILD_DATE).arg(__INTEL_COMPILER_UPDATE).arg("."));
#endif
    v += temp;
#if defined __clang__
    v += (QString(tr(" Compiled by") +  " %1 %2.%3.%4%5").arg("Clang").arg(__clang_major__).arg(__clang_minor__).arg(__clang_patchlevel__).arg("."));
#endif
#endif
// MSVC version
#ifdef _MSC_VER
#define COMPILER "MSVC++"
    QString tmp = QString::number(_MSC_VER);
    QString version;
    QString full_version = tr("Full version number ") + QString::number(_MSC_FULL_VER);

    switch(_MSC_VER) {
        case 1500:
            version = "9.0 (Visual Studio 2008)\n" + full_version;
            break;

        case 1600:
            version = "10.0 (Visual Studio 2010)\n" + full_version;
            break;

        case 1700:
            version = "11.0 (Visual Studio 2012)<br>" + full_version;
            break;

        case 1800:
            version = "12.0 (Visual Studio 2013)\n" + full_version;
            break;

        case 1900:
            version = "14.0 (Visual Studio 2015)\n" + full_version;
            break;

        case 1910:
            version = "15.0 (Visual Studio 2017)\n" + full_version;
            break;

        case 1912:
            version = "15.5 (Visual Studio 2017)\n" + full_version;
            break;

        case 1914:
            version = "15.7 (Visual Studio 2017)\n" + full_version;
            break;

        case 1915:
            version = "15.8 (Visual Studio 2017)\n" + full_version;
            break;

        case 1916:
            version = "15.9 (Visual Studio 2017)\n" + full_version;
            break;

        case 1921:
            version = "16.1 (Visual Studio 2019)\n" + full_version;
            break;

        case 1922:
            version = "16.2 (Visual Studio 2019)\n" + full_version;
            break;

        case 1923:
            version = "16.3 (Visual Studio 2019)\n" + full_version;
            break;

        case 1924:
            version = "16.4 (Visual Studio 2019)\n" + full_version;
            break;

        case 1925:
            version = "16.5 (Visual Studio 2019)\n" + full_version;
            break;

        default:
            version = tr("Unknown version") + "\n" + full_version;
            break;
    }

    v += (QString(tr(" Compiled by") +  " %1 %2%3").arg(COMPILER).arg(version).arg("."));
#endif
    v += "\n";
#else
    v += tr("Unknown compiler.");
    v += "\n";
#endif
    v += "<br>";
    QString CURRENT_YEAR_S = CURRENT_YEAR;
    CURRENT_YEAR_S = CURRENT_YEAR_S.right(4);
    const QString auther = "<h3 style=\"color:green;\">Copyright &copy; " START_YEAR " - " + CURRENT_YEAR_S + " " PROGRAMMER_NAME "</h3><br><a style=\"text-decoration:none;\" href='" + applicationHomepage + "'>" + tr("Home page") + "</a> | <a style=\"text-decoration:none;\" href='" SOURCEKODE "'>" + tr("Source code") + "</a> | <a style=\"text-decoration:none;\" href='" WIKI "'>" + tr("Wiki") + "</a><br><a style=\"text-decoration:none;\"  href=\"mailto:" PROGRAMMER_EMAIL "\">" PROGRAMMER_EMAIL "</a>  <br>" + tr("Phone: ") + PROGRAMMER_PHONE "<br><br>";
    QMessageBox::about(nullptr, tr("About ") + DISPLAY_NAME + " " + VERSION,
                       "<h1 style=\"color:green;\">" DISPLAY_NAME " " VERSION "</h1>" + auther + translator + "<br>" + syfte + "<br>" + tr("This program uses Qt version ") + QT_VERSION_STR + tr(" running on ") + v + "<br>" + license);
}




