
#-------------------------------------------------
#    streamCapture2
#    Copyright (C) 2016-2019 Ingemar Ceicer
#    http://ceicer.org/ingemar/
#    programmering1 (at) ceicer (dot) org
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#-------------------------------------------------
#
# Project created by QtCreator 2017-05-07T13:14:14
#
#-------------------------------------------------

QT       += core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = streamcapture2-0.18.6
TEMPLATE = app


# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS
#CONFIG  += cmdline precompile_header

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

CONFIG += C++14
## 32-bit Lubuntu 16.04
#  CONFIG += /usr/lib/i386-linux-gnu/qt5/bin/lrelease embed_translations
## 64-bit Lubuntu 16.04
#  CONFIG += /opt/Qt5.14.2_shared/bin/lrelease embed_translations

## Upuntu 18.04
#  CONFIG += /opt/Qt5.14.2/5.14.2/gcc_64/bin/lrelease embed_translations
## Windows
   CONFIG += C:\Qt\Qt\5.14.2\msvc2017_64\bin\lrelease embed_translations
#  CONFIG += C:\Qt\Qt5.14.2_static\5.14.1\bin\lrelease embed_translations
## Windows 32
#  CONFIG +=C:\Qt\Qt5.14.2_static\5.14.2\bin\lrelease embed_translations
## 64-bit Lubuntu 14.04
#  CONFIG += /opt/Qt5.8.0/5.8/gcc_64/bin\lrelease embed_translations
## Ubuntu 18.04
#  CONFIG += /opt/Qt5.13.2/5.13.2/gcc_64/bin/lrelease embed_translations
#  CONFIG += lrelease embed_translations

# Input
#PRECOMPILED_HEADER  = checkforupdates.h \
#                      newprg.h \
#                      info.h \
#		       selectfont.h

HEADERS += checkforupdates.h newprg.h \
           info.h \
           selectfont.h
FORMS +=   newprg.ui \
           selectfont.ui
         
SOURCES += checkforupdates.cpp \
           coppytodefaultlocation.cpp \
           download.cpp \
           downloadall.cpp \
           downloadallepisodes.cpp \
           listallepisodes.cpp \
           newprg.cpp main.cpp \
           about.cpp \
           info.cpp \
           paytv_create.cpp \
           paytv_edit.cpp \
           save.cpp \
           setgetconfig.cpp \
           language.cpp \
           sok.cpp \
           testsearch.cpp \
           selectfont.cpp

RESOURCES += myres.qrc \
            fonts_forall.qrc
			
linux-g++ | linux-g++-64 | linux-g++-32 {
RESOURCES += fonts.qrc
}


RC_FILE = myapp.rc

TRANSLATIONS += i18n/streamcapture2_xx_XX.ts \
                i18n/streamcapture2_sv_SE.ts \
                i18n/streamcapture2_de_DE.ts

DESTDIR=../build-executable
unix:UI_DIR = ../code
win32:UI_DIR = ../code
win32:RC_ICONS += images/icon.ico

