/*
    streamCapture2
    Copyright (C) 2016 - 2020 Ingemar Ceicer
    http://ceicer.org/ingemar/
    programmering1 (at) ceicer (dot) org

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
*/

#ifndef SELECTFONT_H
#define SELECTFONT_H

#include <QDialog>
#include <QDebug>
#include <QSettings>
#include "info.h"
#define THIS_WIDHT 671
#define THIS_HEIGHT 140


QT_BEGIN_NAMESPACE
namespace Ui
{
class SelectFont;
}
QT_END_NAMESPACE

class SelectFont : public QDialog
{
    Q_OBJECT

private:
    Ui::SelectFont *ui;
    void triggerSignal();


public:
    SelectFont(QWidget *parent = nullptr);
    ~SelectFont();



signals:
    void valueChanged(QFont font);


};
#endif // SELECTFONT_H
