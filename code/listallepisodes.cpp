/*
    streamCapture2
    Copyright (C) 2016 - 2020 Ingemar Ceicer
    http://ceicer.org/ingemar/
    programmering1 (at) ceicer (dot) org

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
*/
#include "newprg.h"
#include "ui_newprg.h"
#include "info.h"
void Newprg::listAllEpisodes()
{
    ui->teOut->setTextColor(QColor("black"));
    ui->pbAdd->setDisabled(true);
    ui->actionAdd->setDisabled(true);
    ui->actionAddAllEpisodesToDownloadList->setEnabled(true);
    ui->pbDownloadAll->setDisabled(true);
    ui->actionSaveDownloadList->setEnabled(false);
    ui->comboBox->clear();
    ui->leQuality->setText(tr("Searching..."));
    ui->leMethod->setText(tr("Searching..."));
    ui->teOut->setText(tr("The request is processed...\nStarting search..."));
    ui->pbSok->setEnabled(false);
    ui->actionSearch->setEnabled(false);
    ui->actionListAllEpisodes->setEnabled(false);
    QTimer::singleShot(2000, this, [this] {
        ui->pbSok->setEnabled(true);
        ui->actionSearch->setEnabled(true);
        ui->actionListAllEpisodes->setEnabled(true);
    });
    ui->teOut->setReadOnly(true);
    ui->actionSaveDownloadList->setEnabled(false);
    QString sok = ui->leSok->text();
    sok = sok.simplified();

    if(sok == "") {
        ui->teOut->setText(tr("The search field is empty!"));
        ui->leQuality->setText("");
        ui->leMethod->setText("");
    } else if((sok.left(7) != "http://") && (sok.left(8) != "https://")) {
        ui->teOut->setText(tr("Incorrect URL"));
        ui->leQuality->setText("");
        ui->leMethod->setText("");
    } else {
        address = ui->leSok->text().trimmed();
        // ipac v0.14.9 Removed question marks from search address.
        int questionmark = address.indexOf('?');
        address = address.left(questionmark);
        // Hit
        QString EXECUTE;

        if(ui->comboPayTV->currentIndex() == 0) { // No Password
            QString test = doTestSearch(address);

            if(test == "skit") {
                ui->teOut->setText("ERROR");
                return;
            }

            if(test != "") {
                ui->teOut->setText(test);
            }

            // Windows
#ifdef Q_OS_WIN // Windows 32- and 64-bit
            EXECUTE = QDir::toNativeSeparators("\"" + QCoreApplication::applicationDirPath() + "/svtplay-dl.exe\" -A --get-only-episode-url ")  + address;
#endif
#ifdef Q_OS_LINUX // Linux
            EXECUTE = QDir::toNativeSeparators("\"" + QCoreApplication::applicationDirPath() + "/svtplay-dl\" -A --get-only-episode-url ")  + address;
#endif
        } else {  // Password
            QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME, EXECUTABLE_NAME);
            settings.setIniCodec("UTF-8");
            QString provider = ui->comboPayTV->currentText();
            settings.beginGroup("Provider");
            QString username = settings.value(provider + "/username").toString();
            QString password = settings.value(provider + "/password").toString();

            if(password == "") {
                password = secretpassword;

                if(password == "") {
                    bool ok;
                    QString newpassword = QInputDialog::getText(nullptr, provider + " " + tr("Enter your password"),
                                          tr("Spaces are not allowed. Use only the characters your streaming provider approves.\nThe Password will not be saved."), QLineEdit::Password,
                                          "", &ok);

                    if(newpassword.indexOf(' ') >= 0)
                        return;

                    if(ok) {
                        secretpassword = newpassword;
                        password = newpassword;
                    }
                }
            }

            QString test = doTestSearch(address, username, password);

            if(test != "") {
                ui->teOut->setText(test);
                return;
            }

            // Windows
#ifdef Q_OS_WIN // Windows 32- and 64-bit
            EXECUTE = QDir::toNativeSeparators("\"" + QCoreApplication::applicationDirPath() + "/svtplay-dl.exe\" ") +  " -A --get-only-episode-url --username=" + username + " " + "--password=" + password + " " + address;
#endif
#ifdef Q_OS_LINUX // Linux
            EXECUTE = QDir::toNativeSeparators("\"" + QCoreApplication::applicationDirPath() + "/svtplay-dl\" ") + " -A --get-only-episode-url --username=" + username + " " + "--password=" + password + " " + address;
#endif
        }

        CommProcess2 = new QProcess(nullptr);
        CommProcess2->setProcessChannelMode(QProcess::MergedChannels);
        //  CommProcess2->start(EXECUTE + " --list-quality");
        CommProcess2->start(EXECUTE);
        processpid = CommProcess2->processId();
        connect(CommProcess2, SIGNAL(finished(int, QProcess::ExitStatus)), this, SLOT(onCommProcessExit_sok(int, QProcess::ExitStatus)));
        connect(CommProcess2, &QProcess::readyReadStandardOutput, [this]() {
            QString result(CommProcess2->readAllStandardOutput());
#ifdef Q_OS_LINUX // Linux
            const QString os = "svtplay-dl";
#endif
#ifdef Q_OS_WIN // Windows 32- and 64-bit
            const QString os = "svtplay-dl.exe";
#endif

            if(result.mid(0, 25) == "ERROR: svtplay-dl crashed") {
                ui->teOut->setTextColor(QColor("red"));
                ui->teOut->append(os + tr(" crashed."));
                ui->teOut->append(tr("Can not find any video streams, please check the address."));
                ui->teOut->setTextColor(QColor("black"));
                return;
            }

            if(result.mid(0, 34) == "Traceback (most recent call last):") {
                return;
            }

            if(result.mid(0, 6) != "ERROR:") {
                result = result.simplified();
                result.replace("Url: ", "");
                QStringList list = result.split("INFO:", QString::SkipEmptyParts);
                list.removeDuplicates();

                foreach(QString temp, list) {
                    ui->teOut->append(temp.trimmed());
                }
            } else {
                ui->teOut->append(result);
            }
        });
    }
}
