<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de_DE">
<context>
    <name>CheckForUpdates</name>
    <message>
        <location filename="../checkforupdates.cpp" line="40"/>
        <location filename="../checkforupdates.cpp" line="84"/>
        <source>No Internet connection was found.
Please check your Internet settings and firewall.</source>
        <translation>Es wurde keine Internetverbindung gefunden.
Bitte überprüfen Sie Ihre Interneteinstellungen und Firewall.</translation>
    </message>
    <message>
        <location filename="../checkforupdates.cpp" line="46"/>
        <location filename="../checkforupdates.cpp" line="89"/>
        <source>
There is a newer version of </source>
        <translation>
Es gibt eine neuere Version von </translation>
    </message>
    <message>
        <source>Please download from</source>
        <translation type="vanished">Bitte herunterladen von</translation>
    </message>
    <message>
        <location filename="../checkforupdates.cpp" line="46"/>
        <location filename="../checkforupdates.cpp" line="89"/>
        <source>Latest version: </source>
        <translation>Neueste Version: </translation>
    </message>
    <message>
        <location filename="../checkforupdates.cpp" line="46"/>
        <location filename="../checkforupdates.cpp" line="89"/>
        <source>Please </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../checkforupdates.cpp" line="46"/>
        <location filename="../checkforupdates.cpp" line="89"/>
        <source>Download</source>
        <translation type="unfinished">Herunterladen</translation>
    </message>
    <message>
        <location filename="../checkforupdates.cpp" line="50"/>
        <source>
Your version of </source>
        <translation>
Ihre Version von </translation>
    </message>
    <message>
        <location filename="../checkforupdates.cpp" line="50"/>
        <source> is newer than the latest official version. </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source> is newer than the latest official version </source>
        <translation type="vanished"> ist neuer als die neueste offizielle Version </translation>
    </message>
    <message>
        <location filename="../checkforupdates.cpp" line="54"/>
        <source>
You have the latest version of </source>
        <translation>
Sie haben die neueste Version von </translation>
    </message>
    <message>
        <location filename="../checkforupdates.cpp" line="58"/>
        <location filename="../checkforupdates.cpp" line="91"/>
        <source>
There was an error when the version was checked.</source>
        <translation>
Es gab einen Fehler bei der Überprüfung der Version.</translation>
    </message>
    <message>
        <source>No Internet connection was found.
Impossible to check for a new version.
Please check your Internet settings and firewall.</source>
        <oldsource>No Internet connection was found.
Impossible to check for new version.
Please check your Internet settings and firewall.</oldsource>
        <translation type="vanished">Es wurde keine Internetverbindung gefunden.
Es ist unmöglich, nach einer neuen Version zu suchen.
Bitte überprüfen Sie Ihre Interneteinstellungen und Firewall.</translation>
    </message>
    <message>
        <location filename="../checkforupdates.cpp" line="129"/>
        <source>New updates:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Info</name>
    <message>
        <location filename="../info.cpp" line="29"/>
        <source>A graphical shell for </source>
        <oldsource>A graphical shell </oldsource>
        <translation>Eine grafische Shell für </translation>
    </message>
    <message>
        <location filename="../info.cpp" line="29"/>
        <source> and </source>
        <translation> und </translation>
    </message>
    <message>
        <location filename="../info.cpp" line="30"/>
        <source>Many thanks to </source>
        <translatorcomment>Många tack till</translatorcomment>
        <translation>Vielen Dank an</translation>
    </message>
    <message>
        <location filename="../info.cpp" line="30"/>
        <source> for the German translation.</source>
        <translation> für die deutsche Übersetzung.</translation>
    </message>
    <message>
        <source>CPU architecture:</source>
        <translation type="vanished">CPU-Architektur</translation>
    </message>
    <message>
        <location filename="../info.cpp" line="27"/>
        <source> is free software, license </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../info.cpp" line="29"/>
        <source> streamCapture2 handles downloads of video streams.&lt;br&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../info.cpp" line="50"/>
        <source> was created </source>
        <translation> wurde erstellt </translation>
    </message>
    <message>
        <source>Compiled by</source>
        <translation type="vanished">Kompiliert von</translation>
    </message>
    <message>
        <location filename="../info.cpp" line="50"/>
        <source>by a computer with</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../info.cpp" line="63"/>
        <location filename="../info.cpp" line="67"/>
        <location filename="../info.cpp" line="71"/>
        <location filename="../info.cpp" line="147"/>
        <source> Compiled by</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../info.cpp" line="79"/>
        <source>Full version number </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../info.cpp" line="143"/>
        <source>Unknown version</source>
        <translation>Unbekannte Version</translation>
    </message>
    <message>
        <location filename="../info.cpp" line="151"/>
        <source>Unknown compiler.</source>
        <translation>Unbekannter Kompiler.</translation>
    </message>
    <message>
        <location filename="../info.cpp" line="157"/>
        <source>Phone: </source>
        <translation>Telefon: </translation>
    </message>
    <message>
        <location filename="../info.cpp" line="157"/>
        <source>Home page</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../info.cpp" line="157"/>
        <source>Source code</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../info.cpp" line="157"/>
        <source>Wiki</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../info.cpp" line="158"/>
        <source>About </source>
        <translation>Über </translation>
    </message>
    <message>
        <location filename="../info.cpp" line="159"/>
        <source>This program uses Qt version </source>
        <translation>Dieses Programm verwendet Qt-Version </translation>
    </message>
    <message>
        <location filename="../info.cpp" line="159"/>
        <source> running on </source>
        <translation> läuft auf </translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../newprg.cpp" line="348"/>
        <location filename="../newprg.cpp" line="365"/>
        <source>Enter your password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="349"/>
        <location filename="../newprg.cpp" line="366"/>
        <source>Spaces are not allowed. Use only the characters your streaming provider approves.
The Password will not be saved.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="421"/>
        <source>svtplay-dl crashed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="427"/>
        <source>Could not stop svtplay-dl.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="433"/>
        <source>Delete any files that may have already been downloaded.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="433"/>
        <source>svtplay-dl stopped. Exit code </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="451"/>
        <source>Copy to: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="464"/>
        <source>Copy downloaded streaming media to directory</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="483"/>
        <source>Download streaming media to directory</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="503"/>
        <source>Download to: </source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Newprg</name>
    <message>
        <location filename="../about.cpp" line="60"/>
        <location filename="../about.cpp" line="80"/>
        <location filename="../about.cpp" line="100"/>
        <source>The license file is not found</source>
        <translation>Die Lizenzdatei wurde nicht gefunden</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="204"/>
        <source> installation.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../about.cpp" line="177"/>
        <source>version </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../language.cpp" line="15"/>
        <location filename="../language.cpp" line="40"/>
        <location filename="../language.cpp" line="65"/>
        <source>The program must be restarted for the new language settings to take effect.</source>
        <translation>Das Programm muss neu gestartet werden, damit die neuen Spracheinstellungen wirksam werden.</translation>
    </message>
    <message>
        <location filename="../language.cpp" line="16"/>
        <location filename="../language.cpp" line="41"/>
        <location filename="../language.cpp" line="66"/>
        <source>Restart Now</source>
        <translation>Jetzt neustarten</translation>
    </message>
    <message>
        <location filename="../language.cpp" line="17"/>
        <location filename="../language.cpp" line="42"/>
        <location filename="../language.cpp" line="67"/>
        <location filename="../setgetconfig.cpp" line="257"/>
        <location filename="../setgetconfig.cpp" line="287"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
    <message>
        <location filename="../setgetconfig.cpp" line="351"/>
        <source>Could not save a file to store the list off downloads. Check your file permissions.</source>
        <translation>Eine Datei zum Speichern der Liste konnte nicht gespeichert werden. Überprüfen Sie Ihre Dateiberechtigungen.</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="37"/>
        <location filename="../downloadallepisodes.cpp" line="59"/>
        <source>The request is processed...
Preparing to download...</source>
        <translation>Die Anfrage wird bearbeitet...
Herunterladen wird vorbereitet...</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="152"/>
        <location filename="../downloadall.cpp" line="148"/>
        <location filename="../downloadallepisodes.cpp" line="138"/>
        <location filename="../listallepisodes.cpp" line="93"/>
        <location filename="../paytv_create.cpp" line="101"/>
        <location filename="../paytv_edit.cpp" line="121"/>
        <location filename="../sok.cpp" line="88"/>
        <source>Enter your password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../download.cpp" line="153"/>
        <location filename="../downloadall.cpp" line="149"/>
        <location filename="../downloadallepisodes.cpp" line="139"/>
        <location filename="../listallepisodes.cpp" line="94"/>
        <location filename="../sok.cpp" line="89"/>
        <source>Spaces are not allowed. Use only the characters your streaming provider approves.
The Password will not be saved.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../downloadallepisodes.cpp" line="23"/>
        <source>You have chosen to copy to &quot;Default folder&quot;.
Unfortunately, this does not work when you select &quot;Download all episodes&quot;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../downloadallepisodes.cpp" line="27"/>
        <source>Because you cannot select &quot;Method&quot; and &quot;Quality&quot; when you select &quot;Download all episodes&quot;, no folders will be created.

Do you want to start the download?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../downloadallepisodes.cpp" line="35"/>
        <source>Once svtplay-dl has started downloading all episodes, streamCapture2 no longer has control. If you want to cancel, you may need to log out or restart your computer.

Do you want to start the download?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../downloadallepisodes.cpp" line="160"/>
        <source>Starts downloading all episodes: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../downloadallepisodes.cpp" line="174"/>
        <source>The media files (and if you have selected the subtitles) have been downloaded.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../downloadall.cpp" line="100"/>
        <location filename="../downloadall.cpp" line="199"/>
        <source>The video streams are saved in </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../download.cpp" line="80"/>
        <location filename="../download.cpp" line="83"/>
        <source>The video stream is saved in </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../download.cpp" line="90"/>
        <location filename="../downloadall.cpp" line="39"/>
        <location filename="../downloadallepisodes.cpp" line="69"/>
        <source>The default folder for downloading video streams cannot be found.
Download is interrupted.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../download.cpp" line="96"/>
        <location filename="../downloadall.cpp" line="45"/>
        <location filename="../downloadall.cpp" line="66"/>
        <location filename="../downloadallepisodes.cpp" line="75"/>
        <source>You do not have the right to save to the folder.
Download is interrupted.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../downloadall.cpp" line="62"/>
        <source>The default folder for copying video streams cannot be found.
Download is interrupted.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../download.cpp" line="217"/>
        <location filename="../downloadall.cpp" line="283"/>
        <source>Download completed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../downloadallepisodes.cpp" line="179"/>
        <source>Download failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../downloadall.cpp" line="70"/>
        <source>The video files are copied to </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../downloadall.cpp" line="240"/>
        <source>Preparing to download</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../downloadall.cpp" line="250"/>
        <location filename="../downloadall.cpp" line="255"/>
        <source> succeeded (</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../downloadall.cpp" line="250"/>
        <location filename="../downloadall.cpp" line="255"/>
        <location filename="../downloadall.cpp" line="270"/>
        <source>Download </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../downloadall.cpp" line="270"/>
        <source> failed (</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../download.cpp" line="202"/>
        <location filename="../download.cpp" line="208"/>
        <source>Download succeeded </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../download.cpp" line="225"/>
        <source>Download failed </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../download.cpp" line="251"/>
        <location filename="../download.cpp" line="252"/>
        <location filename="../listallepisodes.cpp" line="29"/>
        <location filename="../listallepisodes.cpp" line="30"/>
        <source>Searching...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../download.cpp" line="253"/>
        <location filename="../listallepisodes.cpp" line="31"/>
        <source>The request is processed...
Starting search...</source>
        <translation>Die Anfrage wird bearbeitet...
Suche wird gestartet...</translation>
    </message>
    <message>
        <location filename="../listallepisodes.cpp" line="46"/>
        <location filename="../sok.cpp" line="41"/>
        <source>The search field is empty!</source>
        <translation>Das Suchfeld ist leer!</translation>
    </message>
    <message>
        <location filename="../listallepisodes.cpp" line="50"/>
        <location filename="../sok.cpp" line="45"/>
        <source>Incorrect URL</source>
        <translation>Falsche URL</translation>
    </message>
    <message>
        <location filename="../listallepisodes.cpp" line="140"/>
        <location filename="../sok.cpp" line="135"/>
        <source> crashed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../listallepisodes.cpp" line="141"/>
        <location filename="../sok.cpp" line="136"/>
        <source>Can not find any video streams, please check the address.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../download.cpp" line="101"/>
        <source>The video file is copied to </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="63"/>
        <source>normal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="66"/>
        <source>bold and italic</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="69"/>
        <source>bold</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="71"/>
        <source>italic</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="74"/>
        <source>Current font:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="74"/>
        <source>size:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="531"/>
        <source>MaintenanceTool cannot be found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="648"/>
        <source>Click to copy to the search box.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="675"/>
        <source>Click to delete all saved searches.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="689"/>
        <source>All your saved settings will be deleted. All lists of files to download will disappear.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="697"/>
        <source>Failed to delete your configuration files. Check your file permissions.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="707"/>
        <source> was normally terminated. QProsess Exit code = </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="712"/>
        <source> crashed. QProsess Exit code = </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="601"/>
        <source>Can not find any video streams, please check the address.

</source>
        <translation>Kann keine Videostreams finden, bitte überprüfen Sie die Adresse.

</translation>
    </message>
    <message>
        <location filename="../save.cpp" line="30"/>
        <source>Save streaming media to directory</source>
        <oldsource>Save streaming media files to directory</oldsource>
        <translation type="unfinished">Streaming-Mediendateien im Verzeichnis speichern</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="559"/>
        <source>Downloading...</source>
        <translation>Herunterladen...</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="623"/>
        <source>The search is complete&lt;br&gt;</source>
        <translation>Die Suche ist vollständig&lt;br&gt;</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="625"/>
        <source>The search failed&lt;br&gt;</source>
        <translation>Die Suche ist fehlgeschlagen&lt;br&gt;</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="43"/>
        <source>The version history file is not found</source>
        <translation>Versionsgeschichte kann nicht gefunden werden</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="204"/>
        <source> could not be found. Check your installation. </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../about.cpp" line="204"/>
        <source> is included with the </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../setgetconfig.cpp" line="179"/>
        <source>Up and running</source>
        <translation>In Betrieb</translation>
    </message>
    <message>
        <location filename="../setgetconfig.cpp" line="248"/>
        <location filename="../setgetconfig.cpp" line="278"/>
        <source>Uninstall </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../setgetconfig.cpp" line="248"/>
        <source> and Delete all Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../setgetconfig.cpp" line="250"/>
        <source>Uninstall and completely remove all components. All saved searches and the list of streams to download will be deleted.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../setgetconfig.cpp" line="255"/>
        <location filename="../setgetconfig.cpp" line="285"/>
        <source>Do you want to uninstall </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../setgetconfig.cpp" line="256"/>
        <location filename="../setgetconfig.cpp" line="286"/>
        <source>Uninstall Now</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../setgetconfig.cpp" line="272"/>
        <location filename="../setgetconfig.cpp" line="293"/>
        <source>Uninstaller not found.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../setgetconfig.cpp" line="280"/>
        <source>Uninstall and completely remove all components.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../setgetconfig.cpp" line="335"/>
        <source>Could not save a file to store Recent Search list. Check your file permissions.</source>
        <translation>Eine gespeicherte Datei konnte nicht gespeichert werden. Überprüfen Sie Ihre Dateiberechtigungen.</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="175"/>
        <source>Starts downloading: </source>
        <translation>Startet das Herunterladen: </translation>
    </message>
    <message>
        <location filename="../download.cpp" line="236"/>
        <location filename="../downloadall.cpp" line="305"/>
        <location filename="../downloadallepisodes.cpp" line="189"/>
        <source>No folder is selected</source>
        <translation>Es ist kein Ordner ausgewählt</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="662"/>
        <source>The number of previous searches to be saved...</source>
        <oldsource>The number of recently opened files to be displayed...</oldsource>
        <translation>Die Anzahl der vorherigen Suchvorgänge, die gespeichert werden...</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="664"/>
        <source>Specify how many previous searches you want to save. If the number of searches exceeds the specified number, the oldest search is deleted.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="673"/>
        <source>Remove all saved searches</source>
        <translation>Alle gespeicherten Suchvorgänge entfernen</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="668"/>
        <source>The number of searches to be saved: </source>
        <oldsource>Set number of Recent Files: </oldsource>
        <translation>Die Anzahl der Suchvorgänge, die gespeichert werden: </translation>
    </message>
    <message>
        <location filename="../paytv_create.cpp" line="57"/>
        <source>Enter your service provider</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../paytv_create.cpp" line="58"/>
        <location filename="../paytv_create.cpp" line="80"/>
        <location filename="../paytv_create.cpp" line="102"/>
        <location filename="../paytv_edit.cpp" line="100"/>
        <location filename="../paytv_edit.cpp" line="122"/>
        <source>Spaces are not allowed. Use only the characters your streaming provider approves.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../paytv_create.cpp" line="79"/>
        <location filename="../paytv_edit.cpp" line="99"/>
        <source>Enter your username</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../paytv_create.cpp" line="110"/>
        <location filename="../paytv_edit.cpp" line="130"/>
        <source>Save password?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../paytv_create.cpp" line="111"/>
        <location filename="../paytv_edit.cpp" line="131"/>
        <source>Do you want to save the password? (unsafe)?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../paytv_edit.cpp" line="24"/>
        <source>Edit or delete
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../paytv_edit.cpp" line="25"/>
        <source>Edit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../paytv_edit.cpp" line="26"/>
        <source>Delete</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../paytv_edit.cpp" line="51"/>
        <source>Create New</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../paytv_edit.cpp" line="59"/>
        <location filename="../setgetconfig.cpp" line="227"/>
        <source>No Password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../coppytodefaultlocation.cpp" line="54"/>
        <location filename="../coppytodefaultlocation.cpp" line="136"/>
        <source>You have not selected any place to copy the media files.
Please select a location before proceeding.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../coppytodefaultlocation.cpp" line="72"/>
        <location filename="../coppytodefaultlocation.cpp" line="96"/>
        <location filename="../coppytodefaultlocation.cpp" line="145"/>
        <location filename="../coppytodefaultlocation.cpp" line="165"/>
        <source>Copy succeeded </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../coppytodefaultlocation.cpp" line="76"/>
        <location filename="../coppytodefaultlocation.cpp" line="100"/>
        <location filename="../coppytodefaultlocation.cpp" line="149"/>
        <location filename="../coppytodefaultlocation.cpp" line="169"/>
        <source>Copy failed </source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SelectFont</name>
    <message>
        <location filename="../selectfont.ui" line="26"/>
        <source>Select font</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../selectfont.ui" line="85"/>
        <source>Normal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../selectfont.ui" line="95"/>
        <source>Bold</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../selectfont.ui" line="102"/>
        <source>Italic</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../selectfont.ui" line="109"/>
        <source>Bold and italic</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../selectfont.ui" line="138"/>
        <source>Default</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../selectfont.ui" line="145"/>
        <source>Exit</source>
        <translation type="unfinished">Beenden</translation>
    </message>
    <message>
        <location filename="../selectfont.cpp" line="55"/>
        <source>All fonts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../selectfont.cpp" line="55"/>
        <source>Monospaced fonts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../selectfont.cpp" line="55"/>
        <source>Proportional fonts</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>newprg</name>
    <message>
        <location filename="../newprg.ui" line="32"/>
        <source>TEST</source>
        <translation>TEST</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="114"/>
        <location filename="../newprg.ui" line="759"/>
        <source>Search</source>
        <translation>Suche</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="62"/>
        <source>Paste the link to the page where the video is displayed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="233"/>
        <source>Quality (Bitrate)</source>
        <oldsource>Bitrate</oldsource>
        <translation>Qualität (Bitrate)</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="287"/>
        <source>Media streaming communications protocol.</source>
        <translation>Medien-Streaming-Kommunikationsprotokoll.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="302"/>
        <source>Method</source>
        <translation>Verfahren</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="437"/>
        <source>Download the file you just searched for</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="440"/>
        <location filename="../newprg.ui" line="786"/>
        <source>Download</source>
        <translation>Herunterladen</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="399"/>
        <location filename="../newprg.ui" line="951"/>
        <source>Download all</source>
        <translation>Alle herunterladen</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="453"/>
        <source>Select quality on the video you download</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="65"/>
        <location filename="../newprg.ui" line="771"/>
        <source>Paste</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="146"/>
        <location filename="../newprg.ui" line="936"/>
        <source>Add to Download list</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="462"/>
        <source>Quality (bitrate) and method. Higher bitrate gives better quality and larger file.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="361"/>
        <location filename="../newprg.ui" line="913"/>
        <source>Include Subtitle</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="396"/>
        <source>Download all files you added to the list.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="492"/>
        <source>Select quality on the video you download.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="520"/>
        <source>Select provider. If yoy need a password.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="536"/>
        <location filename="../newprg.ui" line="898"/>
        <location filename="../newprg.ui" line="1014"/>
        <source>Password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="578"/>
        <source>&amp;File</source>
        <translation>&amp;Datei</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="590"/>
        <source>&amp;Language</source>
        <translation>&amp;Sprache</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="598"/>
        <source>&amp;Tools</source>
        <translation>&amp;Werkzeuge</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="615"/>
        <source>&amp;Help</source>
        <translation>&amp;Hilfe</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="631"/>
        <source>&amp;Recent</source>
        <oldsource>Recent</oldsource>
        <translation>&amp;Neueste</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="636"/>
        <source>&amp;Download list</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="649"/>
        <source>&amp;Pay TV</source>
        <oldsource>Pay TV</oldsource>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="656"/>
        <source>&amp;Several episodes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="962"/>
        <source>Create folder &quot;method_quality&quot;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1033"/>
        <source>Direct Download of all Video Streams in current serie (Not from the Download List)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1036"/>
        <source>Searches for all Video Streams in the current series and tries to download them directly.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1058"/>
        <source>List all Video Streams</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1061"/>
        <source>Looking for Video Streams in the current series.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1080"/>
        <source>The completed video file will be copied to the selected location.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1091"/>
        <source>Save the location where the finished video file is copied.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1099"/>
        <source>Save the location for direct download.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1110"/>
        <source>Direct download to the default location.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1118"/>
        <source>Add all Video Streams to Download List</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1121"/>
        <source>Searches for all Video Streams in the current series and adds them to the download list.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1126"/>
        <source>Select font</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1135"/>
        <source>Visit svtplay-dl forum for issues</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1140"/>
        <source>Maintenance Tool</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1143"/>
        <source>Starts the Maintenance Tool. To update or uninstall.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1151"/>
        <source>All saved searches, download list and settings are deleted.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1041"/>
        <source>Download after Date...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1050"/>
        <source>Stop all downloads</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1053"/>
        <source>Trying to stop svtplay-dl.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1066"/>
        <location filename="../newprg.ui" line="1148"/>
        <source>Delete all settings and Exit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1069"/>
        <source>All saved searches and the list of streams to be downloaded will be deleted.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1077"/>
        <source>Copy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1088"/>
        <source>Set Copy Location...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1096"/>
        <source>Set Default Download Location...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1107"/>
        <source>Download to Default Location</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="699"/>
        <source>Check for software updates each time the program starts.</source>
        <oldsource>Check for software updates each time the program starts</oldsource>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="729"/>
        <source>Exits the program.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="111"/>
        <location filename="../newprg.ui" line="762"/>
        <source>Search for video streams.</source>
        <oldsource>Search for video files.</oldsource>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="774"/>
        <source>Paste the link to the page where the video is displayed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="789"/>
        <source>Download the stream you just searched for.</source>
        <oldsource>Download the file you just searched for.</oldsource>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="798"/>
        <source>License</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="839"/>
        <source>View Download list</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="842"/>
        <source>Look at the list of all the streams to download.</source>
        <oldsource>Look at the list of all the files to download.</oldsource>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="889"/>
        <source>Save the name of a video stream provider, your username and, if you want, your password.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1022"/>
        <source>Uninstall streamCapture</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1025"/>
        <source>Uninstall and remove all components</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="533"/>
        <location filename="../newprg.ui" line="901"/>
        <location filename="../newprg.ui" line="1017"/>
        <source>If no saved password is found, click here.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="916"/>
        <source>Searching for and downloading subtitles.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="939"/>
        <source>Add current video to the list of streams that will be downloaded.</source>
        <oldsource>Add current video to the list of files that will be downloaded.</oldsource>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="954"/>
        <source>Download all the streams in the list. If it is the same video stream in different qualities folders are automatically created for each video stream.</source>
        <oldsource>Download all the files in the list. If it is the same video stream in different qualities folders are automatically created for each video stream.</oldsource>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="965"/>
        <source>Automatically create a folder for each downloaded video stream.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1003"/>
        <source>View more information from svtplay-dl.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="886"/>
        <source>Create New</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="924"/>
        <source>Explain what is going on</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="974"/>
        <source>Edit Download list (Advanced)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="977"/>
        <source>Change method or quality. Remove a download from the list. NOTE! If you change incorrectly, it will not work.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="989"/>
        <source>Save Download list (Advanced)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="992"/>
        <source>Your changes to the download list are saved.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1000"/>
        <source>Show more</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="679"/>
        <source>English</source>
        <translation>Englisch</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="143"/>
        <source>Add current video to the list of files that will be downloaded.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="224"/>
        <source>The number of bits that are conveyed or processed per unit of time. Higher numbers give better quality and larger file..</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="358"/>
        <source>Searches for the subtitle and downloads it at the same time as the video stream.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="688"/>
        <source>Swedish</source>
        <translation>Schwedisch</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="696"/>
        <source>Check for updates at program start</source>
        <translation>Beim Programmstart nach Aktualisierungen suchen</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="708"/>
        <source>About</source>
        <translation>Über</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="717"/>
        <source>Check for updates</source>
        <translation>Nach Aktualisierungen suchen</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="726"/>
        <source>Exit</source>
        <translation>Beenden</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="732"/>
        <source>F4</source>
        <translation>F4</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="741"/>
        <source>About svtplay-dl</source>
        <translation>Über svtplay-dl</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="750"/>
        <source>About FFmpeg</source>
        <translation>Über FFmpeg</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="816"/>
        <source>License FFmpeg</source>
        <translation>License FFmpeg</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="807"/>
        <source>License svtplay-dl</source>
        <translation>License svtplay-dl</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="821"/>
        <source>Recent files</source>
        <translation>Neueste Dateien</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="830"/>
        <source>Help</source>
        <translation>Hilfe</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="851"/>
        <location filename="../newprg.ui" line="859"/>
        <source>Delete download list</source>
        <translation>Herunterladeliste löschen</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="854"/>
        <source>All saved streams in the download list are deleted.</source>
        <oldsource>All saved items in the download list are deleted.</oldsource>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="868"/>
        <source>German</source>
        <translation>Deutsche</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="877"/>
        <source>Version history</source>
        <translation>Version History</translation>
    </message>
</context>
</TS>
