<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="sv_SE">
<context>
    <name>CheckForUpdates</name>
    <message>
        <location filename="../checkforupdates.cpp" line="40"/>
        <location filename="../checkforupdates.cpp" line="84"/>
        <source>No Internet connection was found.
Please check your Internet settings and firewall.</source>
        <translation>Ingen Internet-anslutning hittades.
Kontrollera dina Internet-inställningar och brandvägg.</translation>
    </message>
    <message>
        <location filename="../checkforupdates.cpp" line="46"/>
        <location filename="../checkforupdates.cpp" line="89"/>
        <source>
There is a newer version of </source>
        <translation>
Det finns en nyare version av </translation>
    </message>
    <message>
        <location filename="../checkforupdates.cpp" line="46"/>
        <location filename="../checkforupdates.cpp" line="89"/>
        <source>Latest version: </source>
        <translation>Senaste version: </translation>
    </message>
    <message>
        <source>Please download from</source>
        <translation type="vanished">Ladda ner från</translation>
    </message>
    <message>
        <location filename="../checkforupdates.cpp" line="50"/>
        <source>
Your version of </source>
        <translation>
Din version av </translation>
    </message>
    <message>
        <source> is newer than the latest official version </source>
        <translatorcomment> är nyare än den senaste officiella versionen </translatorcomment>
        <translation type="vanished"> är nyare än den senaste officiella versionen </translation>
    </message>
    <message>
        <source>
There is a later version of </source>
        <translation type="vanished">
Det finns en nyare version av </translation>
    </message>
    <message>
        <location filename="../checkforupdates.cpp" line="46"/>
        <location filename="../checkforupdates.cpp" line="89"/>
        <source>Please </source>
        <translation>Vänligen </translation>
    </message>
    <message>
        <location filename="../checkforupdates.cpp" line="46"/>
        <location filename="../checkforupdates.cpp" line="89"/>
        <source>Download</source>
        <translation>Ladda ner</translation>
    </message>
    <message>
        <location filename="../checkforupdates.cpp" line="50"/>
        <source> is newer than the latest official version. </source>
        <translation> är nyare än den senaste officiella versionen. </translation>
    </message>
    <message>
        <source> is later than the latest official version </source>
        <translation type="vanished"> är nyare än den senaste officiella versionen </translation>
    </message>
    <message>
        <location filename="../checkforupdates.cpp" line="54"/>
        <source>
You have the latest version of </source>
        <oldsource>
You have the latestprg version of </oldsource>
        <translation>
Du har den senaste versionen av </translation>
    </message>
    <message>
        <location filename="../checkforupdates.cpp" line="58"/>
        <location filename="../checkforupdates.cpp" line="91"/>
        <source>
There was an error when the version was checked.</source>
        <translation>
Det blev ett fel under versionskontrollen.</translation>
    </message>
    <message>
        <source>No Internet connection was found.
Impossible to check for a new version.
Please check your Internet settings and firewall.</source>
        <oldsource>No Internet connection was found.
Impossible to check for new version.
Please check your Internet settings and firewall.</oldsource>
        <translation type="vanished">Ingen Internet-anslutning hittades.
Omöjligt att kontrollera om det finns en ny version.
Kontrollera dina Internet-inställningar och brandvägg.</translation>
    </message>
    <message>
        <location filename="../checkforupdates.cpp" line="129"/>
        <source>New updates:</source>
        <translation>Nya uppdateringar:</translation>
    </message>
</context>
<context>
    <name>Info</name>
    <message>
        <location filename="../info.cpp" line="29"/>
        <source> streamCapture2 handles downloads of video streams.&lt;br&gt;</source>
        <translation> streamCapture2 hanterar nedladdningar av videoströmmar.&lt;br&gt;</translation>
    </message>
    <message>
        <location filename="../info.cpp" line="30"/>
        <source>Many thanks to </source>
        <translation>Många tack till </translation>
    </message>
    <message>
        <location filename="../info.cpp" line="30"/>
        <source> for the German translation.</source>
        <translation> för den tyska översättningen.</translation>
    </message>
    <message>
        <location filename="../info.cpp" line="29"/>
        <source>A graphical shell for </source>
        <translation>Ett grafiskt skal för </translation>
    </message>
    <message>
        <location filename="../info.cpp" line="29"/>
        <source> and </source>
        <translation> och </translation>
    </message>
    <message>
        <source>CPU architecture:</source>
        <translation type="vanished">CPU-arkitektur:</translation>
    </message>
    <message>
        <location filename="../info.cpp" line="27"/>
        <source> is free software, license </source>
        <translation> är fri mjukvara, licens </translation>
    </message>
    <message>
        <source>, kernel </source>
        <translation type="vanished">, kärna </translation>
    </message>
    <message>
        <location filename="../info.cpp" line="50"/>
        <source> was created </source>
        <translation> skapades </translation>
    </message>
    <message>
        <source> was build for </source>
        <translation type="vanished"> skapades för </translation>
    </message>
    <message>
        <source>Compiled by</source>
        <translation type="vanished">Kompilerad med</translation>
    </message>
    <message>
        <location filename="../info.cpp" line="50"/>
        <source>by a computer with</source>
        <translation>av en dator med</translation>
    </message>
    <message>
        <location filename="../info.cpp" line="63"/>
        <location filename="../info.cpp" line="67"/>
        <location filename="../info.cpp" line="71"/>
        <location filename="../info.cpp" line="147"/>
        <source> Compiled by</source>
        <translation> Kompilerad med</translation>
    </message>
    <message>
        <location filename="../info.cpp" line="79"/>
        <source>Full version number </source>
        <translation>Fullständigt versionsnummer </translation>
    </message>
    <message>
        <location filename="../info.cpp" line="143"/>
        <source>Unknown version</source>
        <translation>Okänd version</translation>
    </message>
    <message>
        <location filename="../info.cpp" line="151"/>
        <source>Unknown compiler.</source>
        <translation>Okänd kompilator.</translation>
    </message>
    <message>
        <location filename="../info.cpp" line="157"/>
        <source>Phone: </source>
        <translation>Telefon: </translation>
    </message>
    <message>
        <location filename="../info.cpp" line="157"/>
        <source>Home page</source>
        <translation>Hemsida</translation>
    </message>
    <message>
        <location filename="../info.cpp" line="157"/>
        <source>Source code</source>
        <translation>Källkod</translation>
    </message>
    <message>
        <location filename="../info.cpp" line="157"/>
        <source>Wiki</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../info.cpp" line="158"/>
        <source>About </source>
        <translation>Om </translation>
    </message>
    <message>
        <location filename="../info.cpp" line="159"/>
        <source>This program uses Qt version </source>
        <translation>Programmet använder Qt version </translation>
    </message>
    <message>
        <location filename="../info.cpp" line="159"/>
        <source> running on </source>
        <translation> och körs på </translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../newprg.cpp" line="348"/>
        <location filename="../newprg.cpp" line="365"/>
        <source>Enter your password</source>
        <translation>Skriv in ditt lösenord</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="349"/>
        <location filename="../newprg.cpp" line="366"/>
        <source>Spaces are not allowed. Use only the characters your streaming provider approves.
The Password will not be saved.</source>
        <translation>Det är inte tillåtet med mellanslag. Använd endast skrivtecken som din streaminleverantör godkänner.
Lösenordet sparas inte.</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="421"/>
        <source>svtplay-dl crashed.</source>
        <translation>svtplay-dl kraschade.</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="427"/>
        <source>Could not stop svtplay-dl.</source>
        <translation>Kunde inte stoppa svtplay-dl.</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="433"/>
        <source>Delete any files that may have already been downloaded.</source>
        <translation>Ta bort alla filer som kan ha blivit nedladdade.</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="433"/>
        <source>svtplay-dl stopped. Exit code </source>
        <translation>svtplay-dl stoppade. Exit kod </translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="451"/>
        <source>Copy to: </source>
        <translation>Kopiera till: </translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="464"/>
        <source>Copy downloaded streaming media to directory</source>
        <translation>Kopiera strömmande media till folder</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="483"/>
        <source>Download streaming media to directory</source>
        <translation>Ladda ner strömmande media till folder</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="503"/>
        <source>Download to: </source>
        <translation>Ladda ner till: </translation>
    </message>
</context>
<context>
    <name>Newprg</name>
    <message>
        <location filename="../about.cpp" line="60"/>
        <location filename="../about.cpp" line="80"/>
        <location filename="../about.cpp" line="100"/>
        <source>The license file is not found</source>
        <translation>Licensfilen kan inte hittas</translation>
    </message>
    <message>
        <source>ffmpeg.exe could not be found. Check your installation. ffmpeg.exe is included with the </source>
        <translation type="vanished">ffmpeg.exe kan inte hittas. Kontrollera din installation. ffmpeg.exe medföljer installationen av </translation>
    </message>
    <message>
        <location filename="../about.cpp" line="204"/>
        <source> installation.</source>
        <translation>.</translation>
    </message>
    <message>
        <source>ffmpeg could not be found. Check your installation. ffmpeg is included with the </source>
        <translation type="vanished">ffmpeg kan inte hittas. Kontrollera din installation. ffmpeg medföljer installationen av </translation>
    </message>
    <message>
        <source>svtplay-dl could not be found. Check your installation. svtplay-dl is included with the </source>
        <translation type="vanished">svtplay-dl kan inte hittas. Kontrollera din installation. svtplay-dl medföljer installationen av </translation>
    </message>
    <message>
        <source>svtplay-dl.exe could not be found. Check your installation. svtplay-dl.exe is included with the </source>
        <translation type="vanished">svtplay-dl.exe kan inte hittas. Kontrollera din installation. svtplay-dl.exe medföljer installationen av </translation>
    </message>
    <message>
        <location filename="../about.cpp" line="177"/>
        <source>version </source>
        <translation>version </translation>
    </message>
    <message>
        <location filename="../language.cpp" line="15"/>
        <location filename="../language.cpp" line="40"/>
        <location filename="../language.cpp" line="65"/>
        <source>The program must be restarted for the new language settings to take effect.</source>
        <translation>Programmet måste startas om för att de nya språkinställningarna ska börja gälla.</translation>
    </message>
    <message>
        <location filename="../language.cpp" line="16"/>
        <location filename="../language.cpp" line="41"/>
        <location filename="../language.cpp" line="66"/>
        <source>Restart Now</source>
        <translation>Starta om nu</translation>
    </message>
    <message>
        <location filename="../language.cpp" line="17"/>
        <location filename="../language.cpp" line="42"/>
        <location filename="../language.cpp" line="67"/>
        <location filename="../setgetconfig.cpp" line="257"/>
        <location filename="../setgetconfig.cpp" line="287"/>
        <source>Cancel</source>
        <translation>Avbryt</translation>
    </message>
    <message>
        <location filename="../setgetconfig.cpp" line="179"/>
        <source>Up and running</source>
        <oldsource>Upp and running</oldsource>
        <translation>Uppe och kör</translation>
    </message>
    <message>
        <location filename="../setgetconfig.cpp" line="248"/>
        <location filename="../setgetconfig.cpp" line="278"/>
        <source>Uninstall </source>
        <translation>Avinstallera </translation>
    </message>
    <message>
        <location filename="../setgetconfig.cpp" line="248"/>
        <source> and Delete all Settings</source>
        <translation> och ta bort alla inställningar</translation>
    </message>
    <message>
        <location filename="../setgetconfig.cpp" line="250"/>
        <source>Uninstall and completely remove all components. All saved searches and the list of streams to download will be deleted.</source>
        <translation>Avinstallerar och tar bort alla filer. Alla sparade sökningar och listan över strömmar att ladda ner tas bort.</translation>
    </message>
    <message>
        <location filename="../setgetconfig.cpp" line="255"/>
        <location filename="../setgetconfig.cpp" line="285"/>
        <source>Do you want to uninstall </source>
        <translation>Vill du avinstallera </translation>
    </message>
    <message>
        <location filename="../setgetconfig.cpp" line="256"/>
        <location filename="../setgetconfig.cpp" line="286"/>
        <source>Uninstall Now</source>
        <translation>Avinstallera nu</translation>
    </message>
    <message>
        <location filename="../setgetconfig.cpp" line="272"/>
        <location filename="../setgetconfig.cpp" line="293"/>
        <source>Uninstaller not found.</source>
        <translation>Avinstallationsprogrammet hittas inte.</translation>
    </message>
    <message>
        <location filename="../setgetconfig.cpp" line="280"/>
        <source>Uninstall and completely remove all components.</source>
        <translation>Avinstallerar och tar bort alla filer.</translation>
    </message>
    <message>
        <location filename="../setgetconfig.cpp" line="335"/>
        <source>Could not save a file to store Recent Search list. Check your file permissions.</source>
        <oldsource>Could not save a file to store Recent Searche list. Check your file permissions.</oldsource>
        <translation>Kunde inte spara listan med tidigare.sökningar. Kolla dina filrättigheter.</translation>
    </message>
    <message>
        <location filename="../setgetconfig.cpp" line="351"/>
        <source>Could not save a file to store the list off downloads. Check your file permissions.</source>
        <translation>Kunde inte spara listan med tidigare.nedladdningar. Kolla dina filrättigheter.</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="37"/>
        <location filename="../downloadallepisodes.cpp" line="59"/>
        <source>The request is processed...
Preparing to download...</source>
        <translation>Begäran behandlas ...
Förbereder sig för att ladda ner ...</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="253"/>
        <location filename="../listallepisodes.cpp" line="31"/>
        <source>The request is processed...
Starting search...</source>
        <translation>Begäran behandlas ...
Startar att söka...</translation>
    </message>
    <message>
        <location filename="../listallepisodes.cpp" line="46"/>
        <location filename="../sok.cpp" line="41"/>
        <source>The search field is empty!</source>
        <translation>Sökrutan är tom!</translation>
    </message>
    <message>
        <location filename="../listallepisodes.cpp" line="50"/>
        <location filename="../sok.cpp" line="45"/>
        <source>Incorrect URL</source>
        <translation>Felaktig URL</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="601"/>
        <source>Can not find any video streams, please check the address.

</source>
        <translation>Det går inte att hitta några videoströmmar , kontrollera adressen.

</translation>
    </message>
    <message>
        <location filename="../save.cpp" line="30"/>
        <source>Save streaming media to directory</source>
        <oldsource>Save streaming media files to directory</oldsource>
        <translation>Spara strömmande media till en folder</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="559"/>
        <source>Downloading...</source>
        <translation>Laddar ner...</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="623"/>
        <source>The search is complete&lt;br&gt;</source>
        <translation>Sökningen är klar&lt;br&gt;</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="625"/>
        <source>The search failed&lt;br&gt;</source>
        <translation>Sökningen misslyckades&lt;br&gt;</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="43"/>
        <source>The version history file is not found</source>
        <translation>Filen med versionshistorik kan inte hittas</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="204"/>
        <source> could not be found. Check your installation. </source>
        <translation> kan inte hittas. Kontrollera din installation. </translation>
    </message>
    <message>
        <location filename="../about.cpp" line="204"/>
        <source> is included with the </source>
        <translation> är inkluderad i </translation>
    </message>
    <message>
        <source> help</source>
        <translation type="vanished"> hjälp</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="175"/>
        <source>Starts downloading: </source>
        <translation>Startar nedladdningen: </translation>
    </message>
    <message>
        <location filename="../download.cpp" line="236"/>
        <location filename="../downloadall.cpp" line="305"/>
        <location filename="../downloadallepisodes.cpp" line="189"/>
        <source>No folder is selected</source>
        <translation>Ingen mapp är vald</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="153"/>
        <location filename="../downloadall.cpp" line="149"/>
        <location filename="../downloadallepisodes.cpp" line="139"/>
        <location filename="../listallepisodes.cpp" line="94"/>
        <location filename="../sok.cpp" line="89"/>
        <source>Spaces are not allowed. Use only the characters your streaming provider approves.
The Password will not be saved.</source>
        <translation>Det är inte tillåtet med mellanslag. Använd endast skrivtecken som din streaminleverantör godkänner.
Lösenordet sparas inte.</translation>
    </message>
    <message>
        <location filename="../downloadall.cpp" line="100"/>
        <location filename="../downloadall.cpp" line="199"/>
        <source>The video streams are saved in </source>
        <oldsource>The video files are saved in </oldsource>
        <translation>Videoströmmarna sparas i </translation>
    </message>
    <message>
        <location filename="../download.cpp" line="80"/>
        <location filename="../download.cpp" line="83"/>
        <source>The video stream is saved in </source>
        <oldsource>The video file is saved in </oldsource>
        <translation>Videoströmmen sparas i </translation>
    </message>
    <message>
        <location filename="../download.cpp" line="90"/>
        <location filename="../downloadall.cpp" line="39"/>
        <location filename="../downloadallepisodes.cpp" line="69"/>
        <source>The default folder for downloading video streams cannot be found.
Download is interrupted.</source>
        <translation>Standardmappen för att ladda ner videoströmmar kan inte hittas.
Nedladdningen avbryts.</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="96"/>
        <location filename="../downloadall.cpp" line="45"/>
        <location filename="../downloadall.cpp" line="66"/>
        <location filename="../downloadallepisodes.cpp" line="75"/>
        <source>You do not have the right to save to the folder.
Download is interrupted.</source>
        <translation>Du har inte rätten att spara i mappen du valt.
Nedladdningen avbryts.</translation>
    </message>
    <message>
        <location filename="../downloadall.cpp" line="62"/>
        <source>The default folder for copying video streams cannot be found.
Download is interrupted.</source>
        <translation>Standardmappen för att kopiera videoströmmar kan inte hittas.
Nedladdningen avbryts.</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="217"/>
        <location filename="../downloadall.cpp" line="283"/>
        <source>Download completed</source>
        <translation>Nedladdningen klar</translation>
    </message>
    <message>
        <location filename="../downloadallepisodes.cpp" line="23"/>
        <source>You have chosen to copy to &quot;Default folder&quot;.
Unfortunately, this does not work when you select &quot;Download all episodes&quot;.</source>
        <translation>Du har valt att kopiera till &quot;Förvald folder&quot;.
 Detta fungerar tyvärr inte när du väljer &quot;Ladda ner alla episoder&quot;.</translation>
    </message>
    <message>
        <location filename="../downloadallepisodes.cpp" line="27"/>
        <source>Because you cannot select &quot;Method&quot; and &quot;Quality&quot; when you select &quot;Download all episodes&quot;, no folders will be created.

Do you want to start the download?</source>
        <translation>Eftersom du inte kan välja &quot;Metod&quot; och &quot;Kvalitet&quot; när du väljer &quot;Ladda ner alla avsnitt&quot;, skapas inga mappar.

Vill du starta nedladdningen?</translation>
    </message>
    <message>
        <location filename="../downloadallepisodes.cpp" line="35"/>
        <source>Once svtplay-dl has started downloading all episodes, streamCapture2 no longer has control. If you want to cancel, you may need to log out or restart your computer.

Do you want to start the download?</source>
        <translation>När svtplay-dl startat nedladdningarna av alla episoder har streamCapture2 inte längre någon kontroll. Om du vill avbryta kan du behöva logga ut eller starta om datorn.

Vill du starta nedladdningen?</translation>
    </message>
    <message>
        <location filename="../downloadallepisodes.cpp" line="160"/>
        <source>Starts downloading all episodes: </source>
        <translation>Börjar ladda ner alla avsnitt: </translation>
    </message>
    <message>
        <location filename="../downloadallepisodes.cpp" line="174"/>
        <source>The media files (and if you have selected the subtitles) have been downloaded.</source>
        <translation>Mediefilerna (och om du har valt undertexterna) har laddats ner.</translation>
    </message>
    <message>
        <location filename="../downloadallepisodes.cpp" line="179"/>
        <source>Download failed</source>
        <translation>Nedladdningen misslyckades</translation>
    </message>
    <message>
        <location filename="../downloadall.cpp" line="70"/>
        <source>The video files are copied to </source>
        <translation>Vidoefilerna kopieras till </translation>
    </message>
    <message>
        <location filename="../downloadall.cpp" line="240"/>
        <source>Preparing to download</source>
        <translation>Förbereder att ladda ner</translation>
    </message>
    <message>
        <location filename="../downloadall.cpp" line="250"/>
        <location filename="../downloadall.cpp" line="255"/>
        <source> succeeded (</source>
        <translation> lyckades (</translation>
    </message>
    <message>
        <location filename="../downloadall.cpp" line="250"/>
        <location filename="../downloadall.cpp" line="255"/>
        <location filename="../downloadall.cpp" line="270"/>
        <source>Download </source>
        <translation>Nedladdning </translation>
    </message>
    <message>
        <location filename="../downloadall.cpp" line="270"/>
        <source> failed (</source>
        <translation> misslyckades (</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="202"/>
        <location filename="../download.cpp" line="208"/>
        <source>Download succeeded </source>
        <translation>Nedladdningen lyckades </translation>
    </message>
    <message>
        <location filename="../download.cpp" line="225"/>
        <source>Download failed </source>
        <translation>Nedladdningen misslyckades </translation>
    </message>
    <message>
        <location filename="../download.cpp" line="251"/>
        <location filename="../download.cpp" line="252"/>
        <location filename="../listallepisodes.cpp" line="29"/>
        <location filename="../listallepisodes.cpp" line="30"/>
        <source>Searching...</source>
        <translation>Söker...</translation>
    </message>
    <message>
        <location filename="../listallepisodes.cpp" line="140"/>
        <location filename="../sok.cpp" line="135"/>
        <source> crashed.</source>
        <translation> kraschade.</translation>
    </message>
    <message>
        <location filename="../listallepisodes.cpp" line="141"/>
        <location filename="../sok.cpp" line="136"/>
        <source>Can not find any video streams, please check the address.</source>
        <translation>Kan inte hitta någon videoström, kontrollera adressen.</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="101"/>
        <source>The video file is copied to </source>
        <translation>Videofilen kopieras till </translation>
    </message>
    <message>
        <source>unknown</source>
        <translation type="vanished">okänd</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="63"/>
        <source>normal</source>
        <translation>normal</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="66"/>
        <source>bold and italic</source>
        <translation>fet och kursiv</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="69"/>
        <source>bold</source>
        <translation>fet</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="71"/>
        <source>italic</source>
        <translation>kursivt</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="74"/>
        <source>Current font:</source>
        <translation>Nuvarande teckensnitt:</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="74"/>
        <source>size:</source>
        <translation>storlek:</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="531"/>
        <source>MaintenanceTool cannot be found</source>
        <translation>Kan inte hitta Underhållsverktyget</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="648"/>
        <source>Click to copy to the search box.</source>
        <translation>Klicka för att kopiera till sökrutan.</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="662"/>
        <source>The number of previous searches to be saved...</source>
        <oldsource>The number of recently opened files to be displayed...</oldsource>
        <translation>Antalet tidigare sökningar som ska sparas...</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="664"/>
        <source>Specify how many previous searches you want to save. If the number of searches exceeds the specified number, the oldest search is deleted.</source>
        <translation>Bestäm antalet tidigare sökningar som ska sparas. Om antalet sökningar överstiger det angivna antalet kommer den äldsta sökningen att tas bort.</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="673"/>
        <source>Remove all saved searches</source>
        <translation>Ta bort alla tidigare sökningar</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="675"/>
        <source>Click to delete all saved searches.</source>
        <translation>Klicka för att ta bort alla sparade sökningar.</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="689"/>
        <source>All your saved settings will be deleted. All lists of files to download will disappear.</source>
        <translation>Alla dina sparade inställningar raderas. Alla listor över filer att ladda ner försvinner.</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="697"/>
        <source>Failed to delete your configuration files. Check your file permissions.</source>
        <translation>Misslyckades med att ta bort dina inställningsfiler. Kolla dina filrättigheter.</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="707"/>
        <source> was normally terminated. QProsess Exit code = </source>
        <translation> avslutades normalt QProcess Exit kod = </translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="712"/>
        <source> crashed. QProsess Exit code = </source>
        <translation> kraschade QProcess Exit code = </translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="668"/>
        <source>The number of searches to be saved: </source>
        <oldsource>Set number of Recent Files: </oldsource>
        <translation>Ange maxantalet sparade sökningar: </translation>
    </message>
    <message>
        <location filename="../paytv_create.cpp" line="57"/>
        <source>Enter your service provider</source>
        <translation>Ange din tjänsteleverantör</translation>
    </message>
    <message>
        <location filename="../paytv_create.cpp" line="58"/>
        <location filename="../paytv_create.cpp" line="80"/>
        <location filename="../paytv_create.cpp" line="102"/>
        <location filename="../paytv_edit.cpp" line="100"/>
        <location filename="../paytv_edit.cpp" line="122"/>
        <source>Spaces are not allowed. Use only the characters your streaming provider approves.</source>
        <translation>Mellanslag är inte tillåtna. Använd endast de tecken som din streamingleverantör godkänner.</translation>
    </message>
    <message>
        <location filename="../paytv_create.cpp" line="79"/>
        <location filename="../paytv_edit.cpp" line="99"/>
        <source>Enter your username</source>
        <translation>Skriv in ditt användarnamn</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="152"/>
        <location filename="../downloadall.cpp" line="148"/>
        <location filename="../downloadallepisodes.cpp" line="138"/>
        <location filename="../listallepisodes.cpp" line="93"/>
        <location filename="../paytv_create.cpp" line="101"/>
        <location filename="../paytv_edit.cpp" line="121"/>
        <location filename="../sok.cpp" line="88"/>
        <source>Enter your password</source>
        <translation>Skriv in ditt lösenord</translation>
    </message>
    <message>
        <location filename="../paytv_create.cpp" line="110"/>
        <location filename="../paytv_edit.cpp" line="130"/>
        <source>Save password?</source>
        <translation>Spara lösenordet?</translation>
    </message>
    <message>
        <location filename="../paytv_create.cpp" line="111"/>
        <location filename="../paytv_edit.cpp" line="131"/>
        <source>Do you want to save the password? (unsafe)?</source>
        <translation>Vill du spara lösenordet (osäkert)?</translation>
    </message>
    <message>
        <location filename="../paytv_edit.cpp" line="24"/>
        <source>Edit or delete
</source>
        <translation>Redigera eller ta bort
</translation>
    </message>
    <message>
        <location filename="../paytv_edit.cpp" line="25"/>
        <source>Edit</source>
        <translation>Redigera</translation>
    </message>
    <message>
        <location filename="../paytv_edit.cpp" line="26"/>
        <source>Delete</source>
        <oldsource>Delet</oldsource>
        <translation>Ta bort</translation>
    </message>
    <message>
        <location filename="../paytv_edit.cpp" line="51"/>
        <source>Create New</source>
        <translation>Skapa ny</translation>
    </message>
    <message>
        <location filename="../paytv_edit.cpp" line="59"/>
        <location filename="../setgetconfig.cpp" line="227"/>
        <source>No Password</source>
        <translation>Inget lösenord</translation>
    </message>
    <message>
        <location filename="../coppytodefaultlocation.cpp" line="54"/>
        <location filename="../coppytodefaultlocation.cpp" line="136"/>
        <source>You have not selected any place to copy the media files.
Please select a location before proceeding.</source>
        <translation>Du har inte valt någon plats att kopiera mediafilerna till.
Var vänlig och bestäm en plats innan du fortsätter.</translation>
    </message>
    <message>
        <location filename="../coppytodefaultlocation.cpp" line="72"/>
        <location filename="../coppytodefaultlocation.cpp" line="96"/>
        <location filename="../coppytodefaultlocation.cpp" line="145"/>
        <location filename="../coppytodefaultlocation.cpp" line="165"/>
        <source>Copy succeeded </source>
        <translation>Kopieringen lyckades </translation>
    </message>
    <message>
        <location filename="../coppytodefaultlocation.cpp" line="76"/>
        <location filename="../coppytodefaultlocation.cpp" line="100"/>
        <location filename="../coppytodefaultlocation.cpp" line="149"/>
        <location filename="../coppytodefaultlocation.cpp" line="169"/>
        <source>Copy failed </source>
        <translation>Kopieringen misslyckades </translation>
    </message>
</context>
<context>
    <name>SelectFont</name>
    <message>
        <location filename="../selectfont.ui" line="26"/>
        <source>Select font</source>
        <translation>Välj teckensnitt</translation>
    </message>
    <message>
        <location filename="../selectfont.ui" line="85"/>
        <source>Normal</source>
        <translation>Normal</translation>
    </message>
    <message>
        <location filename="../selectfont.ui" line="95"/>
        <source>Bold</source>
        <translation>Fet</translation>
    </message>
    <message>
        <location filename="../selectfont.ui" line="102"/>
        <source>Italic</source>
        <translation>Kursiv</translation>
    </message>
    <message>
        <location filename="../selectfont.ui" line="109"/>
        <source>Bold and italic</source>
        <translation>Fet och kursiv</translation>
    </message>
    <message>
        <source>Apply</source>
        <translation type="vanished">Verställ</translation>
    </message>
    <message>
        <location filename="../selectfont.ui" line="138"/>
        <source>Default</source>
        <translation>Standard</translation>
    </message>
    <message>
        <location filename="../selectfont.ui" line="145"/>
        <source>Exit</source>
        <translation>Stäng</translation>
    </message>
    <message>
        <location filename="../selectfont.cpp" line="55"/>
        <source>All fonts</source>
        <translation>Alla teckensnitt</translation>
    </message>
    <message>
        <source>Scalable fonts</source>
        <translation type="vanished">Skalbara teckensnitt</translation>
    </message>
    <message>
        <source>Non scalable fonts</source>
        <translation type="vanished">Ej skalbara teckensnit</translation>
    </message>
    <message>
        <location filename="../selectfont.cpp" line="55"/>
        <source>Monospaced fonts</source>
        <translation>Teckensnitt med fast breddsteg</translation>
    </message>
    <message>
        <location filename="../selectfont.cpp" line="55"/>
        <source>Proportional fonts</source>
        <translation>Proportionella teckensnitt</translation>
    </message>
</context>
<context>
    <name>newprg</name>
    <message>
        <location filename="../newprg.ui" line="114"/>
        <location filename="../newprg.ui" line="759"/>
        <source>Search</source>
        <translation>Sök</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="302"/>
        <source>Method</source>
        <translation>Metod</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="440"/>
        <location filename="../newprg.ui" line="786"/>
        <source>Download</source>
        <translation>Ladda ner</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="578"/>
        <source>&amp;File</source>
        <translation>&amp;Arkiv</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="32"/>
        <source>TEST</source>
        <translation>TEST</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="62"/>
        <source>Paste the link to the page where the video is displayed</source>
        <translation>Klistra in länken till sidan där videon visas</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="233"/>
        <source>Quality (Bitrate)</source>
        <oldsource>Bitrate</oldsource>
        <translation>Kvalitet (Bitrate)</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="287"/>
        <source>Media streaming communications protocol.</source>
        <translation>Protokoll för att strömma media.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="437"/>
        <source>Download the file you just searched for</source>
        <translation>Ladda ner filen som du just sökt efter</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="399"/>
        <location filename="../newprg.ui" line="951"/>
        <source>Download all</source>
        <translation>Ladda ner alla</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="453"/>
        <source>Select quality on the video you download</source>
        <translation>Välj kvalitet på videon du laddar ner</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="146"/>
        <location filename="../newprg.ui" line="936"/>
        <source>Add to Download list</source>
        <translation>Lägg till i nedladdningslistan</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="65"/>
        <location filename="../newprg.ui" line="771"/>
        <source>Paste</source>
        <translation>Klistra in</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="462"/>
        <source>Quality (bitrate) and method. Higher bitrate gives better quality and larger file.</source>
        <translation>Kvalitet (bitrate) och metod. Högre bitrate ger högre kvalitet och större fil.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="361"/>
        <location filename="../newprg.ui" line="913"/>
        <source>Include Subtitle</source>
        <translation>Inkludera undertexten</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="396"/>
        <source>Download all files you added to the list.</source>
        <translation>Ladda ner alla filer som du lagt till på listan.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="492"/>
        <source>Select quality on the video you download.</source>
        <translation>Välj kvalitet på videoströmmen du laddar ner.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="520"/>
        <source>Select provider. If yoy need a password.</source>
        <translation>Välj en tjänsteleverantör. Om du behöver lösenord.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="536"/>
        <location filename="../newprg.ui" line="898"/>
        <location filename="../newprg.ui" line="1014"/>
        <source>Password</source>
        <translation>Lösenord</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="590"/>
        <source>&amp;Language</source>
        <translation>&amp;Språk</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="598"/>
        <source>&amp;Tools</source>
        <translation>&amp;Verktyg</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="615"/>
        <source>&amp;Help</source>
        <translation>&amp;Hjälp</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="631"/>
        <source>&amp;Recent</source>
        <oldsource>Recent</oldsource>
        <translation>S&amp;enaste</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="636"/>
        <source>&amp;Download list</source>
        <translation>&amp;Nedladdningslista</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="649"/>
        <source>&amp;Pay TV</source>
        <oldsource>Pay TV</oldsource>
        <translation>&amp;Betal-TV</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="656"/>
        <source>&amp;Several episodes</source>
        <translation>&amp;Flera avsnitt</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="679"/>
        <source>English</source>
        <translation>Engelska</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="688"/>
        <source>Swedish</source>
        <translation>Svenska</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="696"/>
        <source>Check for updates at program start</source>
        <translation>Kolla efter uppdateringar när programmet startat</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="699"/>
        <source>Check for software updates each time the program starts.</source>
        <oldsource>Check for software updates each time the program starts</oldsource>
        <translation>Kolla efter uppdateringar varje gång programmet startar.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="708"/>
        <source>About</source>
        <translation>Om</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="717"/>
        <source>Check for updates</source>
        <translation>Kolla efter uppdateringar</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="726"/>
        <source>Exit</source>
        <translation>Stäng</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="729"/>
        <source>Exits the program.</source>
        <translation>Avsluta programmet.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="732"/>
        <source>F4</source>
        <translation>F4</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="741"/>
        <source>About svtplay-dl</source>
        <translation>Om svtplay-dl</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="750"/>
        <source>About FFmpeg</source>
        <translation>Om FFmpeg</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="962"/>
        <source>Create folder &quot;method_quality&quot;</source>
        <translation>Skapa folder &quot;metod_kvalitet&quot;</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1033"/>
        <source>Direct Download of all Video Streams in current serie (Not from the Download List)</source>
        <translation>Direktnedladdning av alla videoströmmar i nuvarande serie (inte från nedladdningslistan)</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1036"/>
        <source>Searches for all Video Streams in the current series and tries to download them directly.</source>
        <translation>Söker efter alla videoströmmar i aktuell serie och försöker ladda ner dem direkt.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1058"/>
        <source>List all Video Streams</source>
        <translation>Lista alla videoströmmar</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1061"/>
        <source>Looking for Video Streams in the current series.</source>
        <translation>Letar efter videoströmmar i aktuell serie.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1080"/>
        <source>The completed video file will be copied to the selected location.</source>
        <translation>Den färdiga videofilen kommer att kopieras till den valda platsen.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1091"/>
        <source>Save the location where the finished video file is copied.</source>
        <translation>Spara platsen dit den färdiga videofilen kopieras.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1099"/>
        <source>Save the location for direct download.</source>
        <translation>Spara platsen för direkt nedladdning.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1110"/>
        <source>Direct download to the default location.</source>
        <translation>Direktnedladdning till standardplatsen.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1118"/>
        <source>Add all Video Streams to Download List</source>
        <translation>Lägg till alla videoströmmar i nedladdningslistan</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1121"/>
        <source>Searches for all Video Streams in the current series and adds them to the download list.</source>
        <translation>Söker efter alla videoströmmar i aktuell serie och lägger till dem i nedladdningslistan.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1126"/>
        <source>Select font</source>
        <translation>Välj teckensnitt</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1135"/>
        <source>Visit svtplay-dl forum for issues</source>
        <translatorcomment>Besök svtplay-dl forum för problem</translatorcomment>
        <translation>Besök svtplay-dl forum för problem</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1140"/>
        <source>Maintenance Tool</source>
        <translation>Underhållsverktyg</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1143"/>
        <source>Starts the Maintenance Tool. To update or uninstall.</source>
        <translation>Startar Underhållsverktyget. För att uppdatera eller avinstallera.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1151"/>
        <source>All saved searches, download list and settings are deleted.</source>
        <translation>Alla sparade sökningar, nedladdningslistan och alla inställningar tas bort.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1066"/>
        <location filename="../newprg.ui" line="1148"/>
        <source>Delete all settings and Exit</source>
        <translation>Ta bort alla inställningsfiler och avsluta</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1069"/>
        <source>All saved searches and the list of streams to be downloaded will be deleted.</source>
        <translation>Alla sparade sökningar och listan med filer som ska laddas ner tas bort.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1077"/>
        <source>Copy</source>
        <translation>Kopiera</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1088"/>
        <source>Set Copy Location...</source>
        <translation>Välj folder för kopiering...</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1096"/>
        <source>Set Default Download Location...</source>
        <translation>Ställ in standardplats för nedladdning...</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1107"/>
        <source>Download to Default Location</source>
        <translation>Ladda ner till standardplatsen</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="111"/>
        <location filename="../newprg.ui" line="762"/>
        <source>Search for video streams.</source>
        <oldsource>Search for video files.</oldsource>
        <translation>Sök efter videoströmmar.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="143"/>
        <source>Add current video to the list of files that will be downloaded.</source>
        <translation>Lägg till denna videoström till listan med videoströmmar som ska laddas ner.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="224"/>
        <source>The number of bits that are conveyed or processed per unit of time. Higher numbers give better quality and larger file..</source>
        <translation>Antalet bitar som transporteras eller behandlas per tidsenhet. Högre siffror ger bättre kvalitet och större fil ..</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="358"/>
        <source>Searches for the subtitle and downloads it at the same time as the video stream.</source>
        <translation>Söker efter undertexten och laddar ner den samtidigt som videoströmmen.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="774"/>
        <source>Paste the link to the page where the video is displayed.</source>
        <translation>Klistra in länken till webbsidan där videon visas.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="789"/>
        <source>Download the stream you just searched for.</source>
        <oldsource>Download the file you just searched for.</oldsource>
        <translation>Ladda ner videoströmmen du just sökt efter.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="798"/>
        <source>License</source>
        <translation>Licens</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="816"/>
        <source>License FFmpeg</source>
        <translation>Licens FFmpeg</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="839"/>
        <source>View Download list</source>
        <translation>Titta på nedladdningslistan</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="842"/>
        <source>Look at the list of all the streams to download.</source>
        <oldsource>Look at the list of all the files to download.</oldsource>
        <translation>Titta på listan över alla videoströmmar som ska laddas ner.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="889"/>
        <source>Save the name of a video stream provider, your username and, if you want, your password.</source>
        <translation>Spara namnet på en leverantör av videoströmmar, ditt användarnamn, och om du vill, ditt lösenord.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1022"/>
        <source>Uninstall streamCapture</source>
        <translation>Avinstallera streamCapture</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1025"/>
        <source>Uninstall and remove all components</source>
        <translation>Avinstallera och ta bort alla komponenter</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1041"/>
        <source>Download after Date...</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1050"/>
        <source>Stop all downloads</source>
        <translation>Stoppa alla nedladdningar</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1053"/>
        <source>Trying to stop svtplay-dl.</source>
        <translation>Försöker stoppa svtplay-dl.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="533"/>
        <location filename="../newprg.ui" line="901"/>
        <location filename="../newprg.ui" line="1017"/>
        <source>If no saved password is found, click here.</source>
        <translation>Om inget lösenord är sparat, klicka här.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="916"/>
        <source>Searching for and downloading subtitles.</source>
        <translation>Söker efter och laddar ner undertexter.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="939"/>
        <source>Add current video to the list of streams that will be downloaded.</source>
        <oldsource>Add current video to the list of files that will be downloaded.</oldsource>
        <translation>Lägg till aktuell video till listan på videoströmmar som ska laddas ner.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="954"/>
        <source>Download all the streams in the list. If it is the same video stream in different qualities folders are automatically created for each video stream.</source>
        <oldsource>Download all the files in the list. If it is the same video stream in different qualities folders are automatically created for each video stream.</oldsource>
        <translation>Ladda ner alla videoströmmar i listan. Om det är samma videoström i olika kvaliteter kommer foldrar för varje videoström att skapas automatiskt.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="965"/>
        <source>Automatically create a folder for each downloaded video stream.</source>
        <translation>Skapa automatiskt en folder för varje nedladdad videoström.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1003"/>
        <source>View more information from svtplay-dl.</source>
        <translation>Visa mer information från svtplay-dl.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="886"/>
        <source>Create New</source>
        <translation>Skapa ny</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="924"/>
        <source>Explain what is going on</source>
        <translation>Förklar vad som händer</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="974"/>
        <source>Edit Download list (Advanced)</source>
        <translation>Redigera nedladdningslistan (avancerat)</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="977"/>
        <source>Change method or quality. Remove a download from the list. NOTE! If you change incorrectly, it will not work.</source>
        <translation>Ändra metod eller kvalitet. Ta bort en fil från nedladdning. OBS! Om du gör felaktiga ändringar kommer det inte att fungera.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="989"/>
        <source>Save Download list (Advanced)</source>
        <translation>Spara nedladdningslistan (avancerat)</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="992"/>
        <source>Your changes to the download list are saved.</source>
        <translation>Dina ändringar i nedladdningslisten är sparade.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1000"/>
        <source>Show more</source>
        <translation>Visa mer</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="807"/>
        <source>License svtplay-dl</source>
        <translation>Licens svtplay-dl</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="821"/>
        <source>Recent files</source>
        <translation>Senaste filerna</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="830"/>
        <source>Help</source>
        <translation>Hjälp</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="851"/>
        <location filename="../newprg.ui" line="859"/>
        <source>Delete download list</source>
        <translation>Ta bort nedladdningslistan</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="854"/>
        <source>All saved streams in the download list are deleted.</source>
        <oldsource>All saved items in the download list are deleted.</oldsource>
        <translation>Alla sparade videoströmmar i nedladdningslistan tas bort.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="868"/>
        <source>German</source>
        <translation>Tyska</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="877"/>
        <source>Version history</source>
        <translation>Versionshistorik</translation>
    </message>
</context>
</TS>
