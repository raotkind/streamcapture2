/*
    streamCapture2
    Copyright (C) 2016 - 2020 Ingemar Ceicer
    http://ceicer.org/ingemar/
    programmering1 (at) ceicer (dot) org

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
*/
#include <QApplication>
#include <QTranslator>
#include <QLocale>


#include <string>
#include "newprg.h"
#include "ui_newprg.h"
#include "info.h"
#include "checkforupdates.h"
int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    QString fontPath = ":/fonts/PTSans-Regular.ttf";
    int fontId = QFontDatabase::addApplicationFont(fontPath);

    if(fontId != -1) {
#ifdef Q_OS_LINUX     // Linux
        QFont f("PT Sans", LINUX_FONT);
#endif
#ifdef Q_OS_WIN // Windows 32- and 64-bit
        QFont f("PT Sans", WINDOWS_FONT);
#endif
        QApplication::setFont(f);
        // qDebug() << "BRA";
    }

    /*else {
         qDebug() << "KASS";
    }*/
    QTranslator translator;
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME, EXECUTABLE_NAME);
    settings.setIniCodec("UTF-8");
    settings.beginGroup("Language");
    QString sp = settings.value("language", "").toString();
    settings.endGroup();
    const QString translationPath = ":/i18n/streamcapture2";

    if(sp  == "") {
        sp = QLocale::system().name();

        if(translator.load(translationPath + "_" + sp + ".qm")) {
            QApplication::installTranslator(&translator);
            settings.beginGroup("Language");
            settings.setValue("language", sp);
            settings.endGroup();
        } else {
            if(sp != "en_US")
                QMessageBox::information(nullptr, DISPLAY_NAME " " VERSION, DISPLAY_NAME " is unfortunately not yet translated into your language, the program will start with English menus.");

            settings.beginGroup("Language");
            settings.setValue("language", sp);
            settings.endGroup();
        }
    } else {
        if(translator.load(translationPath + "_" + sp + ".qm"))
            QApplication::installTranslator(&translator);
    }

    Newprg mNewPrg;
    // End config
    QObject::connect(&app, &QApplication::aboutToQuit, [&mNewPrg]()->void {

        mNewPrg.setEndConfig();
    });
    mNewPrg.show();
    int r = QApplication::exec();
    /* END CONFIG */
    /* settings.beginGroup("MainWindow");
     settings.setValue("size", w.size());
     settings.setValue("pos", w.pos());
     settings.endGroup();*/
    return r;
}
