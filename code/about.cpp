#include "newprg.h"
#include "ui_newprg.h"
#include "info.h"
#include "checkforupdates.h"
void Newprg::about()
{
    auto *information = new Info;
    information->getSystem();
    delete information;
}
void Newprg::versionHistory()
{
    ui->teOut->clear();
    ui->teOut->setReadOnly(true);
    ui->teOut->setAcceptRichText(false);
    ui->actionSaveDownloadList->setEnabled(false);
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME, EXECUTABLE_NAME);
    settings.setIniCodec("UTF-8");
    settings.beginGroup("Language");
    QString sp = settings.value("language", "").toString();
    settings.endGroup();
    QString readme_sp(":/txt/readme_" + sp + ".txt");
    QFile file(readme_sp);

    if(file.exists()) {
        file.open(QIODevice::ReadOnly);
        QTextStream stream(&file);
        stream.setCodec("UTF-8");
        QString content = stream.readAll();
        file.close();
        ui->teOut->setText(content);
    } else {
        QString readme(":/txt/readme.txt");
        QFile file(readme);

        if(file.exists()) {
            file.open(QIODevice::ReadOnly);
            QTextStream stream(&file);
            QString content = stream.readAll();
            file.close();
            ui->teOut->setText(content);
        } else
            QMessageBox::warning(this, DISPLAY_NAME " " VERSION, tr("The version history file is not found"));
    }
}
void Newprg::license()
{
    ui->teOut->setReadOnly(true);
    ui->teOut->setAcceptRichText(false);
    ui->actionSaveDownloadList->setEnabled(false);
#ifdef Q_OS_LINUX     // Linux
    QString license(":/txt/license_linux.txt");
#endif
#ifdef Q_OS_WIN // Windows 32- and 64-bit
    QString license(":/txt/license_windows.txt");
#endif
    QFile file(license);

    if(!file.exists())
        QMessageBox::warning(this, DISPLAY_NAME " " VERSION, tr("The license file is not found"));
    else {
        file.open(QIODevice::ReadOnly);
        QTextStream stream(&file);
        QString content = stream.readAll();
        file.close();
        //  ui->teOut->setFontWeight(QFont::Normal);
        ui->teOut->setText(content);
    }
}

void Newprg::licenseSvtplayDl()
{
    ui->teOut->setReadOnly(true);
    ui->teOut->setAcceptRichText(false);
    ui->actionSaveDownloadList->setEnabled(false);
    QString license_svtplay_dl(":/txt/license_svtplay-dl.txt");
    QFile file(license_svtplay_dl);

    if(!file.exists())
        QMessageBox::warning(this, DISPLAY_NAME " " VERSION, tr("The license file is not found"));
    else {
        file.open(QIODevice::ReadOnly);
        QTextStream stream(&file);
        QString content = stream.readAll();
        file.close();
        ui->teOut->setText(content);
    }
}


void Newprg::licenseFfmpeg()
{
    ui->teOut->setReadOnly(true);
    ui->teOut->setAcceptRichText(false);
    ui->actionSaveDownloadList->setEnabled(false);
    QString license_ffmpeg(":/txt/license_ffmpeg.txt");
    QFile file(license_ffmpeg);

    if(!file.exists())
        QMessageBox::warning(this, DISPLAY_NAME " " VERSION, tr("The license file is not found"));
    else {
        file.open(QIODevice::ReadOnly);
        QTextStream stream(&file);
        QString content = stream.readAll();
        file.close();
        ui->teOut->setText(content);
    }
}


void Newprg::aboutFfmpeg()
{
    ui->teOut->setReadOnly(true);
    ui->teOut->setAcceptRichText(false);
    ui->actionSaveDownloadList->setEnabled(false);
    // Windows
#ifdef Q_OS_WIN // QT5
    const QString EXECUTE = QDir::toNativeSeparators("\"" + QCoreApplication::applicationDirPath() + "/ffmpeg.exe \"");
#endif
    // Linux
#ifdef Q_OS_LINUX
    //const QString EXECUTE = "ffmpeg";
    // v0.15.1
    const QString EXECUTE = QDir::toNativeSeparators("\"" + QCoreApplication::applicationDirPath() + "/ffmpeg\"");
#endif
    auto *prg = new QString("ffmpeg");

    if(!prgExists(prg))
        return;

    auto *CommProcess = new QProcess(this);
    CommProcess->setProcessChannelMode(QProcess::MergedChannels);
    CommProcess->start(EXECUTE + " --help");
    CommProcess->waitForFinished(-1);
    QTextStream stream(CommProcess->readAllStandardOutput());
    QString s = "";
    QString line = "";
    int i = 0;

    while(!stream.atEnd()) {
        line = stream.readLine();
        line = line.simplified();

        if(i != 0)
            s = s + "<br>" + line;
        else
            s = s + line;

        i++;
    }

    ui->teOut->setText(s);
}

void Newprg::aboutSvtplayDl()
{
#ifdef Q_OS_WIN // QT5
    const QString EXECUTE = QDir::toNativeSeparators("\"" + QCoreApplication::applicationDirPath() + "/svtplay-dl.exe \"");
#endif
    // Linux
#ifdef Q_OS_LINUX
    const QString EXECUTE = QDir::toNativeSeparators("\"" + QCoreApplication::applicationDirPath() + "/svtplay-dl\"");
#endif
    auto *prg = new QString("svtplay-dl");

    if(!prgExists(prg))
        return;

    ui->teOut->setReadOnly(true);
    ui->teOut->setAcceptRichText(false);
    ui->actionSaveDownloadList->setEnabled(false);
    auto *CommProcess = new QProcess(this);
    CommProcess->setProcessChannelMode(QProcess::MergedChannels);
    CommProcess->start(EXECUTE + " --help");
    CommProcess->waitForFinished(-1);
    QTextStream stream(CommProcess->readAllStandardOutput());
    QString s = "<b>svtplay-dl " + tr("version ") + getSvtplaydlVersion() + "</b><br><br>";
    QString line = "";
    int i = 0;

    while(!stream.atEnd()) {
        line = stream.readLine();
        line = line.simplified();

        if(i != 0)
            s = s + "<br>" + line;
        else
            s = s + line;

        i++;
    }

    ui->teOut->setText(s);
}

bool Newprg::prgExists(QString *prg)
{
#ifdef Q_OS_WIN // QT5
    *prg = *prg + ".exe";
#endif

    if(!QFile::exists(QDir::toNativeSeparators(QCoreApplication::applicationDirPath() + "/" + *prg))) {
        ui->teOut->setTextColor("red");
        ui->teOut->setText(*prg + tr(" could not be found. Check your installation. ") + *prg + tr(" is included with the ") +  DISPLAY_NAME + tr(" installation."));
        ui->teOut->setTextColor("black");
        return false;
    }

    return true;
}


QString Newprg::getSvtplaydlVersion()
{
#ifdef Q_OS_WIN // QT5
    const QString EXECUTE = QDir::toNativeSeparators("\"" + QCoreApplication::applicationDirPath() + "/svtplay-dl.exe \"");
#endif
    // Linux
#ifdef Q_OS_LINUX
    const QString EXECUTE = QDir::toNativeSeparators("\"" + QCoreApplication::applicationDirPath() + "/svtplay-dl\"");
#endif
    auto *CommProcess = new QProcess(nullptr);
    CommProcess->setProcessChannelMode(QProcess::MergedChannels);
    CommProcess->start(EXECUTE + " --version");
    CommProcess->waitForFinished(-1);
    QString line;
    QTextStream stream(CommProcess->readAllStandardOutput());

    while(!stream.atEnd()) {
        line = stream.readLine();
    }

    line = line.mid(11);
    return line;
}


