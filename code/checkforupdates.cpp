/*
    streamCapture2
    Copyright (C) 2016 - 2020 Ingemar Ceicer
    http://ceicer.org/ingemar/
    programmering1 (at) ceicer (dot) org

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
*/


#include "checkforupdates.h"

void CheckForUpdates::check(const QString &progName, const QString &currentVersion, const QString &versionPath, const QString &downloadPath)
{
    QUrl url(versionPath);
    auto *nam = new QNetworkAccessManager(nullptr);
    auto *reply = nam->get(QNetworkRequest(url));
    QObject::connect(reply, &QNetworkReply::finished, [this, reply, versionPath, progName, currentVersion, downloadPath ]() {
        QByteArray bytes = reply->readAll();  // bytes
        QString newVersion(bytes); // string
        newVersion = newVersion.trimmed();
        int index = newVersion.indexOf('\n');
        QString onlyNewVersion = newVersion.left(index);
        QString infoVersion = newVersion.mid(index);
        QString info = "";

        if(index > 0) {
            info = moreInfo(infoVersion);
        }

        if(onlyNewVersion == "")
            QMessageBox::critical(nullptr, progName + " " + currentVersion, tr("No Internet connection was found.\nPlease check your Internet settings and firewall."));
        else {
            int ver = jfrVersion(currentVersion, onlyNewVersion);

            switch(ver) {
                case 0:
                    QMessageBox::information(nullptr, progName + " " + currentVersion, tr("\nThere is a newer version of ") + progName + "<br>" + tr("Please ") + "<p><a href=\"" + downloadPath + "\">" + tr("Download") + "</a></p><p>" + tr("Latest version: ") + onlyNewVersion + info + "</p>");
                    break;

                case 1:
                    QMessageBox::information(nullptr, progName + " " + currentVersion, tr("\nYour version of ") + progName + tr(" is newer than the latest official version. ") + "(" + onlyNewVersion + ").");
                    break;

                case 2:
                    QMessageBox::information(nullptr, progName + " " + currentVersion, tr("\nYou have the latest version of ") + progName + ".");
                    break;

                default:
                    QMessageBox::information(nullptr, progName + " " + currentVersion, tr("\nThere was an error when the version was checked."));
                    break;
            }
        }
    });
}

void CheckForUpdates::checkOnStart(const QString &progName, const QString &currentVersion, const QString &versionPath, const QString &downloadPath)
{
    QUrl url(versionPath);
    auto *nam = new QNetworkAccessManager(nullptr);
    auto *reply = nam->get(QNetworkRequest(url));
    QObject::connect(reply, &QNetworkReply::finished, [this, reply, versionPath, progName, currentVersion, downloadPath ]() {
        QByteArray bytes = reply->readAll();  // bytes
        QString newVersion(bytes); // string
        newVersion = newVersion.trimmed();
        int index = newVersion.indexOf('\n');
        QString onlyNewVersion = newVersion.left(index);
        QString infoVersion = newVersion.mid(index);
        QString info = "";

        if(index > 0) {
            info = moreInfo(infoVersion);
        }

        if(onlyNewVersion == "")  {
            QMessageBox::critical(nullptr, progName + " " + currentVersion, tr("No Internet connection was found.\nPlease check your Internet settings and firewall."));
        } else {
            int ver = jfrVersion(currentVersion, onlyNewVersion);

            if(ver == 0) {
                QMessageBox::information(nullptr, progName + " " + currentVersion, tr("\nThere is a newer version of ") + progName + "<br>" + tr("Please ") + "<p><a href=\"" + downloadPath + "\">" + tr("Download") + "</a></p><p>" + tr("Latest version: ") + onlyNewVersion + info + "</p>");
            } else if(ver == 3) {
                QMessageBox::information(nullptr, progName + " " + currentVersion, tr("\nThere was an error when the version was checked."));
            }
        }
    });
}


int CheckForUpdates::jfrVersion(const QString &currentVersion, const QString &newVersion)
{
    int newVer;
    int currentVer;
    bool ok = true;
    QStringList listCurrentVersion = currentVersion.split(".");
    QStringList listNewVersion = newVersion.split(".");

    for(int i = 0; i <= 2; i++) {
        currentVer = listCurrentVersion.at(i).toInt(&ok, 10);
        newVer = listNewVersion.at(i).toInt(&ok, 10);

        if(!ok)
            return 3;

        if(newVer > currentVer)
            return 0;

        if(newVer < currentVer)
            return 1;
    }

    return 2;
}
QString CheckForUpdates::moreInfo(const QString &infoVersion)
{
    QList<QString> listInfo;
    listInfo = infoVersion.split('\n');
    QString news;

    if(!listInfo.empty()) {
        news = "<br><i>" + tr("New updates:") + "</i>";
        QList<QString>::iterator it;

        for(it = listInfo.begin() + 1; it != listInfo.end(); ++it)
            news.append("<br>" + *it);
    } else {
        news = "";
    }

    return news;
}
CheckForUpdates::CheckForUpdates()
{
    this->setAttribute(Qt::WA_DeleteOnClose, true);
}
CheckForUpdates::~CheckForUpdates()
{
    delete this;
}
