/*
    streamCapture2
    Copyright (C) 2016 - 2020 Ingemar Ceicer
    http://ceicer.org/ingemar/
    programmering1 (at) ceicer (dot) org

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
*/
#include "newprg.h"
#include "ui_newprg.h"
#include "info.h"
#include <QMessageBox>
void Newprg::editOrDelete(const QString &provider)
{
    QMessageBox msgBox;
    msgBox.setText(tr("Edit or delete\n") + provider + "?");
    QPushButton *btnEdit = msgBox.addButton(tr("Edit"), QMessageBox::ActionRole);
    QPushButton *btnDelete = msgBox.addButton(tr("Delete"), QMessageBox::ActionRole);
    QPushButton *btnCancel = msgBox.addButton(QMessageBox::Cancel);
    msgBox.exec();

    if(msgBox.clickedButton() == btnEdit) {
        editprovider(provider);
    } else if(msgBox.clickedButton() == btnDelete) {
        deleteProvider(provider);
    } else if(msgBox.clickedButton() == btnCancel) {
        return;
    }
}


void Newprg::deleteProvider(const QString &provider)
{
    ui->menuPayTV->clear();
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME, EXECUTABLE_NAME);
    settings.setIniCodec("UTF-8");
    settings.beginGroup("Provider");
    QStringList keys = settings.childGroups();
    settings.endGroup();
    int find = keys.indexOf(provider);
    keys.removeAt(find);
    QIcon icon(":/images/new.png");
    QAction *actionCreateNew = new QAction(icon, tr("Create New"), nullptr);
    ui->menuPayTV->addAction(actionCreateNew);
    connect(actionCreateNew, &QAction::triggered, [ this ]() {
        newSupplier();
    });
    QAction *actionPayTV;
    keys.sort(Qt::CaseInsensitive);
    ui->comboPayTV->clear();
    ui->comboPayTV->addItem(tr("No Password"));

    foreach(QString p, keys) {
        actionPayTV = new QAction(p, nullptr);
        ui->menuPayTV->addAction(actionPayTV);
        ui->comboPayTV->addItem(p);
        connect(actionPayTV, &QAction::triggered, [  this, actionPayTV ]() {
            editOrDelete(actionPayTV->text());
        });
    }

    settings.beginGroup("Provider");
    settings.remove(provider);
    settings.endGroup();
}

void Newprg::editprovider(QString provider)
{
    secretpassword = "";
    provider = provider.trimmed();
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME, EXECUTABLE_NAME);
    settings.setIniCodec("UTF-8");
    settings.beginGroup("Provider");
    const QString username = settings.value(provider + "/username", "").toString();
    const QString password = settings.value(provider + "/password", "").toString();
    settings.endGroup();
    bool b;

    do {
        b = testinputusername_edit(username, provider);
    } while(!b);

    do {
        b = testinputpassword_edit(password, provider);
    } while(!b);
}
// private:
bool Newprg::testinputusername_edit(const QString &username, const QString &provider)
{
    bool ok;
    QString newusername = QInputDialog::getText(nullptr, provider + " " + tr("Enter your username"),
                          tr("Spaces are not allowed. Use only the characters your streaming provider approves."), QLineEdit::Normal,
                          username, &ok);

    if(newusername.indexOf(' ') >= 0)
        return false;

    if(ok) {
        QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME, EXECUTABLE_NAME);
        settings.setIniCodec("UTF-8");
        settings.beginGroup("Provider");
        settings.setValue(provider + "/username", newusername);
        settings.endGroup();
        return true;
    }

    return true;
}
// private:
bool Newprg::testinputpassword_edit(const QString &password, const QString &provider)
{
    bool ok;
    QString newpassword = QInputDialog::getText(nullptr, provider + " " + tr("Enter your password"),
                          tr("Spaces are not allowed. Use only the characters your streaming provider approves."), QLineEdit::Password,
                          password, &ok);

    if(newpassword.indexOf(' ') >= 0)
        return false;

    if(ok) {
        QMessageBox msgBox;
        msgBox.setWindowTitle(tr("Save password?"));
        msgBox.setText(tr("Do you want to save the password? (unsafe)?"));
        msgBox.setStandardButtons(QMessageBox::Yes);
        msgBox.addButton(QMessageBox::No);
        msgBox.setDefaultButton(QMessageBox::No);
        QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME, EXECUTABLE_NAME);
        settings.setIniCodec("UTF-8");
        settings.beginGroup("Provider");

        if(msgBox.exec() == QMessageBox::Yes) {
            settings.setValue(provider + "/password", newpassword);
            secretpassword = newpassword;
        } else {
            settings.setValue(provider + "/password", "");
            secretpassword = newpassword;
        }

        settings.endGroup();
        return true;
    }

    return true;
}

