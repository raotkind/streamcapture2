/*
    streamCapture2
    Copyright (C) 2016 - 2020 Ingemar Ceicer
    http://ceicer.org/ingemar/
    programmering1 (at) ceicer (dot) org

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
*/

#ifndef CHECKFORUPDATES_H
#define CHECKFORUPDATES_H
#include <QMessageBox>
#include <QNetworkReply>

class CheckForUpdates : public QWidget
{

    Q_OBJECT

private:

    int jfrVersion(const QString &currentVersion, const QString &newVersion);
    QString moreInfo(const QString &infoVersion);

public:
    void check(const QString &progName, const QString &currentVersion, const QString &versionPath, const QString &downloadPath);

    void checkOnStart(const QString &progName, const QString &currentVersion, const QString &versionPath, const QString &downloadPath);


    ~CheckForUpdates();
    CheckForUpdates();
};

#endif // CHECKFORUPDATES_H
