/*
    streamCapture2
    Copyright (C) 2016 - 2020 Ingemar Ceicer
    http://ceicer.org/ingemar/
    programmering1 (at) ceicer (dot) org

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
*/
#include "newprg.h"
#include "info.h"
#include "ui_newprg.h"
#include "checkforupdates.h"
#include <QMessageBox>
void Newprg::setStartConfig()
{
    deleteSettings = false;
    ui->setupUi(this);
    QIcon icon(":/images/icon.ico");
    QApplication::setWindowIcon(icon);
    this->setWindowTitle(DISPLAY_NAME " " VERSION);
    ui->teOut->setReadOnly(true);
    /* Font */
    int id = QFontDatabase::addApplicationFont(":/fonts/PTSans-Regular.ttf");
    /* */
#ifdef Q_OS_LINUX     // Linux
    QStringList allaFonter {
        "Merriweather-BlackItalic.ttf",
        "Merriweather-Black.ttf",
        "Merriweather-BoldItalic.ttf",
        "Merriweather-Bold.ttf",
        "Merriweather-Italic.ttf",
        "Merriweather-LightItalic.ttf",
        "Merriweather-Light.ttf",
        "Merriweather-Regular.ttf",
        "MerriweatherSans-BoldItalic.ttf",
        "MerriweatherSans-Bold.ttf",
        "MerriweatherSans-ExtraBoldItalic.ttf",
        "MerriweatherSans-ExtraBold.ttf",
        "MerriweatherSans-Italic.ttf",
        "MerriweatherSans-LightItalic.ttf",
        "MerriweatherSans-Light.ttf",
        "MerriweatherSans-Regular.ttf",
        "PTMono-Regular.ttf",
        "PTSansCaption-Bold.ttf",
        "PTSansCaption-Regular.ttf",
        "PTSansNarrow-Bold.ttf",
        "PTSansNarrow-Regular.ttf",
        "PTSans-Regular.ttf",
        "PTSerif-BoldItalic.ttf",
        "PTSerif-Bold.ttf",
        "PTSerif-Italic.ttf",
        "PTSerif-Regular.ttf",
        "Ubuntu-BI.ttf",
        "Ubuntu-B.ttf",
        "Ubuntu-C.ttf",
        "Ubuntu-LI.ttf",
        "Ubuntu-L.ttf",
        "Ubuntu-MI.ttf",
        "UbuntuMono-BI.ttf",
        "UbuntuMono-B.ttf",
        "UbuntuMono-RI.ttf",
        "UbuntuMono-R.ttf",
        "Ubuntu-M.ttf",
        "Ubuntu-RI.ttf",
        "Ubuntu-R.ttf"
    };

    foreach(QString s, allaFonter) {
        QFontDatabase::addApplicationFont(":/fonts/" + s);
    }

#endif
    /*  */
    QString familj = QFontDatabase::applicationFontFamilies(id).at(0);
    QFont f(familj);
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME, EXECUTABLE_NAME);
    settings.setIniCodec("UTF-8");
    settings.beginGroup("Font");
    QString family = settings.value("family", f.family()).toString();
    int size = this->font().pointSize();
    // int size = settings.value("size", f.pointSize()).toInt();
    bool bold = settings.value("bold", false).toBool();
    bool italic = settings.value("italic", false).toBool();
    bool boldItalic = settings.value("boldItalic", false).toBool();
    settings.endGroup();
    QFont f2(family, size);

    if(boldItalic) {
        f2.setBold(true);
        f2.setItalic(true);
//        qInfo() << "Bold and Italic";
    } else {
        if(bold) {
            f2.setBold(true);
//            qInfo() << "Bold";
        } else if(italic) {
            f2.setItalic(true);
            qInfo() << "Italic";
        } else  {
            f2.setBold(false);
            f2.setItalic(false);
//            qInfo() << "Normal";
        }
    }

    ui->teOut->setFont(f2);
    /* End Font */
    /* COLOR */
    QColor c(RED, GREEN, BLUE);
    QPalette p = ui->teOut->palette(); // define pallete for textEdit..
    p.setColor(QPalette::Base, c); // set color "c" for textedit base
    ui->teOut->setPalette(p); // change textedit palette
#ifdef Q_OS_LINUX
    ui->actionStopAllDownloads->setVisible(false);
#endif
    processpid = 0;
    avbrutet = false;
    /* */
    QFileInfo fi(settings.fileName());
    QString settings_location = fi.canonicalPath();
    settings.beginGroup("Settings");
    int ma = settings.value("maxantal", 10).toInt();
    bool createfolder = settings.value("createfolder", false).toBool();
    bool showmore = settings.value("showmore", false).toBool();
    settings.endGroup();
    settings.beginGroup("Path");
    bool copytodefaultlocation = settings.value("copytodefaultlocation", false).toBool();
    bool downloadtodefaultlocation = settings.value("downloadtodefaultlocation", false).toBool();
    settings.endGroup();

    if(createfolder) {
        ui->actionCreateFolder->setChecked(true);
    } else {
        ui->actionCreateFolder->setChecked(false);
    }

    if(showmore) {
        ui->actionShowMore->setChecked(true);
    } else {
        ui->actionShowMore->setChecked(false);
    }

    if(copytodefaultlocation) {
        ui->actionCopyToDefaultLocation->setChecked(true);
    } else {
        ui->actionCopyToDefaultLocation->setChecked(false);
    }

    if(downloadtodefaultlocation) {
        ui->actionDownloadToDefaultLocation->setChecked(true);
    } else {
        ui->actionDownloadToDefaultLocation->setChecked(false);
    }

    MAX_RECENT = ma;
    settings.beginGroup("Update");
    const bool checkonstart = settings.value("checkonstart", true).toBool();
    settings.endGroup();

    if(checkonstart) {
        ui->actionCheckOnStart->setChecked(true);
//        settings.beginGroup("Language");
//        const QString sp = settings.value("language", "en_US").toString();
//        settings.endGroup();
        auto *cfu = new CheckForUpdates;
        cfu->checkOnStart(DISPLAY_NAME, VERSION, VERSION_PATH, DOWNLOAD_PATH);
    } else {
        ui->actionCheckOnStart->setChecked(false);
    }

    ui->statusBar->showMessage(tr("Up and running"));
    settings.beginGroup("Settings");
    const bool subtitels = settings.value("subtitels", false).toBool();
    settings.endGroup();

    if(subtitels) {
        ui->chbSubtitle->setChecked(true);
        ui->actionSubtitle->setChecked(true);
    } else {
        ui->chbSubtitle->setChecked(false);
        ui->actionSubtitle->setChecked(false);
    }

    const QString RECENT_FILE_FILE = settings_location + "/recentfiles.list";
    QFile file(RECENT_FILE_FILE);

    if(file.open(QIODevice::ReadOnly | QIODevice::Text)) {
        QTextStream in(&file);

        while(!in.atEnd())
            recentFiles.append(in.readLine());
    }

    file.close();
    /*  */
    const QString DOWNLOADLIST = settings_location + "/downloads.list";
    QFile file2(DOWNLOADLIST);

    if(file2.open(QIODevice::ReadOnly | QIODevice::Text)) {
        QTextStream in(&file2);

        while(!in.atEnd())
            downloadList.append(in.readLine());

        if(!downloadList.empty()) {
            ui->pbDownloadAll->setEnabled(true);
            ui->actionDownloadAll->setEnabled(true);
        }
    }

    file2.close();
    settings.beginGroup("MainWindow");
    this->resize(settings.value("size", QSize(700, 300)).toSize());
    this->move(settings.value("pos", QPoint(200, 200)).toPoint());
    settings.endGroup();
    settings.beginGroup("Provider");
    QStringList keys = settings.childGroups();
    settings.endGroup();
    ui->comboPayTV->addItem("- " + tr("No Password"));
    keys.sort(Qt::CaseInsensitive);

    foreach(QString p, keys) {
        QAction *actionPayTV;
        actionPayTV = new QAction(p, nullptr);
        ui->comboPayTV->addItem(p);
        ui->menuPayTV->addAction(actionPayTV);
        connect(actionPayTV, &QAction::triggered, [  this, actionPayTV ]() {
            editOrDelete(actionPayTV->text());
        });
    }

    ui->pbPassword->setDisabled(true);
    ui->actionPassword->setDisabled(true);
    // Windows
#ifdef Q_OS_WIN // QT5
    const QString UNINSTALL = QDir::toNativeSeparators(QCoreApplication::applicationDirPath() + "/uninstall.exe");

    if(QFile::exists(UNINSTALL)) {
        QAction *actionUninstallRemoveSettings;
        actionUninstallRemoveSettings = new QAction(tr("Uninstall ") + DISPLAY_NAME + tr(" and Delete all Settings"), nullptr);
        actionUninstallRemoveSettings->setIcon(QIcon(":/images/uninstall.png"));
        actionUninstallRemoveSettings->setStatusTip(tr("Uninstall and completely remove all components. All saved searches and the list of streams to download will be deleted."));
        ui->menuHelp->addAction(actionUninstallRemoveSettings);
        //
        connect(actionUninstallRemoveSettings, &QAction::triggered, [ this, UNINSTALL ]() {
            switch(QMessageBox::critical(this, DISPLAY_NAME " " VERSION,
                                         tr("Do you want to uninstall ") + DISPLAY_NAME "?",
                                         tr("Uninstall Now"),
                                         tr("Cancel"), nullptr, 2)) {
                case 0:
                    //  const QString MAINTENANCETOOL = QDir::toNativeSeparators("\"" + QCoreApplication::applicationDirPath() + "/uninstall.exe\"");
                    deleteAllSettings();
                    QProcess *process = new QProcess(this);

//                    QProcessEnvironment env = QProcessEnvironment::systemEnvironment();
//                    const QString path = QDir::toNativeSeparators(QCoreApplication::applicationDirPath());
//                    const QString oldpath = env.value("PATH");
//                    env.insert("PATH", path + ";" + oldpath); // Add an environment variable
//                    process->setProcessEnvironment(env);

                    // process->startDetached("streamcapture2.exe");
                    // process->execute("cmd uninstall.exe");
                    if(!process->startDetached("\"" + UNINSTALL + "\"")) {
                        QMessageBox::critical(this, DISPLAY_NAME " " VERSION, tr("Uninstaller not found."));
                    } else
                        close();
            }
        });
        QAction *actionUninstall;
        actionUninstall = new QAction(tr("Uninstall ") + DISPLAY_NAME, nullptr);
        actionUninstall->setIcon(QIcon(":/images/uninstall.png"));
        actionUninstall->setStatusTip(tr("Uninstall and completely remove all components."));
        ui->menuHelp->addAction(actionUninstall);
        //
        connect(actionUninstall, &QAction::triggered, [ this, UNINSTALL ]() {
            switch(QMessageBox::critical(this, DISPLAY_NAME " " VERSION,
                                         tr("Do you want to uninstall ") + DISPLAY_NAME "?",
                                         tr("Uninstall Now"),
                                         tr("Cancel"), nullptr, 2)) {
                case 0:
                    //  const QString MAINTENANCETOOL = QDir::toNativeSeparators("\"" + QCoreApplication::applicationDirPath() + "/uninstall.exe\"");
                    QProcess *process = new QProcess(this);

                    if(!process->startDetached("\"" + UNINSTALL + "\"")) {
                        QMessageBox::critical(this, DISPLAY_NAME " " VERSION, tr("Uninstaller not found."));
                    } else
                        close();
            }
        });
    }

#endif
// END WINDOWS
}
void Newprg::closeEvent(QCloseEvent * event)
{
//    if(!deleteSettings)
//        setEndConfig();
    event->accept();
}
void Newprg::setEndConfig()
{
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME, EXECUTABLE_NAME);
    settings.setIniCodec("UTF-8");
    QFileInfo fi(settings.fileName());
    QString settings_location = fi.canonicalPath();
    settings.beginGroup("Settings");
    settings.setValue("maxantal", MAX_RECENT);
    settings.endGroup();
    settings.beginGroup("Settings");

    if(ui->chbSubtitle->isChecked())
        settings.setValue("subtitels", true);
    else
        settings.setValue("subtitels", false);

    settings.endGroup();
    settings.beginGroup("MainWindow");
    settings.setValue("size", this->size());
    settings.setValue("pos", this->pos());
    settings.endGroup();
    recentFiles.removeDuplicates();
    const QString RECENT_FILE_FILE = settings_location + "/recentfiles.list";
    QFile file(RECENT_FILE_FILE);

    if(!file.open(QIODevice::WriteOnly | QIODevice::Text))
        QMessageBox::critical(this, DISPLAY_NAME " " VERSION, tr("Could not save a file to store Recent Search list. Check your file permissions."));

    if(!recentFiles.empty()) {
        recentFiles.removeDuplicates();

        for(int i = 0; i < recentFiles.size(); ++i) {
            QTextStream out(&file);
            out << recentFiles.at(i) << "\n";
        }
    }

    file.close();
    const QString DOWNLOADLIST = settings_location + "/downloads.list";
    QFile file2(DOWNLOADLIST);

    if(!file2.open(QIODevice::WriteOnly | QIODevice::Text))
        QMessageBox::critical(this, DISPLAY_NAME " " VERSION, tr("Could not save a file to store the list off downloads. Check your file permissions."));

    if(!downloadList.empty()) {
        downloadList.removeDuplicates();

        for(int i = 0; i < downloadList.size(); ++i) {
            QTextStream out(&file2);
            out << downloadList.at(i) << "\n";
        }
    }

    file2.close();
}
