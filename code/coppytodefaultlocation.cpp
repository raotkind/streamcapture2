/*
    streamCapture2
    Copyright (C) 2016 - 2020 Ingemar Ceicer
    http://ceicer.org/ingemar/
    programmering1 (at) ceicer (dot) org

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
*/
#include "newprg.h"
#include "ui_newprg.h"
#include "info.h"
//#include <QMessageBox>

void Newprg::copyToDefaultLocation(QString filnamn, const QString *folderaddress)
{
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME, EXECUTABLE_NAME);
    settings.setIniCodec("UTF-8");
    settings.beginGroup("Path");
    QString defaultdownloadlocation = settings.value("defaultdownloadlocation", QDir::homePath()).toString();
    QString savepath = settings.value("savepath").toString();
    QString copypath = settings.value("copypath").toString();
    settings.endGroup();
    QString copytoFolder = "";
    QString preferred, quality;

    if(ui->actionCreateFolder->isChecked()) {
        preferred = ui->leMethod->text();
        preferred = preferred.toLower();
        quality = ui->leQuality->text();
        copytoFolder = QDir::toNativeSeparators(copypath + folderaddress  + "/" + preferred + "_" + quality);
        QDir dir(copytoFolder);

        if(!dir.exists())
            dir.mkpath(".");
    }

    QString from;

    if(ui->actionDownloadToDefaultLocation->isChecked()) {
        from = defaultdownloadlocation;
    } else {
        from = savepath;
    }

    if(copypath.isEmpty()) {
        QMessageBox::critical(this, DISPLAY_NAME " " VERSION, tr("You have not selected any place to copy the media files.\nPlease select a location before proceeding."));
        return;
    }

    if(ui->actionCreateFolder->isChecked()) {
        from = from + folderaddress + "/" + preferred + "_" + quality;
        copypath = copytoFolder;
    }

    QString old = QDir::toNativeSeparators(from + "/" + filnamn);
    QString ny = QDir::toNativeSeparators(copypath + "/" + filnamn);

    if(QFile::exists(ny)) {
        QFile::remove(ny);
    }

    if(QFile::copy(old, ny)) {
        ui->teOut->setTextColor(QColor("darkBlue"));
        ui->teOut->append(tr("Copy succeeded ") + "(" + filnamn + ")");
        ui->teOut->setTextColor(QColor("black"));
    } else {
        ui->teOut->setTextColor(QColor("red"));
        ui->teOut->append(tr("Copy failed ") + "(" + filnamn + ")");
        ui->teOut->setTextColor(QColor("black"));
    }

    int langd = old.size();
    QString undertexten = old.replace(langd - 3, 3, "srt");
    /* */
    int langd2 = filnamn.size();
    QString undertextfilen = filnamn.replace(langd2 - 3, 3, "srt");

    if(QFile::exists(undertexten)) {
        langd = ny.size();
        QString ny_undertexten = ny.replace(langd - 3, 3, "srt");

        if(QFile::exists(ny_undertexten)) {
            QFile::remove(ny_undertexten);
        }

        if(QFile::copy(undertexten, ny_undertexten)) {
            ui->teOut->setTextColor(QColor("darkBlue"));
            ui->teOut->append(tr("Copy succeeded ") + "(" + undertextfilen + ")");
            ui->teOut->setTextColor(QColor("black"));
        } else {
            ui->teOut->setTextColor(QColor("red"));
            ui->teOut->append(tr("Copy failed ") + "(" + undertextfilen + ")");
            ui->teOut->setTextColor(QColor("black"));
        }
    }
}


void Newprg::copyToDefaultLocation(QString filnamn, const QString *folderaddress, const QString *preferredQuality)
{
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME, EXECUTABLE_NAME);
    settings.setIniCodec("UTF-8");
    settings.beginGroup("Path");
    QString defaultdownloadlocation = settings.value("defaultdownloadlocation", QDir::homePath()).toString();
    QString savepath = settings.value("savepath").toString();
    QString copypath = settings.value("copypath").toString();
    settings.endGroup();
    QString copytoFolder = "";

    if(*preferredQuality != "non") {
        copytoFolder = QDir::toNativeSeparators(copypath + folderaddress  + "/" + preferredQuality);
        copypath = copytoFolder;
        QDir dir(copytoFolder);

        if(!dir.exists())
            dir.mkpath(".");
    }

    QString from;

    if(ui->actionDownloadToDefaultLocation->isChecked()) {
        from = defaultdownloadlocation + "/" + folderaddress  + "/" + preferredQuality;
    } else {
        from = savepath + "/" + folderaddress  + "/" + preferredQuality;
    }

    if(copypath.isEmpty()) {
        QMessageBox::critical(this, DISPLAY_NAME " " VERSION, tr("You have not selected any place to copy the media files.\nPlease select a location before proceeding."));
        return;
    }

    QString old = QDir::toNativeSeparators(from + "/" + filnamn);
    QString ny = QDir::toNativeSeparators(copypath + "/" + filnamn);

    if(QFile::copy(old, ny)) {
        ui->teOut->setTextColor(QColor("darkBlue"));
        ui->teOut->append(tr("Copy succeeded ") + "(" + filnamn + ")");
        ui->teOut->setTextColor(QColor("black"));
    } else {
        ui->teOut->setTextColor(QColor("red"));
        ui->teOut->append(tr("Copy failed ") + "(" + filnamn + ")");
        ui->teOut->setTextColor(QColor("black"));
    }

    int langd = old.size();
    QString undertexten = old.replace(langd - 3, 3, "srt");
    /* */
    int langd2 = filnamn.size();
    QString undertextfilen = filnamn.replace(langd2 - 3, 3, "srt");

    if(QFile::exists(undertexten)) {
        langd = ny.size();
        QString ny_undertexten = ny.replace(langd - 3, 3, "srt");

        if(QFile::copy(undertexten, ny_undertexten)) {
            ui->teOut->setTextColor(QColor("darkBlue"));
            ui->teOut->append(tr("Copy succeeded ") + "(" + undertextfilen + ")");
            ui->teOut->setTextColor(QColor("black"));
        } else {
            ui->teOut->setTextColor(QColor("red"));
            ui->teOut->append(tr("Copy failed ") + "(" + undertextfilen + ")");
            ui->teOut->setTextColor(QColor("black"));
        }
    }
}
