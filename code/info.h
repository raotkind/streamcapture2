/*
    streamCapture2
    Copyright (C) 2016 - 2020 Ingemar Ceicer
    http://ceicer.org/ingemar/
    programmering1 (at) ceicer (dot) org

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
*/
#ifndef INFO_H
#define INFO_H

#include <QWidget>

#include <string>
#include <sstream>

#define VERSION_PATH "http://bin.ceicer.com/streamcapture2/version2.txt"
#define DOWNLOAD_PATH "https://gitlab.com/posktomten/streamcapture2/-/wikis/DOWNLOADS"
#define DISPLAY_NAME "streamCapture2"
#define EXECUTABLE_NAME "streamcapture2"
#define VERSION "0.18.6"
#define BUILD_DATE_TIME __DATE__ " " __TIME__
#define START_YEAR "2016"
#define CURRENT_YEAR __DATE__
#define LICENCE_VERSION "3"
#define PROGRAMMER_EMAIL "programmering1@ceicer.org"
#define PROGRAMMER_NAME "Ingemar Ceicer"
#define PROGRAMMER_PHONE "+46706361747"
#define APPLICATION_HOMEPAGE "https://gitlab.com/posktomten/streamcapture2/wikis/Home"
#define APPLICATION_HOMEPAGE_ENG "https://gitlab.com/posktomten/streamcapture2/wikis/Home"
#define SOURCEKODE "https://gitlab.com/posktomten/streamcapture2"
#define WIKI "https://gitlab.com/posktomten/streamcapture2/wikis/Home"
#define FFMPEG_HOMEPAGE  "https://www.ffmpeg.org/";
#define SVTPLAYDL_HOMEPAGE "https://svtplay-dl.se/";
#define SIZE_OF_RECENT 50
#define RED 218
#define GREEN 255
#define BLUE 221

#define LINUX_FONT 13
#define WINDOWS_FONT 11


class Info : public QWidget
{
    Q_OBJECT
public:


    void getSystem();

};

#endif // INFO_H
