16:53 1 May 2020
Version 0.18.6
Windowsversionen reducerad i storlek.
Mindre Bugfix.
Uses Qt 5.14.2, compiled with MinGW GCC 8.1.0 (Windows).
Uses Qt 5.14.2, compiled with GCC 5.4.0 (Linux).

20:27 27 April 2020
Version 0.18.5
Bugfix: Genvägen på startmenyn fungerade inte i
version 0.18.4 (Windows).
Länk till svtplay-dl forum för problem.
Valbart teckensnitt.
Bättre kontrasterande färger mellan teckensnitt och bakgrund.
Mindre förbättringar.
Använder Qt 5.14.2, kompilerad med MinGW GCC 7.3.0 (Windows).
Använder Qt 5.14.2, kompilerad med GCC 5.4.0 (Linux)

12:42 9 April 2020
Version 0.18.4
Uppdaterat teckensnitt.
Uppdaterad översättning.
Ny bakgrundsfärg.
Använder Qt 5.14.2, kompilerad med MinGW GCC 7.3.0 (Windows).
Använder Qt 5.14.2, kompilerad med GCC 5.4.0 (Linux)

01:09 3 April 2020
Version 0.18.3
Uppdaterad svtplay-dl version 2.4-35-g81cb18f
Använder Qt 5.14.2, kompilerad med MinGW GCC 7.3.0 (Windows).
Använder Qt 5.14.2, kompilerad med GCC 5.4.0 (Linux)

13:49 2 Februari 2020
Version 0.18.2
Uppdaterad svtplay-dl version 2.4-31-g25d3105
Uppdaterad FFmpeg version 4.2.2
Använder Qt 5.14.0, kompilerad med MinGW GCC 7.3.0 (Windows).
Använder Qt 5.14.0, kompilerad med GCC 5.4.0 (Linux)

12:35 31 December 2019
Version 0.18.1
Kontrollera att svtplay-dl och ffmpeg finns med och hittas.
Ingen uppgift om svtplay-dl-versionen i dialogrutan "Om".
Använder Qt 5.14.0, kompilerad med MinGW GCC 7.3.0 (Windows).
Använder Qt 5.14.0, kompilerad med GCC 5.4.0 (Linux).

16:23 20 December 2019
Version 0.18.0
Efter "Lista alla avsnitt", välj och ladda ner valda avsnitt.
Använder Qt 5.14.0, kompilerad med MinGW GCC 7.3.0 (Windows).
Använder Qt 5.14.0, kompilerad med GCC 5.4.0 (Linux).

22:55 25 November 2019
Version 0.17.0
Förbättrad visning av sökresultat.
Möjlighet att använda en förvald folder för nedladdning.
Möjlighet att använda en förvald folder dit mediafilerna och undertexterna kopieras.
Möjlighet att söka efter och lista alla episoder i en serie.
Möjlighet att ladda ner alla episoder på en gång.
Möjligt att avbryta pågående nedladdning. (Windows)
Möjlighet att radera alla inställningsfiler.
Förbättrad hantering av undertexter.
Många buggfixar.
Rättat felstavningar.
"Avinstalleraren" borttagen från Windows, Portable version.
Använder Qt 5.13.2, kompilerad med MinGW GCC 7.3.0 (Windows).
Använder Qt 5.13.2, kompilerad med GCC 5.4.0 (Linux).

11:01 20 October 2019
Version 0.16.0
Statisk FFmpeg version 4.2.1 (Linux and Windows).
Uppdaterad svtplay-dl version 2.4-29-gc59a305 (Linux and Windows).
Tydligare meddelande från svtplay-dl om sökresultatet.
Mindre buggfixar.
Använder Qt 5.13.1, kompilerad med MinGW GCC 7.3.0 (Windows).
Använder Qt 5.13.1, kompilerad med GCC 5.4.0 (Linux).

21:49 11 Oktober 2019
Version 0.15.0
Bugg: "/" sist i adressen tas nu bort.
Menyval för att avinstallera (Windows).
Uppdaterad svtplay-dl version 2.4-26-g5dcc899 (Linux and Windows).
Använder Qt 5.13.1, kompilerad med MinGW GCC 7.3.0 (Windows).
Använder Qt 5.13.1, kompilerad med GCC 5.4.0 (Linux).

21:24 16 September 2019
Version 0.14.0
Frågetecken i sökadressen tas bort.
Använder Qt 5.13.1, kompilerad med MinGW GCC 7.3.0 (Windows).
Använder Qt 5.13.1, kompilerad med GCC 5.4.0 (Linux).

19:53 16 September 2019
Version 0.13.9
Uppdaterad svtplay-dl version 2.4-20-g0826b8f (Linux and Windows).
Använder Qt 5.13.1, kompilerad med MinGW GCC 7.3.0 (Windows).
Använder Qt 5.13.1, kompilerad med GCC 5.4.0 (Linux).

23:24 13 September 2019
Version 0.13.8
Uppdaterad svtplay-dl version 2.4-6-gce9bfd3 (Linux and Windows).
Använder Qt 5.13.1, kompilerad med MinGW GCC 7.3.0 (Windows).
Använder Qt 5.13.1, kompilerad med GCC 5.4.0 (Linux).

21:59 11 September 2019
Version 0.13.7
Uppdaterad svtplay-dl version 2.4-2-g5466853 (Linux and Windows).
Använder Qt 5.13.1, kompilerad med MinGW GCC 7.3.0 (Windows).
Använder Qt 5.13.1, kompilerad med GCC 5.4.0 (Linux).

09:19 7 September 2019
Version 0.13.6
Uppdaterad svtplay-dl version 2.3-23-gbc15c69 (Linux och Windows).
Uppdaterad ffmpeg version 4.2 (Windows).
Använder Qt 5.13.1, kompilerad med MinGW GCC 7.3.0 (Windows).
Använder Qt 5.13.1, kompilerad med GCC 5.4.0 (Linux).
22:17 2 September 2019

Version 0.13.5
Uppdaterad svtplay-dl version 2.2-5-gd1904b2 (Linux).
Uppdaterad svtplay-dl version 2.2-7-g62cbf8a (Windows).
Använder Qt 5.13.0, kompilerad med MinGW GCC 7.3.0 (Windows).
Använder Qt 5.13.0, kompilerad med GCC 5.4.0 (Linux).

00:06 1 September 2019
Version 0.13.4
Uppdaterad svtplay-dl version 2.2-2-g838b3ec (Linux).
Uppdaterad svtplay-dl version 2.2-14-gc77a4a5 (Windows).
Mindre bugg. Visar inte "Söker..." efter misslyckad sökning.
Använder Qt 5.13.0, kompilerad med MinGW GCC 7.3.0 (Windows).
Använder Qt 5.13.0, kompilerad med GCC 5.4.0 (Linux).

18:03 28 Augusti 2019
Version 0.13.3
Uppdaterad svtplay-dl (version 2.2-1-g9146d0d, Linux och Windows).
Nedladdningslänk pekar till gitlab.
Använder Qt 5.13.0, kompilerad med MinGW GCC 7.3.0 (Windows).
Använder Qt 5.13.0, kompilerad med GCC 5.4.0 (Linux).

09:48 24 Augusti 2019
Version 0.13.2
Uppdaterad svtplay-dl (version 2.1-65-gb64dbf3, Linux och Windows).
Använder Qt 5.13.0, kompilerad med MinGW GCC 7.3.0 (Windows).
Använder Qt 5.13.0, kompilerad med GCC 5.4.0 (Linux).

17:43 21 Augusti 2019
Version 0.13.1
Bug fix, Nu fungerar: "Kontrollera automatiskt efter uppdateringar när programmet startar"
Uppdaterad svtplay-dl (version 2.1-57-gf429cfc, Linux och Windows).
Använder Qt 5.13.0, kompilerad med MinGW GCC 7.3.0 (Windows).
Använder Qt 5.13.0, kompilerad med GCC 5.4.0 (Linux).

17:59 17 Augusti 2019
Version 0.13.0
Information om vad som uppdateras när programmet söker efter uppdateringar.
Förbättrad "Om".
Flyttade "Lösenord" från "Arkiv" -menyn till "Betal-TV".
Bättre statusmeddelanden.
Bekräftelse på att videoströmmen har laddats ner
Visa antal nedladdade strömmar.
Operativsystemberoende visning av sökväg.
Mindre bug fix.
Architecture instruction set 64-bit.
Använder Qt 5.13.0, kompilerad med MinGW GCC 7.3.0 (Windows).
Använder Qt 5.13.0, kompilerad med GCC 5.4.0 (Linux).

12:07 6 Augusti 2019
Version 0.12.0
Uppdaterad svtplay-dl (version 2.1-56-gbe6005e, Windows).
Nedgradderad ffmpeg (Windows, version 4.1.4, stabil).
Skapa automatiska mappar, om det behövs. (Samma videoström i olika kvaliteter).
Möjlighet att se mer information från svtplay-dl.
Lägg till status-tips text.
Arkitekturens instruktionsuppsättning 64-bit.
Använder Qt 5.13.0, kompilerad med MinGW GCC 7.3.0 (Windows).
Använder Qt 5.13.0, kompilerad med GCC 5.4.0 (Linux).

16:32 30 juli 2019
Version 0.11.0
Arkitekturens instruktionsuppsättning 64-bit.
Använder Qt 5.13.0, kompilerad med  MinGW GCC 7.3.0 (Windows).
Använder Qt 5.13.0, kompilerad med  GCC 5.4.0 (Linux).

22:41 30 juli 2019
Version 0.11.0 RC 1
Förbättrad hjälp och manual.
Redigerbar Download list.
Möjlighet att spara samma stream  i olika storlekar.
Möjlighet att ange användarnamn och lösenord. För betalda streamingtjänster.
Ersätter gammal kod med modernare.
Använder QSettings.
Många bugfixar.
Arkitekturens instruktionsuppsättning 64-bit.
Använder Qt 5.13.0, kompilerad med  MinGW GCC 7.3.0 (Windows).
Använder Qt 5.13.0, kompilerad med  GCC 5.4.0 (Linux).

14:39 5 juli 2019
Version 0.10.7
Flera buggfixar. Fungerar bättre att ladda ner en enskild fil.
Dublikat i valet mellan olika videokvaliteer borttagna.
Använder Qt 5.13.0, kompilerad med MinGW GCC 7.3.0 (Windows).
Använder Qt 5.13.0, kompilerad med GCC 5.4.0 (Linux).

14:59 29 juni 2019
Version 0.10.5
Uppdaterad svtplay-dl (version 2.1-53-gd33186e).
Uppdaterad ffmpeg (version N-94129-g098ab93257).
Arkitekturens instruktionsuppsättning: 64-bit.
Använder Qt 5.13.0, kompilerad med MinGW GCC 7.3.0 (Windows).
Använder Qt 5.13.0, kompilerad med GCC 5.4.0 (Linux).

21:29 13 februari 2018
Version 0.10.3
Uppdaterad svtplay-dl (version 1.9.7).
Arkitekturens instruktionsuppsättning: 64-bit.
Använder Qt 5.10.0, kompilerad med MSVC++ 15.5 (MSVS 2017).

15:41 9 augusti 2017
Version 0.10.1
Kryssruta för att söka efter, ladda och
infoga undertexter i videofilen.
Uppdaterad ffmpeg (version 3.3.3).
Dynamiskt användargränssnitt.
Arkitekturens instruktionsuppsättning: 64-bit.
Använder Qt 5.9.1, kompilerad med MSVC++ 15.0 (MSVS 2017).

01:19 26 juli 2017
Version 0.10.0
Combo Box för att välja kvalitet.
Arkitekturens instruktionsuppsättning: 64-bit.
Använder Qt 5.9.1, kompilerad med MSVC++ 15.0 (MSVS 2017).


15:48 16 juli 2017
Version 0.9.4
Uppdaterad ffmpeg (version 3.3.2).
Arkitekturens instruktionsuppsättning: 64-bit.
Använder Qt 5.9.1, kompilerad med MSVC++ 15.0 (MSVS 2017).

09:38 7 maj 2017
Version 0.9.3
Uppdaterad svtplay-dl (version 1.9.4).
Uppdaterad ffmpeg (version 3.2.4).
Visar versionshistoriken.

16:33 27 january 2017
Version 0.9.2
Uppdaterad svtplay-dl (version 1.9.1).

18:00 14 januari 2017
Version 0.9.1
Uppdaterade svtplay-dl och ffmpeg
Tysk översättning.
Mindre förbättringar.

01:55 30 oktober 2016
Release version 0.9.0
