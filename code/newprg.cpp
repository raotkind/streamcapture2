/*
    streamCapture2
    Copyright (C) 2016 - 2020 Ingemar Ceicer
    http://ceicer.org/ingemar/
    programmering1 (at) ceicer (dot) org

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
*/
#include "newprg.h"
#include "ui_newprg.h"
#include "info.h"
#include "checkforupdates.h"

#include <QMessageBox>
Newprg::Newprg(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::newprg)
{
    /* QSysInfo Linux Ubuntu 18.04 */
    /*
    qDebug() << QSysInfo::buildAbi();  // "x86_64-little_endian-lp64"
    qDebug() << QSysInfo::buildCpuArchitecture(); // "x86_64"
    qDebug() << QSysInfo::currentCpuArchitecture(); // "x86_64"
    qDebug() << QSysInfo::kernelType(); // "linux"
    qDebug() << QSysInfo::kernelVersion();  // "4.15.0-55-generic"
    qDebug() << QSysInfo::machineHosame();  // "ZBOOK"
    qDebug() << QSysInfo::machineUniqueId();  // "5554ad022a4c4f5bbc4b02695c5e8bad"
    qDebug() << QSysInfo::prettyProductName();  // "Ubuntu 18.04.2 LTS"
    qDebug() << QSysInfo::productType(); // "ubuntu"
    qDebug() << QSysInfo::productVersion(); // "18.04"
    */
    /* QSysInfo Windows 10 */
    /*
    qDebug() << QSysInfo::buildAbi();  // "x86_64-little_endian-llp64"
    qDebug() << QSysInfo::buildCpuArchitecture(); // "x86_64"
    qDebug() << QSysInfo::currentCpuArchitecture(); // "x86_64"
    qDebug() << QSysIdoTestSearchnfo::kernelType(); // "winnt"
    qDebug() << QSysInfo::kernelVersion();  // "10.0.18362"
    qDebug() << QSysInfo::machineHostName();  // "ZBOOK"
    qDebug() << QSysInfo::machineUniqueId();  // "f5362b9a-dcbd-4425-9040-7dcdc6cc1da5"
    qDebug() << QSysInfo::prettyProductName();  // "Windows 10 (10.0)"
    qDebug() << QSysInfo::productType(); // "windows"
    qDebug() << QSysInfo::productVersion(); // "10"
    */
    setStartConfig();
    /*  */
    /*  */
    // File
    connect(ui->actionSelectFont, &QAction::hovered, [this]() {
        QFont f = ui->teOut->currentFont();
        QString sf = f.family();
        int size = f.pointSize();
        bool bold = f.bold();
        bool italic = f.italic();
        QString stil = " , " + tr("normal");

        if(bold && italic) {
            stil = " , " + tr("bold and italic");
        } else {
            if(bold)
                stil = ", " + tr("bold");
            else if(italic)
                stil = " , " + tr("italic");
        }

        ui->actionSelectFont->setStatusTip(tr("Current font:") + " \"" + sf + "\", " + tr("size:") + " " + QString::number(size) + stil);
    });
    connect(ui->pbPast, &QPushButton::clicked, [ this ]() {
        ui->teOut->setReadOnly(true);
        ui->actionSaveDownloadList->setEnabled(false);
        QClipboard *clipboard = QApplication::clipboard();
        QString originalText = clipboard->text();
        ui->leSok->setText(originalText);
    });
    connect(ui->actionPast, &QAction::triggered, [ this ]() {
        ui->teOut->setReadOnly(true);
        ui->actionSaveDownloadList->setEnabled(false);
        QClipboard *clipboard = QApplication::clipboard();
        QString originalText = clipboard->text();
        ui->leSok->setText(originalText);
    });
    connect(ui->actionExit, &QAction::triggered, [ this ]() {
        close();
    });
    // Recent
    connect(ui->menuRecent, SIGNAL(aboutToShow()), this, SLOT(slotRecent())); // newprg.cpp
    // Download List
    connect(ui->actionViewDownloadList, &QAction::triggered, [ this ]() {
        ui->teOut->setTextColor("black");
        ui->teOut->setReadOnly(true);
        ui->actionSaveDownloadList->setEnabled(false);
        downloadList.removeDuplicates();
        ui->teOut->clear();

        for(int i = 0; i < downloadList.size(); i++)
            ui->teOut->append(downloadList.at(i));
    });
    connect(ui->actionEditDownloadList, &QAction::triggered, [ this ]() {
        downloadList.removeDuplicates();
        ui->teOut->clear();

        for(int i = 0; i < downloadList.size(); i++)
            ui->teOut->append(downloadList.at(i));

        ui->teOut->setReadOnly(false);
        ui->actionSaveDownloadList->setEnabled(true);
    });
    connect(ui->actionSaveDownloadList, &QAction::triggered, [ this ]() {
        ui->teOut->setReadOnly(true);
        QString data = ui->teOut->toPlainText();
        downloadList = data.split(QRegExp("[\n]"), QString::SkipEmptyParts);
        downloadList.removeDuplicates();
        ui->actionSaveDownloadList->setEnabled(false);
    });
    connect(ui->actionDeleteDownloadList, &QAction::triggered, [ this ]() {
        downloadList.clear();
        ui->pbDownloadAll->setDisabled(true);
        ui->actionDownloadAll->setDisabled(true);
        ui->teOut->setReadOnly(true);
        ui->actionSaveDownloadList->setEnabled(false);
        ui->teOut->clear();
    });
    connect(ui->actionCheckOnStart, &QAction::triggered, [ this ]() {
        QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME, EXECUTABLE_NAME);
        settings.setIniCodec("UTF-8");
        settings.beginGroup("Update");

        if(ui->actionCheckOnStart->isChecked()) {
            settings.setValue("checkonstart", "true");
        } else {
            settings.setValue("checkonstart", "false");
        }

        settings.endGroup();
    });
    connect(ui->actionShowMore, &QAction::triggered, [ this ]() {
        QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME, EXECUTABLE_NAME);
        settings.setIniCodec("UTF-8");
        settings.beginGroup("Settings");

        if(ui->actionShowMore->isChecked()) {
            settings.setValue("showmore", "true");
        } else {
            settings.setValue("showmore", "false");
        }

        settings.endGroup();
    });
    connect(ui->actionCreateFolder, &QAction::triggered, [ this ]() {
        QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME, EXECUTABLE_NAME);
        settings.setIniCodec("UTF-8");
        settings.beginGroup("Settings");

        if(ui->actionCreateFolder->isChecked()) {
            settings.setValue("createfolder", "true");
        } else {
            settings.setValue("createfolder", "false");
        }

        settings.endGroup();
    });
    // Help
    connect(ui->actionHelp, &QAction::triggered, []() -> void {
        QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME, EXECUTABLE_NAME);
        settings.setIniCodec("UTF-8");
        settings.beginGroup("Language");
        QString sp = settings.value("language", "").toString();
        settings.endGroup();

        if(sp  == "")
        {
            sp = QLocale::system().name();
        } else if(sp == "de_DE")
        {
            sp = "en_US";
        }
        QDesktopServices::openUrl(QUrl("http://bin.ceicer.com/streamcapture2/current/help/index_" + sp + ".html", QUrl::TolerantMode));



    });
    connect(ui->actionCheckForUpdates, &QAction::triggered, [ ]() {
        auto * cfu = new CheckForUpdates;
        cfu->check(DISPLAY_NAME, VERSION, VERSION_PATH, DOWNLOAD_PATH);
    });
    // about
    connect(ui->actionVersionHistory, SIGNAL(triggered()), this, SLOT(versionHistory())); // about.cpp
    connect(ui->actionAbout, SIGNAL(triggered()), this, SLOT(about())); // about.cpp
    connect(ui->actionAboutSvtplayDl, SIGNAL(triggered()), this, SLOT(aboutSvtplayDl())); // about.cpp
    connect(ui->actionSvtplayDlForum, &QAction::triggered, []() ->void {
        QDesktopServices::openUrl(QUrl("https://github.com/spaam/svtplay-dl/issues", QUrl::TolerantMode));
    });
    connect(ui->actionAboutFfmpeg, SIGNAL(triggered()), this, SLOT(aboutFfmpeg())); // about.cpp
    connect(ui->actionLicense, SIGNAL(triggered()), this, SLOT(license())); // about.cpp
    connect(ui->actionLicenseFfmpeg, SIGNAL(triggered()), this, SLOT(licenseFfmpeg())); // about.cpp
    connect(ui->actionLicenseSvtplayDl, SIGNAL(triggered()), this, SLOT(licenseSvtplayDl())); // about.cpp
    // Language
    connect(ui->actionSwedish, SIGNAL(triggered()), this, SLOT(swedish())); // language.cpp
    connect(ui->actionEnglish, SIGNAL(triggered()), this, SLOT(english())); // language.cpp
    connect(ui->actionGerman, SIGNAL(triggered()), this, SLOT(german())); // language.cpp
    // Search and download
    connect(ui->pbSok, SIGNAL(pressed()), this, SLOT(initSok())); // download.cpp
    connect(ui->actionSearch, SIGNAL(triggered()), this, SLOT(initSok())); // download.cpp
    connect(ui->pbSok, SIGNAL(clicked()), this, SLOT(sok()));
    connect(ui->actionSearch, SIGNAL(triggered()), this, SLOT(sok()));
    // list all
    connect(ui->actionListAllEpisodes, SIGNAL(triggered()), this, SLOT(listAllEpisodes()));
    //
    connect(ui->pbDownload, SIGNAL(clicked()), this, SLOT(download()));
    connect(ui->actionDownload, SIGNAL(triggered()), this, SLOT(download()));
    connect(ui->pbAdd, &QPushButton::clicked, [ = ]() {
        QString in, provider, sub;

        if((ui->comboPayTV->currentText().indexOf("-", 0)) == 0) {
            provider = "nopassword";
        } else {
            provider = ui->comboPayTV->currentText();
        }

        if(ui->chbSubtitle->isChecked()) {
            sub = "ysub";
        } else {
            sub = "nsub";
        }

        QString soktext = ui->leSok->text();

        // ipac Bugg V0.15.0
        if(soktext.right(1) == '/') {
            int storlek = soktext.size();
            soktext.remove(storlek - 1, 1);
        }

        int hittat = soktext.indexOf('?');
        soktext = soktext.left(hittat);
        in = soktext + "," + ui->leQuality->text() + "," + ui->leMethod->text() + "," + provider + "," + sub;

        if(ui->pbAdd->isEnabled())
            downloadList.append(in);

        ui->pbDownloadAll->setEnabled(true);
        ui->actionDownloadAll->setEnabled(true);
    });
    connect(ui->actionAddAllEpisodesToDownloadList, &QAction::triggered, [ = ]() {
        QString in, provider, sub;

        if((ui->comboPayTV->currentText().indexOf("-", 0)) == 0) {
            provider = "nopassword";
        } else {
            provider = ui->comboPayTV->currentText();
        }

        if(ui->chbSubtitle->isChecked()) {
            sub = "ysub";
        } else {
            sub = "nsub";
        }

        QString soktext = ui->leSok->text();

        // ipac Bugg V0.15.0
        if(soktext.right(1) == '/') {
            int storlek = soktext.size();
            soktext.remove(storlek - 1, 1);
        }

        int hittat = soktext.indexOf('?');
        soktext = soktext.left(hittat);
        QString unknown = "unknown";

        if(ui->actionAddAllEpisodesToDownloadList->isEnabled()) {
            QString data = ui->teOut->toPlainText();
            QStringList addresses = data.split(QRegExp("[\n]"), QString::SkipEmptyParts);

            foreach(QString tmp, addresses) {
                if((tmp.left(8) == "https://") || (tmp.left(7) == "http://")) {
                    in = tmp + "," + unknown + "," + unknown + "," + provider + "," + sub;
                    downloadList.append(in);
                }
            }

            ui->pbDownloadAll->setEnabled(true);
            ui->actionDownloadAll->setEnabled(true);
        }
    });
    connect(ui->actionAdd, &QAction::triggered, [ = ]() {
        QString in, provider, sub;

        if((ui->comboPayTV->currentText().indexOf("-", 0)) == 0) {
            provider = "nopassword";
        } else {
            provider = ui->comboPayTV->currentText();
        }

        if(ui->chbSubtitle->isChecked()) {
            sub = "ysub";
        } else {
            sub = "nsub";
        }

        QString soktext = ui->leSok->text();

        // ipac Bugg V0.15.0
        if(soktext.right(1) == '/') {
            int storlek = soktext.size();
            soktext.remove(storlek - 1, 1);
        }

        int hittat = soktext.indexOf('?');
        soktext = soktext.left(hittat);
        in = soktext + "," + ui->leQuality->text() + "," + ui->leMethod->text() + "," + provider + "," + sub;

        if(ui->pbAdd->isEnabled())
            downloadList.append(in);

        ui->pbDownloadAll->setEnabled(true);
        ui->actionDownloadAll->setEnabled(true);
    });
    connect(ui->pbDownloadAll, SIGNAL(clicked()), this, SLOT(downloadAll()));
    connect(ui->actionDownloadAll, SIGNAL(triggered()), this, SLOT(downloadAll()));
    // comboBox
    connect(ui->comboBox, SIGNAL(currentIndexChanged(int)), this, SLOT(comboBoxChanged()));
    // Pay TV
    connect(ui->actionCreateNew, &QAction::triggered, [ this ]() {
        newSupplier();
    });
    connect(ui->comboPayTV, QOverload<int>::of(&QComboBox::currentIndexChanged),
    [ = ](int index) {
        if(index == 0) {
            ui->pbPassword->setEnabled(false);
            ui->actionPassword->setEnabled(false);
        } else {
            ui->pbPassword->setEnabled(true);
            ui->actionPassword->setEnabled(true);
        }
    });
    connect(ui->pbPassword, &QPushButton::clicked, [ this ]() {
        QString provider = ui->comboPayTV->currentText();
        bool ok;
        QString newpassword = QInputDialog::getText(nullptr, provider + " " + QCoreApplication::translate("MainWindow", "Enter your password"),
                              QCoreApplication::translate("MainWindow", "Spaces are not allowed. Use only the characters your streaming provider approves.\nThe Password will not be saved."), QLineEdit::Password,
                              "", &ok);

        if(newpassword.indexOf(' ') >= 0)
            return false;

        if(ok) {
            secretpassword = newpassword;
            return true;
        }

        return true;
    });
    connect(ui->actionPassword, &QAction::triggered, [this]() {
        QString provider = ui->comboPayTV->currentText();
        bool ok;
        QString newpassword = QInputDialog::getText(nullptr, provider + " " + QCoreApplication::translate("MainWindow", "Enter your password"),
                              QCoreApplication::translate("MainWindow", "Spaces are not allowed. Use only the characters your streaming provider approves.\nThe Password will not be saved."), QLineEdit::Password,
                              "", &ok);

        if(newpassword.indexOf(' ') >= 0)
            return false;

        if(ok) {
            secretpassword = newpassword;
            return true;
        }

        return true;
    });
    // Subtitle
    connect(ui->actionSubtitle, &QAction::toggled, [ this ](bool checked) {
        if(checked) {
            ui->chbSubtitle->setChecked(true);
        } else {
            ui->chbSubtitle->setChecked(false);
        }
    });
    connect(ui->chbSubtitle, &QCheckBox::stateChanged, [ this ](int state) {
        if(state == Qt::Checked) {
            ui->actionSubtitle->setChecked(true);
        } else {
            ui->actionSubtitle->setChecked(false);
        }
    });
    connect(ui->actionDownloadAllEpisodes, &QAction::triggered, [ this ]() {
        downloadAllEpisodes();
    });
    /* Delete all settings */
    connect(ui->actionDeleteAllSettings, &QAction::triggered, [this]() {
        deleteAllSettings();
    });
#ifdef Q_OS_WIN // QT5
    connect(ui->actionStopAllDownloads, &QAction::triggered, [ this ]() {
        if(processpid > 0) {
//#ifdef Q_OS_LINUX
            // ipac
            // int rv = QProcess::execute("kill -9 " + QString::number(processpid));
            // int rv = QProcess::execute("pkill svtplay-dl");
            // int rv = QProcess::startDetached("pkill", {"svtplay-dl"});
            // int rv = QProcess::startDetached("pkill", {QString::number(processpid)});
            // int rv = system("pkill svtplay-dl");
//#endif
//#ifdef Q_OS_WIN // QT5
            // C:\>Taskkill /PID 26356 /F
            // int rv = QProcess::execute("taskkill /PID " + QString::number(processpid) + " /F");
            int rv = QProcess::execute("taskkill /IM svtplay-dl.exe /F");
//#endif

            switch(rv) {
                case -1:
                    ui->teOut->setTextColor(QColor("red"));
                    ui->teOut->append(QCoreApplication::translate("MainWindow", "svtplay-dl crashed."));
                    ui->teOut->setTextColor(QColor("black"));
                    break;

                case -2:
                    ui->teOut->setTextColor(QColor("red"));
                    ui->teOut->append(QCoreApplication::translate("MainWindow", "Could not stop svtplay-dl."));
                    ui->teOut->setTextColor(QColor("black"));
                    break;

                default:
                    ui->teOut->setTextColor(QColor("red"));
                    ui->teOut->append(QCoreApplication::translate("MainWindow", "svtplay-dl stopped. Exit code ") + QString::number(rv) + "\n" + QCoreApplication::translate("MainWindow", "Delete any files that may have already been downloaded."));
                    avbrutet = true;
                    ui->teOut->setTextColor(QColor("black"));
                    processpid = 0;
            }
        }
    });
#endif
    // SET DEFAULT LOCATION
    //
    connect(ui->actionCopyToDefaultLocation, &QAction::triggered, [this]() {
        QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME, EXECUTABLE_NAME);
        settings.setIniCodec("UTF-8");
        settings.beginGroup("Path");
        QString copypath = settings.value("copypath").toString();

        if(ui->actionCopyToDefaultLocation->isChecked()) {
            settings.setValue("copytodefaultlocation", "true");
            ui->statusBar->showMessage(QCoreApplication::translate("MainWindow", "Copy to: ") + copypath);
        } else {
            settings.setValue("copytodefaultlocation", "false");
        }

        settings.endGroup();
    });
    connect(ui->actionSetDefaultCopyLocation, &QAction::triggered, [this]() {
        QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME, EXECUTABLE_NAME);
        settings.setIniCodec("UTF-8");
        settings.beginGroup("Path");
        QString copypath = settings.value("copypath", QDir::homePath()).toString();
        settings.endGroup();
        QString dir = QFileDialog::getExistingDirectory(this, QCoreApplication::translate("MainWindow", "Copy downloaded streaming media to directory"),
                      copypath,
                      QFileDialog::ShowDirsOnly
                      | QFileDialog::DontResolveSymlinks
                      | QFileDialog::DontUseCustomDirectoryIcons);

        if(dir != "") {
            settings.beginGroup("Path");
            settings.setValue("copypath", dir);
            settings.endGroup();
        }
    });
    /* DOWNLOAD LOCATION */
    connect(ui->actionSetDefaultDownloadLocation, &QAction::triggered, [this]() {
        QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME, EXECUTABLE_NAME);
        settings.setIniCodec("UTF-8");
        settings.beginGroup("Path");
        QString defaultdownloadlocation = settings.value("defaultdownloadlocation", QDir::homePath()).toString();
        settings.endGroup();
        QString dir = QFileDialog::getExistingDirectory(this, QCoreApplication::translate("MainWindow", "Download streaming media to directory"),
                      defaultdownloadlocation,
                      QFileDialog::ShowDirsOnly
                      | QFileDialog::DontResolveSymlinks
                      | QFileDialog::DontUseCustomDirectoryIcons);

        if(dir != "") {
            settings.beginGroup("Path");
            settings.setValue("defaultdownloadlocation", dir);
            settings.endGroup();
        }
    });
    connect(ui->actionDownloadToDefaultLocation, &QAction::triggered, [this]() {
        QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME, EXECUTABLE_NAME);
        settings.setIniCodec("UTF-8");
        settings.beginGroup("Path");
        QString defaultdownloadlocation = settings.value("defaultdownloadlocation").toString();

        if(ui->actionDownloadToDefaultLocation->isChecked()) {
            settings.setValue("downloadtodefaultlocation", "true");
            ui->statusBar->showMessage(QCoreApplication::translate("MainWindow", "Download to: ") + defaultdownloadlocation);
        } else {
            settings.setValue("downloadtodefaultlocation", "false");
        }

        settings.endGroup();
    });
    /* Font */
    connect(ui->actionSelectFont, &QAction::triggered, [this] {


        SelectFont *mSelectFont;
        mSelectFont = new SelectFont(nullptr);

        mSelectFont->show();

        connect(mSelectFont, &SelectFont::valueChanged, this, &Newprg::setValue);

    });
    /* End Font */
    connect(ui->actionMaintenanceTool, &QAction::triggered, []() ->void {
        QString path = QCoreApplication::applicationDirPath() + "/streamCapture2MaintenanceTool";
        // qInfo() << path;
        bool b = QProcess::startDetached(path);

        if(b)
            exit(0);

        QMessageBox::critical(nullptr, DISPLAY_NAME " " VERSION, tr("MaintenanceTool cannot be found"));

    });
}

void Newprg::setValue(const QFont & font)
{
    ui->teOut->setFont(font);
}

// private slots:
void Newprg::comboBoxChanged()
{
    QString val = ui->comboBox->currentText();
    QRegExp number("\\d+");
    int first_number = val.indexOf(QRegExp(number));
    int last_number = val.lastIndexOf(QRegExp(number));
    QString quality = val.mid(first_number, last_number - first_number + 1);
    first_number = val.lastIndexOf(" ", -1);
    QString method = val.mid(first_number, val.size() - first_number);
    quality = quality.trimmed();
    ui->leQuality->setText(quality);
    method = method.trimmed();
    ui->leMethod->setText(method);
}

void Newprg::onCommProcessStart()
{
    ui->teOut->append(tr("Downloading..."));
}
void Newprg::onCommProcessExit_sok(int exitCode, QProcess::ExitStatus exitStatus)
{
    CommProcess2->waitForFinished(-1);

    if(exitCode == 0) {
        QTextStream stream(CommProcess2->readAllStandardOutput());
        QString s = "", sf = "", line = "";
        QStringList varannan;

        while(!stream.atEnd()) {
            line = stream.readLine();
            line = line.simplified();
            varannan << line;
        }

        varannan.removeDuplicates();

        foreach(QString listitm, varannan) {
            ui->comboBox->addItem(listitm);
            ui->teOut->append(listitm);
        }

        ui->pbDownload->setEnabled(true);
        ui->actionDownload->setEnabled(true);
        ui->actionDownloadAllEpisodes->setEnabled(true);
        ui->actionAddAllEpisodesToDownloadList->setEnabled(true);
        QString val = ui->comboBox->currentText();
        QRegExp number("\\d+");
        int first_number = val.indexOf(QRegExp(number));
        int last_number = val.lastIndexOf(QRegExp(number));
        QString quality = val.mid(first_number, last_number - first_number + 1);
        first_number = val.lastIndexOf(" ", -1);
        QString method = val.mid(first_number, val.size() - first_number);
        quality = quality.trimmed();
        ui->leQuality->setText(quality);
        method = method.trimmed();
        ui->leMethod->setText(method);
        bool lyckatsok = true;

        if(sf != "") {
            ui->teOut->setText(tr("Can not find any video streams, please check the address.\n\n"));
            ui->teOut->append(sf);
            lyckatsok = false;
        }

        if((s.left(6) == "ERROR:") || (!lyckatsok)) {
            ui->leQuality->clear();
            ui->leMethod->clear();
            ui->pbDownload->setDisabled(true);
            ui->actionDownload->setDisabled(true);
        } else {
            recentFiles.append(address);

            if(ui->comboBox->currentText().isEmpty()) {
                ui->pbAdd->setDisabled(true);
                ui->actionAdd->setDisabled(true);
            } else {
                ui->pbAdd->setEnabled(true);
                ui->actionAdd->setEnabled(true);
            }
        }

        ui->teOut->append(tr("The search is complete<br>"));
    } else {
        ui->teOut->append(tr("The search failed<br>"));
        ui->leMethod->setText("");
        ui->leQuality->setText("");
    }

    statusExit(exitStatus, exitCode);
    delete CommProcess2;
}


void Newprg::slotRecent()
{
    recentFiles.removeDuplicates();

    while(recentFiles.size() > MAX_RECENT)
        recentFiles.removeFirst();

    ui->menuRecent->clear();

    for(int i = recentFiles.size() - 1; i >= 0; i--) {
        QString path = recentFiles.at(i);
        auto actionRF = new QAction(path, this);
        ui->menuRecent->addAction(actionRF);
        actionRF->setStatusTip(tr("Click to copy to the search box."));
        /********************************
         * deprected
        QSignalMapper* signalMapperRF  = new QSignalMapper(this);
        connect(actionRF, SIGNAL(triggered()), signalMapperRF, SLOT(map()));
        signalMapperRF->setMapping(actionRF, path);
        connect(signalMapperRF, SIGNAL(mapped(const QString)), this, SLOT(recentRequested(const QString)));
        ***************************************************************/
        connect(actionRF, &QAction::triggered, [ this, path ]() {
            ui->leSok->setText(path);
        });
    }

    ui->menuRecent->addSeparator();
    QAction* actionAntalRecentFiles = new QAction(tr("The number of previous searches to be saved..."), this);
    ui->menuRecent->addAction(actionAntalRecentFiles);
    actionAntalRecentFiles->setStatusTip(tr("Specify how many previous searches you want to save. If the number of searches exceeds the specified number, the oldest search is deleted."));
    connect(actionAntalRecentFiles, &QAction::triggered, [this]() {
        bool ok;
        int i = QInputDialog::getInt(this, DISPLAY_NAME " " VERSION,
                                     tr("The number of searches to be saved: ") + "(0-" + QString::number(SIZE_OF_RECENT) + ")", MAX_RECENT, 0, SIZE_OF_RECENT, 1, &ok);

        if(ok)
            MAX_RECENT = i;
    });
    QAction* actionClearAntalRecentFiles = new QAction(tr("Remove all saved searches"), this);
    ui->menuRecent->addAction(actionClearAntalRecentFiles);
    actionClearAntalRecentFiles->setStatusTip(tr("Click to delete all saved searches."));
    connect(actionClearAntalRecentFiles, &QAction::triggered, [this]() {
        recentFiles.clear();
    });
}
// private:
void Newprg::deleteAllSettings()
{
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME, EXECUTABLE_NAME);
    const QString settingsFile = settings.fileName();
    int hitta = settingsFile.indexOf("/streamcapture2.ini");
    int lagom =  settingsFile.size() - hitta;
    QString settingsPath = settingsFile.mid(0, settingsFile.size() - lagom);
    QDir dir(settingsPath);
    int ret = QMessageBox::warning(this, DISPLAY_NAME " " VERSION, tr("All your saved settings will be deleted. All lists of files to download will disappear."),
                                   QMessageBox::Yes | QMessageBox::Cancel, QMessageBox::Cancel);

    if(ret == QMessageBox::Yes) {
        if(dir.removeRecursively()) {
            deleteSettings = true;
            exit(0);
        } else
            QMessageBox::critical(this, DISPLAY_NAME " " VERSION, tr("Failed to delete your configuration files. Check your file permissions."));
    }
}
void Newprg::statusExit(QProcess::ExitStatus exitStatus, int exitCode)
{
    if(ui->actionShowMore->isChecked()) {
        QString info;

        switch(exitStatus) {
            case QProcess::NormalExit:
                info = "QProcess" + tr(" was normally terminated. QProsess Exit code = ") + QString::number(exitCode);
                //  info = pr + tr(" was normally terminated.");
                break;

            case QProcess::CrashExit:
                info = "QProcess" + tr(" crashed. QProsess Exit code = ") + QString::number(exitCode);
                //  info = pr + tr(" crashed.");
                break;
        }

        ui->statusBar->showMessage(info);
    }
}
